package com.grupomulticolor.tripode

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.grupomulticolor.tripode.interfaces.ToolbarActivity
import com.grupomulticolor.tripode.ui.fragments_menu.carrito.CarritoFragment
import com.grupomulticolor.tripode.util.Keyboard
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.crashlytics.android.Crashlytics
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder
import com.google.android.gms.tasks.OnCompleteListener
import com.pd.chocobar.ChocoBar
import com.grupomulticolor.tripode.data.departamentos.Departamento
import com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones.PromocionesProductosFragment
import com.grupomulticolor.tripode.util.config.Constants
import com.grupomulticolor.tripode.util.lib.IMainView
import com.grupomulticolor.tripode.data.menu.ExpandableListAdapter
import com.grupomulticolor.tripode.data.menu.ItemMenu
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.productos.InicioProductosFragment
import com.google.android.gms.vision.barcode.Barcode
import com.google.firebase.iid.FirebaseInstanceId
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.grupomulticolor.tripode.ui.fragments_menu.busqueda.BusquedaFragment
import com.grupomulticolor.tripode.ui.fragments_menu.busqueda.BusquedaVaciaFragment
import com.grupomulticolor.tripode.ui.fragments_menu.categorias.CategoriasFragment
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.config.Constants.TAG_FRAGMENT
import com.grupomulticolor.tripode.util.lib.FragmentRoot
import com.grupomulticolor.walibray.dialog_form.WADialogForm
import io.fabric.sdk.android.Fabric
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.schedule


class MainActivity : ToolbarActivity(), MainContract.View, IMainView.View {


    private val BARCODE_KEY = "BARCODE"
    private var barcodeResult: Barcode? = null

    private var listDeptos: ArrayList<Departamento>? = null
    private var mPresenter: MainContract.Presenter? = null
    private var mCallback: IMainView.Callback? = null
    private var fragmentSelected: Fragment? = null
    private var busqueda_string : String = ""

    private val TAG = "tag"

    private var salir = true
    private var score: String = ""
    private var scorem: String = ""
    private var id_pe: Int = 0

    private var dialogForm: WADialogForm? = null

    var mMenuAdapter: ExpandableListAdapter? = null
    var listDataHeader: ArrayList<ItemMenu>? = null
    var listDataChild: HashMap<ItemMenu, List<String>>? = null
    private lateinit var prefs: SessionPrefs

//com.farmaciastripode.app

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this@MainActivity, Crashlytics())
        setContentView(R.layout.activity_main)
        toolbarToLoad(toolbar as Toolbar)

        logoHome.setOnClickListener { onClickHome() }
        btnMenu.setOnClickListener { onClickMenu() }
        btnCarrito.setOnClickListener {  irACarrito() }
        btnBusqueda.setOnClickListener { onClickBusqueda() }
        btnPromos.setOnClickListener { irAPromociones() }
        btnSearch.setOnClickListener {
            mPresenter?.onEvaluarBusqueda(inputSearch.text.toString())
            showSearch(false)
        }


        // inicializa el mvp
        val mainInteractor = MainInteractor(this@MainActivity)
        MainPresenter(this, mainInteractor)

        fab.setOnClickListener { mCallback?.actionFab() }

        initEvents()

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                val token = task.result?.token
                mPresenter?.sendToken("and-"+token!!)
                Log.d("TOKEN","TOKEN FIREBASE"+ token)
            })

        prefs = SessionPrefs(this)
        var id_pedido = prefs.getCalificacion()

        if(id_pedido != "0" && id_pedido != ""){
            id_pe = id_pedido.toInt()
            showFormScore()
        }


    }


    override fun onBackPressed() {

        Log.d("TAG", "ATRASSSS")

        if(salir) {
            var intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }else{
            super.onBackPressed()
        }

        /*
        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
            Log.d("TAG", "ATRASSSS")
            closeDrawable()
        }
        else {
            Log.d("TAG", "ATRASSSS super")

            super.onBackPressed()

            if (supportFragmentManager.fragments.size > 0) {
                fragmentSelected = supportFragmentManager.fragments.last()
                Log.d("TAG", "iffff")

            }
        }*/
    }

     fun irAtras() {


        if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
            Log.d("TAG", "ATRASSSS")
            closeDrawable()
        }
        else {
            Log.d("TAG", "ATRASSSS super")

            salir = false
            super.onBackPressed()
            salir = true
            if (supportFragmentManager.fragments.size > 0) {
                fragmentSelected = supportFragmentManager.fragments.last()
                Log.d("TAG", "iffff")

            }
        }
    }

    private fun initEvents() {
        inputSearch.setOnEditorActionListener { textView, actionId, event ->
            var procesado = false
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mPresenter?.onEvaluarBusqueda(inputSearch.text.toString())
                // Ocultar teclado virtual
                Keyboard.hide(this@MainActivity, textView as TextView)
                procesado = true
                showSearch(false)
            }
            return@setOnEditorActionListener procesado
        }

        btnSearchCode.setOnClickListener {
            //mPresenter?.onScanCode()
            showSearch(false)
            readBarcode()
            //launchScanCode()
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        Log.e(TAG, "PRUEBA SAVE")
        super.onSaveInstanceState(outState)
        if (fragmentSelected != null) {
            if (fragmentSelected!!.isAdded) {
                supportFragmentManager.putFragment(outState, "FragmentActual", fragmentSelected!!)
                Log.e(TAG, "PRUEBA SE GUARDO")

            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        Log.e(TAG, "PRUEBA RESTOREE")

        super.onRestoreInstanceState(savedInstanceState)
        fragmentSelected = supportFragmentManager.getFragment(savedInstanceState, "FragmentActual")
        if (fragmentSelected != null)
            mostrarFragment(fragmentSelected!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Destroy MAIN", (fragmentSelected == null).toString())
    }

    override fun onClickBusqueda() {
        if (layoutSearch.visibility == View.VISIBLE)
            showSearch(false)
        else
            showSearch(true)
    }

    override fun onClickHome() {
        clearStack()
        mostrarFragment(InicioProductosFragment.instance())
    }

    override fun onClickPromos() {
    }

    override fun onClickCarrito() {
    }

    override fun onClickMenu() {
        mPresenter?.openOrCloseDrawableMenu(!drawerLayout.isDrawerOpen(GravityCompat.START))
    }

    /* VIEW IMPLEMENTS */
    override fun openDrawable() {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    override fun closeDrawable() {
        drawerLayout.closeDrawer(GravityCompat.START)
    }

    override fun mostrarFragment(fragment: Fragment) {
        closeDrawable()
        if (fragmentSelected != fragment) {

            if (fragment is SinConexionFragment) {
                supportFragmentManager.popBackStack()
            }

            if (fragment is InicioProductosFragment) {
                supportFragmentManager.popBackStack()
                fragmentSelected = fragment
                supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.mainContainer, fragment)
                    .commit()
            } else {
                fragmentSelected = fragment
                supportFragmentManager
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .addToBackStack(TAG)
                    .replace(R.id.mainContainer, fragment, Constants.TAG_FRAGMENT)
                    .commit()
            }


        }
    }

    override fun clearStack() {
        supportFragmentManager
            .popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun irACarrito() {
        mostrarFragment(CarritoFragment.instance())
    }

    override fun irAPromociones() {
        val fragment = PromocionesProductosFragment.instance() as PromocionesProductosFragment
        fragment.setDetalle(false)
        this.mostrarFragment(fragment)
    }

    override fun showSearch(show: Boolean) {
        if (!show) {
            layoutSearch.visibility = View.GONE
            Keyboard.hideSoftKeyboard(this@MainActivity)
        } else {
            layoutSearch.visibility = View.VISIBLE
            inputSearch.requestFocus()
            Keyboard.show(this, inputSearch)
        }
    }

    override fun irABuscar(text: String) {
        BusquedaFragment.destroy()
        val fragment = BusquedaFragment.instance() as BusquedaFragment
        fragment.setBusqueda(text)
        this.mostrarFragment(fragment)
    }

    override fun readBarcode() {
            clearStack()
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                launchScanCode()
            } else {
                Dexter.withActivity(this@MainActivity)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(object : PermissionListener {
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            launchScanCode()
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permission: PermissionRequest?,
                            token: PermissionToken?
                        ) {
                            Log.d("Splash", "onPermissionRationaleShouldBeShown")
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            ChocoBar.builder().setActivity(this@MainActivity)
                                .setText("Permiso denegado")
                                .setDuration(ChocoBar.LENGTH_LONG)
                                .green()
                                .show()
                        }

                    }).withErrorListener {

                    }.check()
            }

    }

    private fun launchScanCode() {

        val materialBarcodeScanner = MaterialBarcodeScannerBuilder()
            .withActivity(this@MainActivity)
            .withEnableAutoFocus(true)
            .withBleepEnabled(true)
            .withBackfacingCamera()
            .withText(getString(R.string.scan_bar_code))
            .withCenterTracker()
            .withResultListener { barcode ->
                barcodeResult = barcode
                busqueda_string = barcodeResult?.rawValue!!
                Timer("SettingUp", false).schedule(1500) {
                    irABuscar(busqueda_string)
                }
            }
            .build()
        materialBarcodeScanner.startScan()
    }

    override fun setPresenter(presenter: MainContract.Presenter) {
        this.mPresenter = presenter
    }

    override fun setCallback(callback: IMainView.Callback?) {
        mCallback = callback
    }

    override fun showFab(show: Boolean) {
        fab.visibility =
            if (show) View.VISIBLE
            else View.GONE
    }

    override fun intentLlamar() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.tel_tripode)))
            startActivity(intent)
        } else {
            Dexter.withActivity(this@MainActivity)
                .withPermission(Manifest.permission.CALL_PHONE)
                .withListener( object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.tel_tripode)))
                        startActivity(intent)
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        Log.d("MainActivity", "Rationale should")
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        ChocoBar.builder().setActivity(this@MainActivity)
                            .setText("Permiso denegado")
                            .setDuration(ChocoBar.LENGTH_LONG)
                            .green()
                            .show()
                    }

                }).withErrorListener {
                    ChocoBar.builder().setActivity(this@MainActivity)
                        .setText(it.name)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .green()
                        .show()
                }.check()
        }
    }

    override fun cargarMenu(listDataHeader: ArrayList<ItemMenu>,
                            listDataChild: HashMap<ItemMenu, List<String>>,
                            listDep: ArrayList<Departamento>) {

        this.listDataHeader = listDataHeader
        this.listDataChild = listDataChild
        this.listDeptos = listDep

        mMenuAdapter = ExpandableListAdapter(applicationContext, listDataHeader, listDataChild,
            object : Events {
                                override fun onClick(itemId: Int) {
                                    mPresenter?.onItemMenuSelected(itemId)
                                }
                            },
            object : Events {
                                override fun onClick(itemId: Int) {
                                    var nombredep = listDep[itemId].nombre!!
                                    var iddep = listDep[itemId].id!!

                                    CategoriasFragment.destroy()
                                    val fragment = CategoriasFragment.instance() as CategoriasFragment
                                    fragment.setBusqueda(iddep)
                                    fragment.setDepartamento(nombredep)
                                    mostrarFragment(fragment)
                                }
                            }
            )
        navigationmenu.setAdapter(mMenuAdapter)
        mostrarFragment(InicioProductosFragment.instance())
    }

    fun showFormScore(){
        val dialog = WADialogForm.Builder(this)
            .succes()
            .setTitle("El pedido fue entregado")
            .setMessage("Califica el servicio")
            .setButtonPositive("Enviar", object : WADialogForm.OnClickListener {
                override fun onClick() {
                    getScore(dialogForm!!)
                }
            }).build()
        dialog.show()
        dialogForm = dialog
    }

    fun getScore(dialogForm : WADialogForm) {
        score = dialogForm.getScore()
        scorem = dialogForm.getScoreMessage()
        prefs.saveCalificacion("0")
        Log.d(TAG, "CLIENTE : $id_pe $score m$scorem")
        mPresenter?.actualizarComentario(id_pe,scorem, score)
    }

    override fun muestrarError() {
        InicioProductosFragment.destroy()
        val fragment = InicioProductosFragment.instance() as InicioProductosFragment
        this.mostrarFragment(fragment)
    }

    override fun showCalificacion() {
        InicioProductosFragment.destroy()
        val fragment = InicioProductosFragment.instance() as InicioProductosFragment
        this.mostrarFragment(fragment)
    }

}
