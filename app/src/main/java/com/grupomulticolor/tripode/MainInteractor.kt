package com.grupomulticolor.tripode

import android.content.Context
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.data.carrito.source.CarritoService
import com.grupomulticolor.tripode.data.departamentos.Departamento
import com.grupomulticolor.tripode.data.departamentos.source.DepartamentoService
import com.grupomulticolor.tripode.data.menu.ItemMenu
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.pedido.source.PedidoService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.data.usuario.source.service.UsuarioService
import com.grupomulticolor.tripode.ui.fragments_menu.carrito.CarritoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.EstatusFragment
import com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.HistorialFragment
import com.grupomulticolor.tripode.ui.fragments_menu.productos.InicioProductosFragment
import com.grupomulticolor.tripode.ui.fragments_menu.mensajes.CentroMensajesFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.MenuPerfilFragment
import com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones.PromocionesProductosFragment
import com.grupomulticolor.tripode.ui.fragments_menu.programa_lealtad.ProgramaLealtadFragment
import com.grupomulticolor.tripode.util.Network

class MainInteractor (private val context: Context) {

    private val service = DepartamentoService.instance(context)

    private val servicecarrito = CarritoService.instance(context)

    private val serviceUser = UsuarioService.instance(context)

    private val servicePedido = PedidoService.instance(context)

    fun openOrCloseDrawerLayout(cerrado: Boolean, callback: Callback) {
        if (cerrado)
            callback.openDrawer()
        else
            callback.closeDrawer()
    }

    fun menuItemSelected(idItemMenu: Int, callback: Callback) {
        when (idItemMenu) {
            R.id.itemMenuDrawablePerfil -> {
                callback.cambiarFragment(MenuPerfilFragment.instance())
            }
            R.id.itemMenuDrawableInicio -> {
                callback.limpiarPila()
                callback.cambiarFragment(InicioProductosFragment.instance())
            }
            R.id.itemMenuDrawableLealtad -> {
                callback.cambiarFragment(ProgramaLealtadFragment.instance())
            }
            R.id.itemMenuDrawableCarrito -> {
                callback.cambiarFragment(CarritoFragment.instance())
            }
            R.id.itemMenuDrawablePedidos -> {
                callback.cambiarFragment(EstatusFragment.instance())
            }
            R.id.itemMenuDrawableMensajes -> {
                callback.cambiarFragment(CentroMensajesFragment.instance())
            }
            R.id.itemMenuDrawablePromociones -> {
                val fragment = PromocionesProductosFragment.instance() as PromocionesProductosFragment
                fragment.setDetalle(false)
                callback.cambiarFragment(fragment)
            }
            R.id.itemMenuDrawableCompras -> {
                callback.cambiarFragment(HistorialFragment.instance())
            }
            R.id.itemMenuDrawableLlamanos -> {
                callback.onClickLlamanos()
            }
        }
    }

    fun sendToken(token: String, callback: Callback){

        serviceUser.sendToken(token, object : ResponseListener<ResponseData<Usuario>> {
            override fun onSuccess(response: ResponseData<Usuario>) {
                try {
                    callback.enviarToken()
                } catch (e: Exception) {
                    callback.enviarToken()
                }
            }
            override fun onError(t: Throwable) {
                callback.enviarToken()
            }
        })
    }

    fun crearMenu(callback: Callback) {
        val listDataHeader = ArrayList<ItemMenu>()
        val listDataChild = HashMap<ItemMenu, List<String>>()



        val menuInicio = ItemMenu()
        menuInicio.nameMenu = context.getString(R.string.menu_drawable_inicio)
        menuInicio.icon = R.drawable.ico_inicio
        menuInicio.itemId = R.id.itemMenuDrawableInicio
        listDataHeader.add(menuInicio)

        val menuProfile = ItemMenu()
        menuProfile.nameMenu = context.getString(R.string.menu_mi_perfil)
        menuProfile.icon = R.drawable.ico_profile
        menuProfile.itemId = R.id.itemMenuDrawablePerfil
        listDataHeader.add(menuProfile)

        /*
        val menuLealtad = ItemMenu()
        menuLealtad.nameMenu = context.getString(R.string.menu_drawable_lealtad)
        menuLealtad.icon = R.drawable.ico_programa
        menuLealtad.itemId = R.id.itemMenuDrawableLealtad
        listDataHeader.add(menuLealtad)*/

        val menuCarrito = ItemMenu()
        menuCarrito.nameMenu = context.getString(R.string.menu_drawable_carrito)
        menuCarrito.icon = R.drawable.ico_carrito
        menuCarrito.itemId = R.id.itemMenuDrawableCarrito
        listDataHeader.add(menuCarrito)

        val menuPedidos = ItemMenu()
        menuPedidos.nameMenu = context.getString(R.string.menu_drawable_pedidos)
        menuPedidos.icon = R.drawable.ico_marker
        menuPedidos.itemId = R.id.itemMenuDrawablePedidos
        listDataHeader.add(menuPedidos)

        val menuPromociones = ItemMenu()
        menuPromociones.nameMenu = context.getString(R.string.menu_drawable_promociones)
        menuPromociones.icon = R.drawable.ico_promos
        menuPromociones.itemId = R.id.itemMenuDrawablePromociones
        listDataHeader.add(menuPromociones)


        val menuCategorias = ItemMenu()
        menuCategorias.nameMenu = context.getString(R.string.menu_drawable_categorias)
        menuCategorias.icon = R.drawable.ico_categorias
        menuCategorias.itemId = R.id.itemMenuDrawableCategorias
        listDataHeader.add(menuCategorias)

        val menuHistorial = ItemMenu()
        menuHistorial.nameMenu = context.getString(R.string.menu_drawable_compras)
        menuHistorial.icon = R.drawable.ico_historial
        menuHistorial.itemId = R.id.itemMenuDrawableCompras
        listDataHeader.add(menuHistorial)

        val menuMensajes = ItemMenu()
        menuMensajes.nameMenu = context.getString(R.string.menu_drawable_mensajes)
        menuMensajes.icon = R.drawable.ico_mensajes
        menuMensajes.itemId = R.id.itemMenuDrawableMensajes
        listDataHeader.add(menuMensajes)

        val menuLlamanos = ItemMenu()
        menuLlamanos.nameMenu = context.getString(R.string.menu_drawable_llamanos)
        menuLlamanos.icon = R.drawable.ico_phone
        menuLlamanos.itemId = R.id.itemMenuDrawableLlamanos
        listDataHeader.add(menuLlamanos)

        val footer = ItemMenu()
        footer.nameMenu = ""
        footer.isFooter = true
        footer.itemId = -1
        listDataHeader.add(footer)

        callback.loadMenu(listDataHeader, listDataChild, arrayListOf())


        if (Network.isAvailable(context)) {
            service.getDepartamentos(object : ResponseListener<ResponseDataTripode<ArrayList<Departamento>>> {
                override fun onSuccess(response: ResponseDataTripode<ArrayList<Departamento>>) {
                    val heading = ArrayList<String>()
                    val deps = response.data ?: ArrayList()
                    for (dps in deps){
                        var n = dps.nombre!!
                        heading.add(n)
                    }

                    listDataChild[listDataHeader[5]] = heading
                    callback.loadMenu(listDataHeader, listDataChild, deps)
                }
                override fun onError(t: Throwable) {
                    listDataChild[listDataHeader[5]] = ArrayList() // Header, Child data
                    callback.loadMenu(listDataHeader, listDataChild, arrayListOf())
                }
            })
        } else {
            listDataChild[listDataHeader[5]] = ArrayList() // Header, Child data
            callback.loadMenu(listDataHeader, listDataChild, arrayListOf())
        }






        val id_ldcom = SessionPrefs.getInstance(context).getIdLdcom()
        servicecarrito.getIdcarrito( id_ldcom, "", object : ResponseListener<ResponseDataTripode<Int>> {
            override fun onSuccess(response: ResponseDataTripode<Int>) {
                try {
                    var id_carrito = response.data as Int
                    SessionPrefs.getInstance(context).saveShoppingCar(id_carrito)
                } catch (e: Exception) {
                }
            }
            override fun onError(t: Throwable) {
            }
        })

    }

     fun actualizarPedidoComentarios(pedido: Pedido, callback: Callback) {

        servicePedido.putComentarios(pedido, object : ResponseListener<ResponseData<Pedido>> {

            override fun onSuccess(response: ResponseData<Pedido>) {
                callback.onRequestScoreSucces(response.data as Pedido)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed()
            }

        })
    }


    interface Callback {
        fun onClickLlamanos()
        fun openDrawer()
        fun closeDrawer()
        fun enviarToken()
        fun onRequestFailed()
        fun limpiarPila()
        fun cambiarFragment(fragment: Fragment)
        fun onRequestScoreSucces(pedido: Pedido)
        fun loadMenu(listDataHeader: ArrayList<ItemMenu>,
                     listDataChild: HashMap<ItemMenu, List<String>>,
                     listDeptos: ArrayList<Departamento>)
    }
}