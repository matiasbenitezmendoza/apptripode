package com.grupomulticolor.tripode

import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.data.departamentos.Departamento
import com.grupomulticolor.tripode.data.menu.ItemMenu

interface MainContract {

    interface View : BaseView<Presenter> {
        fun openDrawable()
        fun closeDrawable()
        fun intentLlamar()
        fun irACarrito()
        fun irAPromociones()
        fun irABuscar(text: String)
        fun readBarcode()
        fun mostrarFragment(fragment: Fragment)
        fun clearStack()
        fun showCalificacion()
        fun muestrarError()
        fun cargarMenu(listDataHeader: ArrayList<ItemMenu>,
                       listDataChild: HashMap<ItemMenu,
                               List<String>>, listDeptos: ArrayList<Departamento>)
    }

    interface Presenter : BasePresenter {
        fun openOrCloseDrawableMenu(cerrado: Boolean)
        fun onItemMenuSelected(idItemMenu: Int)
        fun onEvaluarBusqueda(text: String)
        fun sendToken(token: String)
        fun actualizarComentario(id: Int, c: String, c2: String)
        fun onScanCode()
    }
}