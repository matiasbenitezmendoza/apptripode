package com.grupomulticolor.tripode

import android.app.Application
import com.grupomulticolor.tripode.util.config.Constants
import mx.openpay.android.Openpay

class App : Application() {

    val openpay: Openpay = Openpay(Constants.OPEN_PAY_ID, Constants.OPEN_PAY_kEY_PUBLICA, Constants.PRODUCTION)

    init {

    }

}