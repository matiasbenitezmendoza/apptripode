package com.grupomulticolor.tripode.data.facturacion.source

import com.grupomulticolor.tripode.data.facturacion.Factura
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface FacturaApi {

    //billing?connection=&transactionnumber=0010101000400174&userrfc=TECP690629J65&email=ptrevino@aph.mx&cfdiid=3
    @POST("billing")
    fun post(
        @Query("connection") conexion: String,
        @Query("transactionnumber") transaccion: String,
        @Query("userrfc") rfc: String,
        @Query("email") email: String,
        @Query("cfdiid") orderbyNamevalue: String
    ): Call<ResponseDataTripode<Factura>>
}