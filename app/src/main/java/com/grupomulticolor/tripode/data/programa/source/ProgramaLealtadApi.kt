package com.grupomulticolor.tripode.data.programa.source


import com.grupomulticolor.tripode.data.programa.ProgramaLealtad
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.*


interface ProgramaLealtadApi {

    //user/51/cards?connection=

    @GET("user/{userid}/cards")
    fun get(
        @Path("userid") userid: Int,
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<ArrayList<ProgramaLealtad>>>

}
