package com.grupomulticolor.tripode.data.direcciones

import com.google.gson.annotations.SerializedName

class Direccion(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("nombre")
    var nombreReferencia: String = "",
    @SerializedName("calle")
    var calle: String = "",
    @SerializedName("numero_exterior")
    var noExt: String = "",
    @SerializedName("numero_interior")
    var noInt: String = "",
    @SerializedName("residencial")
    var residencial: String = "",
    @SerializedName("codigo_postal")
    var codigoPostal: String = "",
    @SerializedName("estado")
    var estado: String = "",
    @SerializedName("colonia")
    var colonia: String = "",
    @SerializedName("ciudad")
    var ciudad: String = "",
    @SerializedName("referencia")
    var referencias: String = "",
    @SerializedName("seleccionada")
    var selected: Boolean = false,
    @SerializedName("pNivel1")
    var pNivel1: Int = 0,
    @SerializedName("pNivel2")
    var pNivel2: Int = 0,
    @SerializedName("pNivel3")
    var pNivel3: Int = 0
) {


    fun detalle() : String {
        var noint = ""
        if (noInt.isNotEmpty())
            noint = "int $noInt"

        return  "$calle $noExt $noint\nCol. $colonia\n"+
                "Residencial $residencial C.P. $codigoPostal\n"+
                "$estado\n$referencias"
    }
}