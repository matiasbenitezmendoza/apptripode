package com.grupomulticolor.tripode.data.padecimientos

import android.graphics.drawable.Drawable
import android.net.Uri
import com.google.gson.annotations.SerializedName
import com.pchmn.materialchips.model.ChipInterface

class Padecimiento : ChipInterface {

    @SerializedName("id_padecimiento")
    var id: Int? = null
    @SerializedName("nombre")
    var nombre: String? = null
    @SerializedName("descripcion")
    var descripcion: String? = null
    @SerializedName("imagen")
    var imagen: String? = null


    override fun getInfo(): String {
        return this.descripcion ?: ""
    }

    override fun getAvatarDrawable(): Drawable? {
        return null
    }

    override fun getLabel(): String {
        return nombre ?: "no name"
    }

    override fun getId(): Any {
        if (id != null) return id!!
        return 0
    }

    override fun getAvatarUri(): Uri {
        return Uri.parse(this.imagen)
    }
}