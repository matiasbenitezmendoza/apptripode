package com.grupomulticolor.tripode.data.pedido
import com.google.gson.annotations.SerializedName
import com.grupomulticolor.tripode.data.detallepedido.Detallepedido
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import kotlin.collections.ArrayList

class Pedido (
    @SerializedName("id") var id: Int? = 0,
    @SerializedName("id_usuario") var id_usuario: Int? = 0,
    @SerializedName("id_repartidor") var id_repartidor: Int? = 0,
    @SerializedName("orden") var orden: String? = "",
    @SerializedName("id_direccion") var id_direccion: Int? = 0,
    @SerializedName("fecha_pedido") var fecha_pedido: String? = "",
    @SerializedName("status_pedido") var status_pedido: Int? = 0,
    @SerializedName("comentarios_usuario") var comentarios_usuario: String? = "",
    @SerializedName("comentarios_repartidor") var comentarios_repartidor: String? = "",
    @SerializedName("calificacion_usuario") var calificacion_usuario: String? = "",
    @SerializedName("calificacion_repartidor") var calificacion_repartidor: String? = "",
    @SerializedName("tipo_pago") var tipo_pago: Int? = 0,
    @SerializedName("status_pago") var status_pago: Int? = 0,
    @SerializedName("calle") var calle: String? = "",
    @SerializedName("numero_exterior") var numero_exterior: String? = "",
    @SerializedName("numero_interior") var numero_interior: String? = "",
    @SerializedName("residencial") var residencial: String? = "",
    @SerializedName("codigo_postal") var codigo_postal: String? = "",
    @SerializedName("estado") var estado: String? = "",
    @SerializedName("ciudad") var ciudad: String? = "",
    @SerializedName("colonia") var colonia: String? = "",
    @SerializedName("referencia") var referencia: String? = "",
    @SerializedName("rfc") var rfc: String? = "",
    @SerializedName("razon_social") var razon_social: String? = "",
    @SerializedName("calle_fac") var calle_fac: String? = "",
    @SerializedName("numero_exterior_fac") var numero_exterior_fac: String? = "",
    @SerializedName("numero_interior_fac") var numero_interior_fac: String? = "",
    @SerializedName("codigo_postal_fac") var codigo_postal_fac: String? = "",
    @SerializedName("estado_fac") var estado_fac: String? = "",
    @SerializedName("municipio_fac") var municipio_fac: String? = "",
    @SerializedName("colonia_fac") var colonia_fac: String? = "",
    @SerializedName("tipo_envio") var tipo_envio: Int? = 0,
    @SerializedName("monto") var total: Double? = 0.0,
    @SerializedName("tarjeta") var tarjeta: String? = "",
    @SerializedName("mes_tarjeta") var mes_tarjeta: Int? = 0,
    @SerializedName("anio_tarjeta") var anio_tarjeta: Int? = 0,
    @SerializedName("detalle") var detallePedido: ArrayList<Detallepedido>? = null

){
    fun setDireccion( direccion : Direccion? ) {
        if(direccion != null){
                    this.calle = direccion.calle
                    this.numero_exterior = direccion.noExt
                    this.numero_interior = direccion.noInt
                    this.residencial = direccion.residencial
                    this.codigo_postal = direccion.codigoPostal
                    this.estado = direccion.estado
                    this.ciudad = direccion.ciudad
                    this.colonia = direccion.colonia
                    this.referencia = direccion.referencias
        }
    }

    fun setFacturacion( facturacion : Facturacion? ) {
        if(facturacion != null){
            this.rfc = facturacion.rfc
            this.razon_social = facturacion.nombre_razonSocial
            this.calle_fac = facturacion.calle
            this.numero_exterior_fac = facturacion.noExt
            this.numero_interior_fac = facturacion.noInt
            this.codigo_postal_fac = facturacion.codigoPostal
            this.estado_fac = facturacion.estado
            this.municipio_fac = facturacion.municipio
            this.colonia_fac = facturacion.colonia
        }
    }



}