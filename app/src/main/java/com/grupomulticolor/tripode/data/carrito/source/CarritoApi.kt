package com.grupomulticolor.tripode.data.carrito.source

import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET




interface CarritoApi {


    //shoppingcart/userid/mobileid?connection=
    //shoppingcart/16/id?connection=
    @GET("shoppingcart/{userid}/id")
    fun getIdcarrito(
        @Path("userid") userid: Int,
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<Int>>

    //shoppingcart/shoppingcartid?connection=
    @GET("shoppingcart/{shoppingcartid}")
    fun getCarrito(
        @Path("shoppingcartid")  shoppingcartid : Int,
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<ArrayList<Carrito>>>


    //shoppingcart/shoppingcartid/item/itemid/quantity?connection=
    @POST("shoppingcart/{shoppingcartid}/item/{itemid}/{quantity}")
    fun addCarrito(
        @Path("shoppingcartid")  shoppingcartid : Int,
        @Path("itemid") itemid: String,
        @Path("quantity") quantity: Int,
        @Query("connection") conexion: String
        ): Call<ResponseDataTripode<Any>>


    //shoppingcartupdate/shoppingcartid/item/itemid/quantity?connection=
    @PUT("shoppingcartupdate/{shoppingcartid}/item/{itemid}/{quantity}")
    fun updateCarrito(
        @Path("shoppingcartid")  shoppingcartid : Int,
        @Path("itemid") itemid: String,
        @Path("quantity") quantity: Int,
        @Query("connection") connection: String
        ): Call<ResponseDataTripode<Any>>

    //shoppingcart/shoppingcartid/item/itemid?connection
    @DELETE("shoppingcart/{shoppingcartid}/item/{itemid}")
    fun deleteCarrito(
        @Path("shoppingcartid")  shoppingcartid : Int,
        @Path("itemid") itemid: String,
        @Query("connection") conexion: String
        ): Call<ResponseDataTripode<Any>>

   /*
    http://pruebas.aph.mx:8085/FRAGARE/RestApi_eCommerce/eCommerce/checkout?
    conexion=&
    pUbicacion=16&
    pPago={"Nombre": "", "NumeroTarjeta": "","TarejtaMes":"","TarejtaAnno":"","CodigoSeguridad":"", "TipoTarjeta":1, "Monto":4533.3,"MontoEnvio":0,
           "GastoID":0,"FacturaElectronica":false,"TipoPagoID":2}&
    pSocioId=16&
    pTipoEntrega=0&
    pSucursalRecogeId=0
   */
    @POST("checkout")
    fun checkout(
       @Query("connection") conexion: String,
       @Query("pUbicacion") pUbicacion: Int,
       @Query("pPago") pPago: String,
       @Query("pSocioId") pSocioId: Int,
       @Query("pTipoEntrega") pTipoEntrega: Int,
       @Query("pSucursalRecogeId") pSucursalRecogeId: Int
   ) : Call<ResponseDataTripode<Any>>
}