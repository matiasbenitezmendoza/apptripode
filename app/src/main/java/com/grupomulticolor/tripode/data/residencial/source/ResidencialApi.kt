package com.grupomulticolor.tripode.data.residencial.source

import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.GET


interface ResidencialApi {

    @GET("residencias/residencias")
    fun get(): Call<ResponseData<ArrayList<Residencial>>>


}
