package com.grupomulticolor.tripode.data.login

import com.google.gson.annotations.SerializedName

class LoginBody(
    @SerializedName("user") var user: String? = null,
    @SerializedName("pass") var pass: String? = null,
    @SerializedName("version") var version: String? = null,
    // for facebook
    @SerializedName("id_facebook") var idFacebook: String? = null,
    @SerializedName("token_facebook") var tokenFacebook: String? = null,
    @SerializedName("nombre") var nombre: String? = null,
    @SerializedName("apellido_paterno") var apellidoP: String? = null,
    @SerializedName("apellido_materno") var apellidoM: String? = null,
    @SerializedName("fecha_nacimiento") var fechaNacimiento: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("telefono") var telefono: String? = null
)