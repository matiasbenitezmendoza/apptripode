package com.grupomulticolor.tripode.data.temas

import com.google.gson.annotations.SerializedName

class Tema(
    @SerializedName("id_tema")
    var id: Int,
    @SerializedName("nombre")
    var nombre: String,
    @SerializedName("descripcion")
    var descripcion: String,
    @SerializedName("imagen")
    var imagen: String
) {
}