package com.grupomulticolor.tripode.data.session

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.grupomulticolor.tripode.BuildConfig
import com.grupomulticolor.tripode.data.usuario.Usuario

class SessionPrefs (var context: Context) {

    private var mPrefs: SharedPreferences? = null

    init {
        mPrefs = context.applicationContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
    }

    fun saveToken(token: String) {
        val editor = mPrefs?.edit()
        editor?.putString(USER_TOKEN, token)
        editor?.apply()
    }

    fun saveShoppingCar(id: Int) {
        val editor = mPrefs?.edit()
        editor?.putInt(USER_SC, id)
        editor?.apply()
    }

    fun isLogin() : Boolean {
        val token = getToken()
        return (token != null && !TextUtils.isEmpty(token))
    }

    fun currentVersion() : Boolean {
        val usu = currentUsu()
        if (usu.version != null) {
            return usu.version == BuildConfig.VERSION_NAME
        }
        return false
    }

    fun getToken() : String? {
        return mPrefs?.getString(USER_TOKEN, "")
    }

    fun logOut() {
        val editor = mPrefs?.edit()
        editor?.putString(USER_TOKEN, null)
        editor?.putInt(USER_DIREC, -1)
        editor?.putInt(USER_TARJ, -1)
        editor?.apply()
    }

    fun currentUsu() : Usuario {
        val currentUser = Usuario()
        if (isLogin()) {
            currentUser.setValuesFromToken(getToken() ?: "")
        }
        return currentUser
    }

    fun saveDireccionEntrega(id: Int) {
        val editor = mPrefs?.edit()
        editor?.putInt(USER_DIREC, id)
        editor?.apply()
    }

    fun getDireccionEntregId() : Int {
        return mPrefs?.getInt(USER_DIREC, -1) ?: -1
    }

    fun saveTarjeta(id: Int) {
        val editor = mPrefs?.edit()
        editor?.putInt(USER_TARJ, id)
        editor?.apply()
    }

    fun getTarjetaId() : Int {
        return mPrefs?.getInt(USER_TARJ, -1) ?: -1
    }

    fun saveTipoPago(id: Int) {
        val editor = mPrefs?.edit()
        editor?.putInt(USER_TIPO_PAGO, id)
        editor?.apply()
    }

    fun getTipoPago() : Int {
        return mPrefs?.getInt(USER_TIPO_PAGO, -1) ?: -1
    }

    fun saveTipoEnvio(id: Int) {
        val editor = mPrefs?.edit()
        editor?.putInt(USER_TIPO_ENVIO, id)
        editor?.apply()
    }

    fun getTipoEnvio() : Int {
        return mPrefs?.getInt(USER_TIPO_ENVIO, -1) ?: -1
    }

    fun getIdLdcom() : Int {
        val usu = currentUsu()
        return usu.idUsuarioLdcom!!
    }

    fun getIdSC() : Int {
        return mPrefs?.getInt(USER_SC, -1) ?: -1
    }

    fun getFechaActivacion() : String {
        val usu = currentUsu()
        return usu.fecha_activacion!!
    }


    fun getNombreUsuario() : String {
        val usu = currentUsu()
        return usu.nombre + " "+ usu.apellidoPaterno +" "+ usu.apellidoMaterno
    }


    fun saveCalificacion(id: String) {
        val editor = mPrefs?.edit()
        editor?.putString(USER_CALIFICACION, id)
        editor?.apply()
    }

    fun getCalificacion() : String {
        return mPrefs?.getString(USER_CALIFICACION, "0") ?: "0"
    }



    companion object {
        const val USER_PREFS = "user_prefs_tripode"
        const val USER_TOKEN = "token_tripode"
        const val USER_DIREC = "user_direccion"
        const val USER_TARJ = "user_tarjeta"
        const val USER_LDCOM = "id_usuario_ldcom"
        const val USER_SC = "id_sc"
        const val USER_TIPO_PAGO = "tipo_pago"
        const val USER_TIPO_ENVIO = "tipo_envio"
        const val USER_FECHA = "fecha_activacion"
        const val USER_CALIFICACION = "calificacion"


        @SuppressLint("StaticFieldLeak")
        fun getInstance(context: Context) : SessionPrefs {
            return SessionPrefs(context)
        }
    }
}