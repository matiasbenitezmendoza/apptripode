package com.grupomulticolor.tripode.data.chat.resource

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.chat.MessageChat
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ChatService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: ChatApi

    init {
        api = retrofit.create(ChatApi::class.java)
    }

    fun get(limit: Int, offset: Int, onResponse: ResponseListener<ResponseData<ArrayList<MessageChat>>>) {
        waprogress.show()
        api.get(limit, offset).enqueue( object : Callback<ResponseData<ArrayList<MessageChat>>> {
            override fun onFailure(call: Call<ResponseData<ArrayList<MessageChat>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<MessageChat>>>,
                response: Response<ResponseData<ArrayList<MessageChat>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<MessageChat>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<MessageChat>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<MessageChat>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun post(messageChat: String, onResponse: ResponseListener<ResponseData<MessageChat>>) {
        waprogress.show()
        api.post(messageChat).enqueue( object : Callback<ResponseData<MessageChat>> {

            override fun onFailure(call: Call<ResponseData<MessageChat>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<MessageChat>>,
                response: Response<ResponseData<MessageChat>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<MessageChat>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<MessageChat>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<MessageChat>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : ChatService {
            return ChatService(context)
        }
    }
}