package com.grupomulticolor.tripode.data.padecimientos.service

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class PadecimientoService(var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: PadecimientoApi

    init {
        api = retrofit.create(PadecimientoApi::class.java)
    }

    fun get( onResponse: ResponseListener<ResponseData<ArrayList<Padecimiento>>>) {
        waprogress.show()
        api.get().enqueue( object : Callback<ResponseData<ArrayList<Padecimiento>>>{

            override fun onFailure(call: Call<ResponseData<ArrayList<Padecimiento>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Padecimiento>>>,
                response: Response<ResponseData<ArrayList<Padecimiento>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Padecimiento>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Padecimiento>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Padecimiento>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }


}