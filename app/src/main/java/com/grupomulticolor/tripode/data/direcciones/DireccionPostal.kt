package com.grupomulticolor.tripode.data.direcciones

import com.google.gson.annotations.SerializedName

class DireccionPostal (
    @SerializedName("codigo_postal") var codigoPostal : String?,
    @SerializedName("municipio") var munucipio: String?,
    @SerializedName("estado") var estado: String?,
    @SerializedName("colonias") var colonias: ArrayList<String>?
) {
}