package com.grupomulticolor.tripode.data.facturacion.source

import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.*

interface FacturacionApi {

    @GET("facturacion/facturacion")
    fun getAll() : Call<ResponseData<ArrayList<Facturacion>>>

    @POST("facturacion/facturacion")
    fun post(@Body facturacion: Facturacion) : Call<ResponseData<Facturacion>>

    @PUT("facturacion/facturacion")
    fun put(@Body facturacion: Facturacion) : Call<ResponseData<Facturacion>>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "facturacion/facturacion", hasBody = true)
    fun deleteDireccion(@Field("id") id: Int) : Call<ResponseData<Any>>
}