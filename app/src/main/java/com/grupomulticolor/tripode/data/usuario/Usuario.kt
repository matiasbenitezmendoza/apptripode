package com.grupomulticolor.tripode.data.usuario

import com.auth0.android.jwt.JWT
import com.google.gson.annotations.SerializedName

class Usuario (
    @SerializedName("nombre") var nombre: String? = "",
    @SerializedName("apellido_paterno") var apellidoPaterno: String? = "",
    @SerializedName("apellido_materno") var apellidoMaterno: String? = "",
    @SerializedName("fecha_nacimiento") var fechaNac: String? = "",
    @SerializedName("calle")var calle: String? = "",
    @SerializedName("numero_exterior") var numExt: String? = "",
    @SerializedName("numero_interior") var numInt: String? = "",
    @SerializedName("residencial") var residencialId: String? = "",
    @SerializedName("codigo_postal") var codigoPostal: String? = "",
    @SerializedName("estado") var estado: String? = "",
    @SerializedName("colonia") var colonia: String? = "",
    @SerializedName("email") var correo: String? = "",
    @SerializedName("email_confirm") var correoConfirm: String? = "",
    @SerializedName("password") var password: String? = "",
    @SerializedName("password_confirm") var passwordConfirm: String? = "",
    @SerializedName("telefono") var phone: String? = "",
    @SerializedName("padecimientos") var padecimientos: String? = "",
    @SerializedName("terminos") var terminosCondiciones : Boolean = false,
    @SerializedName("id_usuario") var idUsuario : Int? = 0,
    @SerializedName("id_openpay") var idOpenPay: String? = null,
    @SerializedName("version") var version: String? = null,
    @SerializedName("id_usuario_ldcom") var idUsuarioLdcom : Int? = 0,
    @SerializedName("ciudad") var ciudad: String? = "",
    @SerializedName("fecha_activacion") var fecha_activacion: String? = ""
) {

    fun setValuesFromToken( _token: String ) {
        if (_token.isNotEmpty()) {
            val jwt = JWT(_token)

            idUsuario = jwt.getClaim(FIELD_ID).asInt()
            idOpenPay = jwt.getClaim(FIELD_ID_OPENPAY).asString()
            nombre = jwt.getClaim(FIELD_NOMBRE).asString()
            correo = jwt.getClaim(FIEL_MAIL).asString()
            apellidoPaterno = jwt.getClaim(FIELD_APELLIDO_PATERNO).asString()
            apellidoMaterno = jwt.getClaim(FIELD_APELLIDO_MATERNO).asString()
            fechaNac = jwt.getClaim(FIELD_FECHA_NAC).asString()
            phone = jwt.getClaim(FIELD_PHONE).asString()
            padecimientos = jwt.getClaim(FIELD_PADECIMIENTOS).asString()
            version = jwt.getClaim(FIELD_VERSION).asString()
            idUsuarioLdcom = jwt.getClaim(FIELD_ID_LDCOM).asInt()
            fecha_activacion = jwt.getClaim(FIELD_FECHA).asString()
        }
    }

    companion object {
        const val FIELD_ID = "id"
        const val FIELD_ID_OPENPAY = "id_openpay"
        const val FIELD_ID_LDCOM = "id_usuario_ldcom"
        const val FIELD_NOMBRE = "nombre"
        const val FIELD_APELLIDO_PATERNO = "apellido_paterno"
        const val FIELD_APELLIDO_MATERNO = "apellido_materno"
        const val FIELD_FECHA_NAC= "fecha_nacimiento"
        const val FIEL_MAIL = "email"
        const val FIELD_PASS = "password"
        const val FIELD_FECHA = "fecha_activacion"
        const val FIELD_PHONE = "telefono"
        const val FIELD_PADECIMIENTOS = "padecimientos"
        const val FIELD_STATUS = "status"
        const val FIELD_VERSION = "version"
    }
}