package com.grupomulticolor.tripode.data.suscripciones.source

import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.suscripciones.Suscripcion
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface SuscripcionApi {

    @GET("suscripciones/suscripciones")
    fun get() : Call<ResponseData<Suscripcion>>

    @POST("suscripciones/suscripciones")
    fun post(@Body suscripcion: Suscripcion) : Call<ResponseData<Suscripcion>>

    @PUT("suscripciones/suscripciones")
    fun put(@Body suscripcion: Suscripcion) : Call<ResponseData<Suscripcion>>

}