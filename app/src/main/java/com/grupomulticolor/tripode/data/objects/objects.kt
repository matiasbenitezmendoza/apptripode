package com.grupomulticolor.tripode.data.objects

import android.content.Context
import com.grupomulticolor.tripode.BuildConfig
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.service.TokenInterceptor
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.util.config.Constants
import com.grupomulticolor.walibray.dialog.WADialogProgress
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object BuildInterceptor {

    fun getInterceptor(context: Context) : OkHttpClient{
        val token = SessionPrefs.getInstance(context).getToken()

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
            .connectTimeout(40, TimeUnit.SECONDS)
            .addInterceptor(TokenInterceptor(token ?: ""))
            .addInterceptor(loggingInterceptor)
            .readTimeout(40, TimeUnit.SECONDS)
            .build()
    }
}

object RetrofitBuilder {

    fun getRetrofit(context: Context) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.URL_HOST_WA)
            .addConverterFactory(GsonConverterFactory.create())
            .client(BuildInterceptor.getInterceptor(context))
            .build()
    }
}

object WADialogProgressBuilder {

    fun getProgress(context: Context) : WADialogProgress {
        return WADialogProgress.Builder(context)
            .setDrawable(R.drawable.isotipo)
            .build()
        /*
        return WADialogProgress.Builder(context)
            .setDrawable(R.drawable.isotipo)
            .build()*/
    }

}


object RetrofitBuilderTripode {
    fun getRetrofit(context: Context) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.URL_HOST_TRIPODE)
            .addConverterFactory(GsonConverterFactory.create())
            .client(BuildInterceptor.getInterceptor(context))
            .build()
    }
}

