package com.grupomulticolor.tripode.data.departamentos.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.departamentos.Departamento
import com.grupomulticolor.tripode.data.objects.RetrofitBuilderTripode
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class DepartamentoService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilderTripode.getRetrofit(context)
    private val waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: DepartamentoApi

    init {
        api = retrofit.create(DepartamentoApi::class.java)
    }

    fun getDepartamentos(onResponse: ResponseListener<ResponseDataTripode<ArrayList<Departamento>>>) {
        waprogress.show()
        api.get("").enqueue(object : Callback<ResponseDataTripode<ArrayList<Departamento>>> {
            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<Departamento>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<Departamento>>>,
                response: Response<ResponseDataTripode<ArrayList<Departamento>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Departamento>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Departamento>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<Departamento>>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : DepartamentoService {
            return DepartamentoService(context)
        }
    }
}