package com.grupomulticolor.tripode.data.pedido.source

import com.grupomulticolor.tripode.data.detallepedido.Detallepedido
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.pedido.Pedido
import retrofit2.Call
import retrofit2.http.*

interface PedidoApi {
    @GET("pedidos/pedidos")
    fun get(@Query("limit") limit: Int,   @Query("offset") offset: Int ) : Call<ResponseData<ArrayList<Pedido>>>

    @GET("pedidos/pedidos_finalizados")
    fun getHistorial() : Call<ResponseData<ArrayList<Pedido>>>

    @GET("pedidos/pedido_id")
    fun getPedido(@Query("id") id: Int) : Call<ResponseData<Pedido>>

    @POST("pedidos/pedidos")
    fun post(@Body pedido: Pedido) : Call<ResponseData<Pedido>>

    @POST("pedidos/detalle_pedidos")
    fun postDetalle(@Body detalle: Detallepedido) : Call<ResponseData<Detallepedido>>

    @GET("pedidos/detalle_pedidos")
    fun getDetalle(@Query("id_pedido") id_pedido: Int ) : Call<ResponseData<ArrayList<Detallepedido>>>

    @PUT("pedidos/calificar_usuario")
    fun putComentarios( @Body pedido: Pedido ) : Call<ResponseData<Pedido>>

}