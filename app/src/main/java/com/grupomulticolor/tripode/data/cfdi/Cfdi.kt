package com.grupomulticolor.tripode.data.cfdi

import com.google.gson.annotations.SerializedName

class Cfdi(
    @SerializedName("Key")
    var Id: String = "",
    @SerializedName("Desc")
    var name: String = ""
) {
    override fun toString(): String {
        return name
    }
}