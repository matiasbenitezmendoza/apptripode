package com.grupomulticolor.tripode.data.pedido.source

import com.grupomulticolor.tripode.data.detallepedido.DetalleOrden
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.*

interface OrdenesApi {

    //user/16/orders?connection=
    @GET("user/{userid}/orders")
    fun getPedidosFinalizados(
        @Path("userid") userid: Int,
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<ArrayList<Ordenes>>>

    //order/98?connection=
    @GET("order/{idorder}")
    fun getDetallePedidos(
        @Path("idorder") idorder: String,
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<ArrayList<DetalleOrden>>>

}