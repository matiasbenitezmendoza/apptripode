package com.grupomulticolor.tripode.data.pedido.source


import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.detallepedido.DetalleOrden
import com.grupomulticolor.tripode.data.objects.RetrofitBuilderTripode
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class OrdenesService(var context: Context) {

    private val retrofit: Retrofit = RetrofitBuilderTripode.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: OrdenesApi

    init {
        api = retrofit.create(OrdenesApi::class.java)
    }


    fun getPedidosFinalizados(id: Int, conection: String, onResponse: ResponseListener<ResponseDataTripode<ArrayList<Ordenes>>>) {
        waprogress.show()
        api.getPedidosFinalizados(id, conection).enqueue( object : Callback<ResponseDataTripode<ArrayList<Ordenes>>>{

            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<Ordenes>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<Ordenes>>>,
                response: Response<ResponseDataTripode<ArrayList<Ordenes>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Ordenes>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Ordenes>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<Ordenes>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun getDetallePedidos(id: String, conection: String, onResponse: ResponseListener<ResponseDataTripode<ArrayList<DetalleOrden>>>) {
        waprogress.show()
        api.getDetallePedidos(id, conection).enqueue( object : Callback<ResponseDataTripode<ArrayList<DetalleOrden>>>{

            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<DetalleOrden>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<DetalleOrden>>>,
                response: Response<ResponseDataTripode<ArrayList<DetalleOrden>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<DetalleOrden>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<DetalleOrden>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<DetalleOrden>>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : OrdenesService {
            return OrdenesService(context)
        }
    }


}