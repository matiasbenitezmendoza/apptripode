package com.grupomulticolor.tripode.data.promociones.source

import com.grupomulticolor.tripode.data.promociones.Promociones
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PromocionesApi {


    //promotions?connection=
    @GET("promotions")
    fun get(
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<ArrayList<Promociones>>>
}