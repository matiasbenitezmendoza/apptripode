package com.grupomulticolor.tripode.data.usuario.source.service

import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.usuario.Usuario
import retrofit2.Call
import retrofit2.http.*

interface UsuarioApi {

    @POST("usuarios/usuarios")
    fun post(@Body usuario: Usuario) : Call<ResponseData<Usuario>>

    @PUT("usuarios/usuarios")
    fun put(@Body usuario: Usuario) : Call<ResponseData<Usuario>>

    @PUT("usuarios/usuarios_password")
    fun putPassword(@Body newPassword: String) : Call<ResponseData<Any>>

    @GET("usuarios/usuarios_recuperar_password")
    fun getRecoverPassword(@Query("email")email: String) : Call<ResponseData<Any>>


    @GET("usuarios/token_firebase")
    fun sendToken(
        @Query("token") token: String)
            : Call<ResponseData<Usuario>>

}