package com.grupomulticolor.tripode.data.departamentos

import com.google.gson.annotations.SerializedName

class Departamento(
    @SerializedName("Id")
    var id: Int? = null,
    @SerializedName("Name")
    var nombre: String? = ""
)