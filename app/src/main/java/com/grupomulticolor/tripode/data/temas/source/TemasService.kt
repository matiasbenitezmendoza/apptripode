package com.grupomulticolor.tripode.data.temas.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.temas.Tema
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class TemasService(var context: Context) {

    private val retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: TemaApi

    init {
        api = retrofit.create(TemaApi::class.java)
    }

    fun get(onResponse: ResponseListener<ResponseData<ArrayList<Tema>>>) {
        waprogress.show()
        api.get().enqueue(object : Callback<ResponseData<ArrayList<Tema>>> {

            override fun onFailure(call: Call<ResponseData<ArrayList<Tema>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Tema>>>,
                response: Response<ResponseData<ArrayList<Tema>>>
            ) {
                waprogress.hide()
                if (response.errorBody() != null) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel =
                                ConvertBodyError<ResponseData<ArrayList<Tema>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel =
                                ConvertBodyError<ResponseData<ArrayList<Tema>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Tema>>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }

                }
            }
        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : TemasService {
            return TemasService(context)
        }
    }

}