package com.grupomulticolor.tripode.data.checkout

import com.google.gson.annotations.SerializedName

class Pago (
    @SerializedName("Nombre") var Nombre: String? = "",
    @SerializedName("NumeroTarjeta") var NumeroTarjeta: String? = "",
    @SerializedName("TarjetaMes") var TarjetaMes: Int? = 0,
    @SerializedName("TarjetaAnno") var TarjetaAnno: Int? = 0,
    @SerializedName("CodigoSeguridad") var CodigoSeguridad: Int? = 0,
    @SerializedName("TipoTarjeta") var TipoTarjeta: Int? = 0,
    @SerializedName("Monto") var Monto: Double? = 0.0,
    @SerializedName("MontoEnvio") var MontoEnvio: Double? = 0.0,
    @SerializedName("GastoID") var GastoID: Int? = 0,
    @SerializedName("FacturaElectronica") var FacturaElectronica: Boolean? = false,
    @SerializedName("TipoPagoID") var TipoPagoID: Int? = 0
    )