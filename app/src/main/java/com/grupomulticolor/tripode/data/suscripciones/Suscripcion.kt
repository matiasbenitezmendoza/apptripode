package com.grupomulticolor.tripode.data.suscripciones

import com.google.gson.annotations.SerializedName

class Suscripcion (
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("novedades")
    var novedades: Int = 0,
    @SerializedName("notificaciones")
    var notificaciones: Int = 0,
    @SerializedName("temas")
    var temas: String = ""
) {
}