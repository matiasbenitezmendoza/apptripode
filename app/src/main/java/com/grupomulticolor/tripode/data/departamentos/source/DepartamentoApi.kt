package com.grupomulticolor.tripode.data.departamentos.source

import com.grupomulticolor.tripode.data.departamentos.Departamento
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface DepartamentoApi {

    //sections/D/catalog?connection
    @GET("sections/D/catalog")
    fun get(@Query("connection") char: String)
            : Call<ResponseDataTripode<ArrayList<Departamento>>>
}