package com.grupomulticolor.tripode.data.cfdi.source


import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.cfdi.Cfdi
import com.grupomulticolor.tripode.data.objects.RetrofitBuilderTripode
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class CfdiService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilderTripode.getRetrofit(context)
    private val waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: CfdiApi

    init {
        api = retrofit.create(CfdiApi::class.java)
    }

    fun get(onResponse: ResponseListener<ResponseDataTripode<ArrayList<Cfdi>>>) {
        waprogress.show()
        api.get("").enqueue(object : Callback<ResponseDataTripode<ArrayList<Cfdi>>> {
            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<Cfdi>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<Cfdi>>>,
                response: Response<ResponseDataTripode<ArrayList<Cfdi>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Cfdi>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Cfdi>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<Cfdi>>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : CfdiService {
            return CfdiService(context)
        }
    }
}