package com.grupomulticolor.tripode.data.repartidor

import com.google.gson.annotations.SerializedName

class Repartidor (
                @SerializedName("id") var id: Int? = 0,
                @SerializedName("nombre") var nombre: String? = "",
                @SerializedName("apellido_paterno") var apellido_paterno: String? = "",
                @SerializedName("apellido_materno") var apellido_materno: String? = "",
                @SerializedName("email") var email: String? = "",
                @SerializedName("imagen") var imagen: String? = ""
)
