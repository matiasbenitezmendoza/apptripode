package com.grupomulticolor.tripode.data.slider

import com.google.gson.annotations.SerializedName


class Sliders (
    @SerializedName("descripcion") var descripcion: String? = "",
    @SerializedName("imagen") var imagen: String? = ""
)
