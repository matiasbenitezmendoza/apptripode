package com.grupomulticolor.tripode.data.menu

class ItemMenu (
    var nameMenu: String = "",
    var icon: Int = -1, // no icon
    var itemId: Int = -1,
    var isFooter: Boolean = false
)