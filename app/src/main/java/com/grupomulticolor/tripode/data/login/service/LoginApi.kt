package com.grupomulticolor.tripode.data.login.service

import com.grupomulticolor.tripode.data.login.LoginBody
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.usuario.Usuario
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("login/login")
    fun login(@Body loginBody: LoginBody) : Call<ResponseData<String>>

    @POST("usuarios/usuarios_facebook")
    fun loginWithFacebook(@Body loginBody: LoginBody) : Call<ResponseData<Usuario>>

}