package com.grupomulticolor.tripode.data.service.model

import com.google.gson.annotations.SerializedName

class ResponseDataTripode<T>(
    @SerializedName("IsSuccessful") var isSuccessful: Boolean,
    @SerializedName("Message") var message: String,
    @SerializedName("Data") var data: T? = null
)