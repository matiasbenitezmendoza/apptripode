package com.grupomulticolor.tripode.data.service

import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor(var token: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val newreq = request.newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()

        return chain.proceed(newreq)
    }
}