package com.grupomulticolor.tripode.data.service.model

interface ResponseListener<in T> {
    fun onSuccess(response: T)
    fun onError(t: Throwable)
}