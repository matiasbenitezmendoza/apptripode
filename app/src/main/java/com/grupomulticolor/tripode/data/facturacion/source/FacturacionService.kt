package com.grupomulticolor.tripode.data.facturacion.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class FacturacionService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: FacturacionApi

    init {
        api = retrofit.create(FacturacionApi::class.java)
    }

    fun getAll(onResponse: ResponseListener<ResponseData<ArrayList<Facturacion>>>) {
        waprogress.show()
        api.getAll().enqueue( object : Callback<ResponseData<ArrayList<Facturacion>>> {
            override fun onFailure(call: Call<ResponseData<ArrayList<Facturacion>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Facturacion>>>,
                response: Response<ResponseData<ArrayList<Facturacion>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Facturacion>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Facturacion>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Facturacion>>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun post(facturacion: Facturacion, onResponse: ResponseListener<ResponseData<Facturacion>>) {
        waprogress.show()
        api.post(facturacion).enqueue(object : Callback<ResponseData<Facturacion>> {

            override fun onFailure(call: Call<ResponseData<Facturacion>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Facturacion>>, response: Response<ResponseData<Facturacion>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Facturacion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Facturacion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Facturacion>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun put(facturacion: Facturacion, onResponse: ResponseListener<ResponseData<Facturacion>>) {
        waprogress.show()
        api.put(facturacion).enqueue(object : Callback<ResponseData<Facturacion>> {

            override fun onFailure(call: Call<ResponseData<Facturacion>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Facturacion>>, response: Response<ResponseData<Facturacion>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Facturacion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Facturacion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Facturacion>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun delete(id: Int, onResponse: ResponseListener<ResponseData<Any>>) {
        waprogress.show()
        api.deleteDireccion(id).enqueue(object : Callback<ResponseData<Any>> {
            override fun onFailure(call: Call<ResponseData<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Any>>, response: Response<ResponseData<Any>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Any>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : FacturacionService {
            return FacturacionService(context)
        }

    }
}