package com.grupomulticolor.tripode.data.padecimientos.service

import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.service.model.ResponseData
import retrofit2.http.GET
import retrofit2.Call

interface PadecimientoApi {

    @GET("padecimientos/padecimientos")
    fun get() : Call<ResponseData<ArrayList<Padecimiento>>>

}