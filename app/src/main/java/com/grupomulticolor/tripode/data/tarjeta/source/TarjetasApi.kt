package com.grupomulticolor.tripode.data.tarjeta.source

import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import retrofit2.Call
import retrofit2.http.*

interface TarjetasApi {

    @GET("tarjetas/tarjetas")
    fun getAll() : Call<ResponseData<ArrayList<TarjetaBancaria>>>


    @GET("tarjetas/pago")
    fun pago(@Query("id") id: Int, @Query("cantidad") cantidad: Double)
            : Call<ResponseData<Any>>

    @POST("tarjetas/tarjetas")
    fun post(@Body tarjetaBancaria: TarjetaBancaria)
            : Call<ResponseData<TarjetaBancaria>>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "tarjetas/tarjetas", hasBody = true)
    fun delete(@Field("id") id: Int) : Call<ResponseData<Any>>
}