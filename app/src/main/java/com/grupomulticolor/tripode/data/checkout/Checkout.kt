package com.grupomulticolor.tripode.data.checkout

import com.google.gson.annotations.SerializedName

class Checkout (
        @SerializedName("conexion") var conexion: String? = "",
        @SerializedName("pUbicacion") var pUbicacion: Int? = 0,
        @SerializedName("pPago") var pPago: Pago? = Pago("","****************",0,0,0,2,0.0,0.0,0,false, 2),
        @SerializedName("pSocioId") var pSocioId: Int? = 0,
        @SerializedName("pTipoEntrega") var pTipoEntrega: Int? = 0,
        @SerializedName("pSucursalRecogeId") var pSucursalRecogeId: Int? = 0,
        @SerializedName("MobileId") var MobileId: String? = ""
        //0&amp;pSucursalRecogeId=0&amp;MobileId=e6617abd10305f03
    )