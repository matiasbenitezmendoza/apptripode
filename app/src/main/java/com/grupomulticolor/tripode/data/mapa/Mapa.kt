package com.grupomulticolor.tripode.data.mapa

import com.google.gson.annotations.SerializedName


class Mapa (
    @SerializedName("latitud") var latitud: Double? = 0.0,
    @SerializedName("longitud") var longitud: Double? = 0.0
)
