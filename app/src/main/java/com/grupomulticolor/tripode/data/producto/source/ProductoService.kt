package com.grupomulticolor.tripode.data.producto.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.RetrofitBuilderTripode
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.producto.ProductoCount
import com.grupomulticolor.tripode.data.producto.ProductoPromocion
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ProductoService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilderTripode.getRetrofit(context)
    private val waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: ProductoApi

    init {
        api = retrofit.create(ProductoApi::class.java)
    }

    fun getProductosInicio(conexion: String, search: String, maximum: Int, page: Int, oa: Int, op: Int, onResponse: ResponseListener<ResponseDataTripode<ProductoCount>>) {
        waprogress.show()
        api.getProductosInicio(conexion, maximum, page).enqueue(object : Callback<ResponseDataTripode<ProductoCount>> {
            override fun onFailure(call: Call<ResponseDataTripode<ProductoCount>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ProductoCount>>,
                response: Response<ResponseDataTripode<ProductoCount>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ProductoCount>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ProductoCount>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<ProductoCount>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getProductos(conexion: String, search: String, maximum: Int, page: Int, oa: Int, op: Int, onResponse: ResponseListener<ResponseDataTripode<ProductoCount>>) {
        waprogress.show()
        api.getProductos(conexion, search, maximum, page, oa, op).enqueue(object : Callback<ResponseDataTripode<ProductoCount>> {
            override fun onFailure(call: Call<ResponseDataTripode<ProductoCount>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ProductoCount>>,
                response: Response<ResponseDataTripode<ProductoCount>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ProductoCount>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ProductoCount>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<ProductoCount>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }


    fun getProductosRelacionados( filter: String, filterId: Int, conexion: String, maximum: Int, page: Int,onResponse: ResponseListener<ResponseDataTripode<ProductoCount>>) {
        waprogress.show()
        api.getProductosRelacionados(filter, filterId, conexion, maximum, page).enqueue(object : Callback<ResponseDataTripode<ProductoCount>> {
            override fun onFailure(call: Call<ResponseDataTripode<ProductoCount>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ProductoCount>>,
                response: Response<ResponseDataTripode<ProductoCount>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ProductoCount>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ProductoCount>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<ProductoCount>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getProductosPromocion( idPromocion : Int, onResponse: ResponseListener<ResponseDataTripode<ArrayList<ProductoPromocion>>>) {
        waprogress.show()
        api.getProductosPromociones(idPromocion,"").enqueue(object : Callback<ResponseDataTripode<ArrayList<ProductoPromocion>>> {
            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<ProductoPromocion>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<ProductoPromocion>>>,
                response: Response<ResponseDataTripode<ArrayList<ProductoPromocion>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<ProductoPromocion>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<ProductoPromocion>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<ProductoPromocion>>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : ProductoService {
            return ProductoService(context)
        }
    }
}