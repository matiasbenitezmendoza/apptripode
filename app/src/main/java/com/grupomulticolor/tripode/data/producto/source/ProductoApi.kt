package com.grupomulticolor.tripode.data.producto.source


import com.grupomulticolor.tripode.data.producto.ProductoCount
import com.grupomulticolor.tripode.data.producto.ProductoPromocion
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.*


interface ProductoApi {

    //search?connection=&search=&maximum=&page=
    //search?connection=&search=banda&maximum=10&page=1&orderbyNamevalue=0&orderbyPricevalue=0
    //search?connection=&search=pas&maximum=10&page=1&orderbyNamevalue=0&orderbyPricevalue=1
    @GET("search")
    fun getProductos(
                     @Query("connection") conexion: String,
                     @Query("search") search: String,
                     @Query("maximum") maximum: Int,
                     @Query("page") page: Int,
                     @Query("orderbyNamevalue") orderbyNamevalue: Int,
                     @Query("orderbyPricevalue") orderbyPricevalue: Int

    ): Call<ResponseDataTripode<ProductoCount>>

    @GET("itemall")
    fun getProductosInicio(
        @Query("connection") conexion: String,
        @Query("maximum") maximum: Int,
        @Query("page") page: Int

    ): Call<ResponseDataTripode<ProductoCount>>

    //sections/C/detail/101?connection=&maximum=3&page=1
    @GET("sections/{filter}/detail/{filterId}")
    fun getProductosRelacionados(
        @Path("filter") filter: String,
        @Path("filterId") filterId: Int,
        @Query("connection") conexion: String,
        @Query("maximum") maximum: Int,
        @Query("page") page: Int
    ): Call<ResponseDataTripode<ProductoCount>>

    //promotion/24?connection=
    @GET("promotion/{id}")
    fun getProductosPromociones(
        @Path("id") idPromocion: Int,
        @Query("connection") conexion: String
    ): Call<ResponseDataTripode<ArrayList<ProductoPromocion>>>

}
