package com.grupomulticolor.tripode.data.menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.interfaces.Events

class ExpandableListAdapter(
    var mContext: Context, var mListDataHeader: List<ItemMenu>,
    var  mListDataChild:  HashMap<ItemMenu, List<String>>, var event: Events, var eventCategoria: Events) :
    BaseExpandableListAdapter() {

    override fun getGroup(groupPosition: Int): Any {
        return this.mListDataHeader[groupPosition]
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val itemMenu = getGroup(groupPosition) as ItemMenu
        var cnvw = convertView

        //if (cnvw == null) {
            cnvw = if (!itemMenu.isFooter) {
                val infalInflater = this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                infalInflater.inflate(R.layout.item_menu, parent, false)
            } else {
                val infalInflater = this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                infalInflater.inflate(R.layout.item_menu_footer, parent, false)
            }

        //}


        if (!itemMenu.isFooter) {
            if (groupPosition != 5) {
                cnvw!!.setOnClickListener {
                    notifyDataSetChanged()
                    event.onClick(itemMenu.itemId)
                }
            }

            val lblListHeader = cnvw!!.findViewById(R.id.titlemMenu) as TextView
            val headerIcon = cnvw.findViewById(R.id.iconMenu) as ImageView
            lblListHeader.text = itemMenu.nameMenu
            headerIcon.setImageResource(itemMenu.icon)
        }

        return cnvw!!
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        if (mListDataChild[this.mListDataHeader[groupPosition]] != null)
            return mListDataChild[this.mListDataHeader[groupPosition]]!!.size
        return 0
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return this.mListDataChild[this.mListDataHeader[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean,
                              convertView: View?, parent: ViewGroup?): View {
        val childText = getChild(groupPosition, childPosition) as String

        var cnvw = convertView

        val infalInflater = this.mContext
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        cnvw = infalInflater.inflate(R.layout.list_submenu, parent, false)

        val txtListChild = cnvw!!.findViewById(R.id.titlemMenu) as TextView
        txtListChild.text = childText

        cnvw.setOnClickListener {
            notifyDataSetChanged()
            eventCategoria.onClick(childPosition)
        }

        return cnvw!!
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return mListDataHeader.size
    }
}