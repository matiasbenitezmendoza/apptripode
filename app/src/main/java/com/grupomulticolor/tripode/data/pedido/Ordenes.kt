package com.grupomulticolor.tripode.data.pedido

import com.google.gson.annotations.SerializedName

class Ordenes (
    @SerializedName("Id") var id: Int? = 0,
    @SerializedName("SaleId") var orden: String? = "",
    @SerializedName("Total") var total: Double? = 0.0,
    @SerializedName("Date") var fecha_pedido: String? = "",
    @SerializedName("Status") var status_pedido: String? = "",
    @SerializedName("Ubicacion_Codigo_Postal") var codigo_postal: String? = "",
    @SerializedName("SubTotal") var subtotal: Double? = 0.0,
    @SerializedName("Transaccion") var Transaccion: String? = "",
    @SerializedName("NumeroFacturaFsical") var NumeroFacturaFiscal: String? = ""
)
