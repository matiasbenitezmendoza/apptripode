package com.grupomulticolor.tripode.data.cfdi.source

import com.grupomulticolor.tripode.data.cfdi.Cfdi
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CfdiApi {
    //cfdioptions?connection=
    @GET("cfdioptions")
    fun get(@Query("connection") char: String)
            : Call<ResponseDataTripode<ArrayList<Cfdi>>>
}