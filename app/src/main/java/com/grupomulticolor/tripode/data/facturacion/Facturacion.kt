package com.grupomulticolor.tripode.data.facturacion

import com.google.gson.annotations.SerializedName

class Facturacion(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("rfc")
    var rfc: String = "",
    @SerializedName("razon_social")
    var nombre_razonSocial: String = "",
    @SerializedName("nombre")
    var nombreReferencia: String = "",
    @SerializedName("calle")
    var calle: String = "",
    @SerializedName("numero_exterior")
    var noExt: String = "",
    @SerializedName("numero_interior")
    var noInt: String = "",
    @SerializedName("residencial")
    var residencial: String = "",
    @SerializedName("codigo_postal")
    var codigoPostal: String = "",
    @SerializedName("estado")
    var estado: String = "",
    @SerializedName("colonia")
    var colonia: String = "",
    @SerializedName("municipio")
    var municipio: String = "",
    @SerializedName("referencia")
    var referencias: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("cfdi_name")
    var cfdi_name: String = "",
    @SerializedName("cfdi")
    var cfdi: String = "",
    @SerializedName("pNivel1")
    var pNivel1: Int = 0,
    @SerializedName("pNivel2")
    var pNivel2: Int = 0,
    @SerializedName("pNivel3")
    var pNivel3: Int = 0,
    @SerializedName("seleccionada")
    var selected: Boolean = false
) {
}