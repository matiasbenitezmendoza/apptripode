package com.grupomulticolor.tripode.data.carrito

import com.google.gson.annotations.SerializedName

class Carrito (
    @SerializedName("Id") var id: Int? = 0,
    @SerializedName("Cart_Id") var cardId: Int? = 0,
    @SerializedName("Item_Id") var itemId: String? = "",
    @SerializedName("Name") var nombre: String? = "",
    @SerializedName("Price") var precio: Double? = 0.0,
    @SerializedName("Quantity") var cantidad: Int? = 0,
    @SerializedName("SubTotal") var subTotal: Double? = 0.0,
    @SerializedName("Total") var total: Double? = 0.0,
    @SerializedName("Image") var imagen: String? = "",
    @SerializedName("Presentation") var detalle: String? = ""

)
