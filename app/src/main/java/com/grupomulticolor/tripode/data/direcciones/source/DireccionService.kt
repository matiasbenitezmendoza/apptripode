package com.grupomulticolor.tripode.data.direcciones.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.direcciones.DireccionPostal
import com.grupomulticolor.tripode.data.direcciones.Estados
import com.grupomulticolor.tripode.data.direcciones.Colonias
import com.grupomulticolor.tripode.data.direcciones.Municipios
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class DireccionService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: DireccionApi

    init {
        api = retrofit.create(DireccionApi::class.java)
    }

    fun getEstados(onResponse: ResponseListener<ResponseData<ArrayList<Estados>>>){
        waprogress.show()
        api.getDirEdos().enqueue( object : Callback<ResponseData<ArrayList<Estados>>>{
            override fun onFailure(call: Call<ResponseData<ArrayList<Estados>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Estados>>>,
                response: Response<ResponseData<ArrayList<Estados>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Estados>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Estados>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Estados>>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getMunicipios(mun: String,onResponse: ResponseListener<ResponseData<ArrayList<Municipios>>>){
        waprogress.show()
        api.getDirMun(mun).enqueue( object : Callback<ResponseData<ArrayList<Municipios>>>{
            override fun onFailure(call: Call<ResponseData<ArrayList<Municipios>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Municipios>>>,
                response: Response<ResponseData<ArrayList<Municipios>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Municipios>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Municipios>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Municipios>>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getColonias(est: String,mun: String,onResponse: ResponseListener<ResponseData<ArrayList<Colonias>>>){
        waprogress.show()
        api.getDirCol(est,mun).enqueue( object : Callback<ResponseData<ArrayList<Colonias>>>{
            override fun onFailure(call: Call<ResponseData<ArrayList<Colonias>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Colonias>>>,
                response: Response<ResponseData<ArrayList<Colonias>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Colonias>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Colonias>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Colonias>>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getDireccionPostal(codigoPostal: String, onResponse: ResponseListener<ResponseData<DireccionPostal>>) {
        waprogress.show()
        api.getDireccionPostal(codigoPostal).enqueue( object : Callback<ResponseData<DireccionPostal>>{
            override fun onFailure(call: Call<ResponseData<DireccionPostal>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<DireccionPostal>>,
                response: Response<ResponseData<DireccionPostal>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<DireccionPostal>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<DireccionPostal>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<DireccionPostal>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getDireccionesUsuario(onResponse: ResponseListener<ResponseData<ArrayList<Direccion>>>) {
        waprogress.show()
        api.getDireccionesUser().enqueue( object : Callback<ResponseData<ArrayList<Direccion>>> {
            override fun onFailure(call: Call<ResponseData<ArrayList<Direccion>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<ArrayList<Direccion>>>,
                response: Response<ResponseData<ArrayList<Direccion>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Direccion>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<ArrayList<Direccion>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<ArrayList<Direccion>>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun postDireccion(direccion: Direccion, onResponse: ResponseListener<ResponseData<Direccion>>) {
        waprogress.show()
        api.postDireccion(direccion).enqueue(object : Callback<ResponseData<Direccion>> {

            override fun onFailure(call: Call<ResponseData<Direccion>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Direccion>>, response: Response<ResponseData<Direccion>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Direccion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Direccion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Direccion>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun putDireccion(direccion: Direccion, onResponse: ResponseListener<ResponseData<Direccion>>) {
        waprogress.show()
        api.putDireccion(direccion).enqueue(object : Callback<ResponseData<Direccion>> {

            override fun onFailure(call: Call<ResponseData<Direccion>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Direccion>>, response: Response<ResponseData<Direccion>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Direccion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Direccion>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Direccion>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun deleteDireccion(id: Int, onResponse: ResponseListener<ResponseData<Any>>) {
        waprogress.show()
        api.deleteDireccion(id).enqueue(object : Callback<ResponseData<Any>> {
            override fun onFailure(call: Call<ResponseData<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Any>>, response: Response<ResponseData<Any>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Any>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }


    companion object {
        @Synchronized fun instance(context: Context) : DireccionService {
            return DireccionService(context)
        }
    }
}