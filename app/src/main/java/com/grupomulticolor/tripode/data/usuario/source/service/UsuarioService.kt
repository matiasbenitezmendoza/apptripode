package com.grupomulticolor.tripode.data.usuario.source.service

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class UsuarioService(var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: UsuarioApi

    init {
        api = retrofit.create(UsuarioApi::class.java)
    }

    fun post(usuario: Usuario, onResponse: ResponseListener<ResponseData<Usuario>>) {
        waprogress.show()
        api.post(usuario).enqueue(object : Callback<ResponseData<Usuario>> {
            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Usuario>>,
                                    response: Response<ResponseData<Usuario>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun put(usuario: Usuario, onResponse: ResponseListener<ResponseData<Usuario>>) {
        waprogress.show()
        api.put(usuario).enqueue(object : Callback<ResponseData<Usuario>> {

            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Usuario>>, response: Response<ResponseData<Usuario>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Usuario>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun recoverPass(email: String, onResponse: ResponseListener<ResponseData<Any>>) {
        waprogress.show()
        api.getRecoverPassword(email).enqueue(object : Callback<ResponseData<Any>> {

            override fun onFailure(call: Call<ResponseData<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Any>>, response: Response<ResponseData<Any>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<Any>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<Any>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Any>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun sendToken(token: String, onResponse: ResponseListener<ResponseData<Usuario>>) {
        waprogress.show()
        api.sendToken(token).enqueue( object : Callback<ResponseData<Usuario>>{
            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<Usuario>>,
                response: Response<ResponseData<Usuario>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Usuario>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Usuario>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        if (responseModel.status)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }
    companion object {
        @Synchronized fun instance(context: Context) : UsuarioService {
            return UsuarioService(context)
        }
    }
}