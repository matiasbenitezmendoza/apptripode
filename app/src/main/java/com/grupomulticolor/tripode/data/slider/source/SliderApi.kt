package com.grupomulticolor.tripode.data.slider.source

import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.slider.Sliders
import retrofit2.Call
import retrofit2.http.GET


interface SliderApi {

    @GET("sliders/sliders")
    fun get() : Call<ResponseData<ArrayList<Sliders>>>

}