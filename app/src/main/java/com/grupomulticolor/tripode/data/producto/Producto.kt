package com.grupomulticolor.tripode.data.producto

import com.google.gson.annotations.SerializedName

class Producto (
                @SerializedName("Name") var nombre: String? = "",
                @SerializedName("Presentation") var detalle: String? = "",
                @SerializedName("Image") var img: String? = "",
                @SerializedName("offer_type") var marca: String? = "",
                @SerializedName("Id") var itemNum: String? = "",
                @SerializedName("Price") var precio: Double? = 0.0,
                @SerializedName("offer_price") var descuento: Double? = 0.0,
                @SerializedName("cantidad") var cantidad: Int? = 0,
                @SerializedName("CategoryId") var category_Id: Int? = 0,
                @SerializedName("Category") var caregory: String? = "",
                @SerializedName("PromotionName") var PromotionName: String? = "",
                @SerializedName("PromotionType") var PromotionType: String? = "",
                @SerializedName("HasLoyaltyPlan") var HasLoyaltyPlan: Boolean? = false
                )
