package com.grupomulticolor.tripode.data.programa.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.RetrofitBuilderTripode
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.programa.ProgramaLealtad
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ProgramaLealtadService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilderTripode.getRetrofit(context)
    private val waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: ProgramaLealtadApi

    init {
        api = retrofit.create(ProgramaLealtadApi::class.java)
    }

    fun get(id: Int, onResponse: ResponseListener<ResponseDataTripode<ArrayList<ProgramaLealtad>>>) {
        waprogress.show()
        api.get(id,"").enqueue(object : Callback<ResponseDataTripode<ArrayList<ProgramaLealtad>>> {
            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<ProgramaLealtad>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<ProgramaLealtad>>>,
                response: Response<ResponseDataTripode<ArrayList<ProgramaLealtad>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<ProgramaLealtad>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<ProgramaLealtad>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<ProgramaLealtad>>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : ProgramaLealtadService {
            return ProgramaLealtadService(context)
        }
    }
}