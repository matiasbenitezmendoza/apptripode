package com.grupomulticolor.tripode.data.carrito.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.objects.RetrofitBuilderTripode
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class CarritoService(var context: Context) {
    private val retrofit: Retrofit = RetrofitBuilderTripode.getRetrofit(context)
    private val waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: CarritoApi

    init {
        api = retrofit.create(CarritoApi::class.java)
    }

    fun getIdcarrito(socio_Id: Int,  conexion: String, onResponse: ResponseListener<ResponseDataTripode<Int>>) {
        waprogress.show()
        api.getIdcarrito(socio_Id, conexion).enqueue(object : Callback<ResponseDataTripode<Int>> {
            override fun onFailure(call: Call<ResponseDataTripode<Int>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<Int>>,
                response: Response<ResponseDataTripode<Int>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Int>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Int>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<Int>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun getCarrito(shoppingcart: Int,conexion: String, onResponse: ResponseListener<ResponseDataTripode<ArrayList<Carrito>>>) {
        waprogress.show()
        api.getCarrito(shoppingcart, conexion).enqueue(object : Callback<ResponseDataTripode<ArrayList<Carrito>>> {
            override fun onFailure(call: Call<ResponseDataTripode<ArrayList<Carrito>>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<ArrayList<Carrito>>>,
                response: Response<ResponseDataTripode<ArrayList<Carrito>>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Carrito>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<ArrayList<Carrito>>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<ArrayList<Carrito>>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun addCarrito(shoppingcart: Int, item: String, quantity: Int, conexion: String, onResponse: ResponseListener<ResponseDataTripode<Any>>) {
        waprogress.show()
        api.addCarrito(shoppingcart, item, quantity, conexion).enqueue(object : Callback<ResponseDataTripode<Any>> {
            override fun onFailure(call: Call<ResponseDataTripode<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<Any>>,
                response: Response<ResponseDataTripode<Any>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<Any>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun updateCarrito(shoppingcart: Int, item: String, quantity: Int, conexion: String, onResponse: ResponseListener<ResponseDataTripode<Any>>) {
        waprogress.show()
        api.updateCarrito(shoppingcart, item, quantity, conexion).enqueue(object : Callback<ResponseDataTripode<Any>> {
            override fun onFailure(call: Call<ResponseDataTripode<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<Any>>,
                response: Response<ResponseDataTripode<Any>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<Any>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun deleteCarrito(shoppingcart: Int, item: String, conexion: String, onResponse: ResponseListener<ResponseDataTripode<Any>>) {
        waprogress.show()
        api.deleteCarrito(shoppingcart, item, conexion).enqueue(object : Callback<ResponseDataTripode<Any>> {
            override fun onFailure(call: Call<ResponseDataTripode<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseDataTripode<Any>>,
                response: Response<ResponseDataTripode<Any>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {

                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {

                    try {
                        val responseModel = response.body() as ResponseDataTripode<Any>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable(responseModel.message))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    fun checkout(conexion: String, pUbicacion: Int, pPago: String, pSocioId: Int, pTipoEntrega: Int, pSucursalRecogeId: Int, onResponse: ResponseListener<ResponseDataTripode<Any>>) {
        waprogress.show()
        api.checkout(conexion, pUbicacion, pPago, pSocioId, pTipoEntrega, pSucursalRecogeId).enqueue(object : Callback<ResponseDataTripode<Any>> {

            override fun onFailure(call: Call<ResponseDataTripode<Any>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseDataTripode<Any>>, response: Response<ResponseDataTripode<Any>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseDataTripode<Any>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_500)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseDataTripode<Any>
                        if (responseModel.isSuccessful)
                            onResponse.onSuccess(responseModel)
                        else
                            onResponse.onError(Throwable("Datos no validos"))
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }
        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : CarritoService {
            return CarritoService(context)
        }
    }
}