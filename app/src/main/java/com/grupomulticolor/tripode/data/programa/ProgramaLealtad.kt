package com.grupomulticolor.tripode.data.programa

import com.google.gson.annotations.SerializedName

class ProgramaLealtad (
    @SerializedName("UserId") var id: String? = "",
    @SerializedName("Id") var num_socio: String? = "",
    @SerializedName("CashBack") var saldo: String? = "",
    @SerializedName("PrimaryCard") var card: Boolean? = false,
    @SerializedName("fecha") var fecha: String? = "",
    @SerializedName("nombre") var nombre: String? = ""
)

