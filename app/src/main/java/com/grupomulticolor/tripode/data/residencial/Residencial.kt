package com.grupomulticolor.tripode.data.residencial

import com.google.gson.annotations.SerializedName



class Residencial (
    @SerializedName("id") var id: Int? = 0,
    @SerializedName("nombre") var nombre: String = "",
    @SerializedName("latitud") var latitud: String? = "",
    @SerializedName("longitud") var longitud: String? = ""
) {
    override fun toString(): String {
        return nombre
    }
}


