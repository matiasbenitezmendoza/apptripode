package com.grupomulticolor.tripode.data.temas.source

import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.temas.Tema
import retrofit2.Call
import retrofit2.http.GET

interface TemaApi {

    @GET("temas/temas")
    fun get() : Call<ResponseData<ArrayList<Tema>>>

}