package com.grupomulticolor.tripode.data.direcciones

import com.google.gson.annotations.SerializedName

class Estados(
    @SerializedName("Id")
    var Id: Int = 0,
    @SerializedName("Name")
    var name: String = ""
) {
    override fun toString(): String {
        return name
    }
}