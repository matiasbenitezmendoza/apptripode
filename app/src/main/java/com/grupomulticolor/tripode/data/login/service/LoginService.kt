package com.grupomulticolor.tripode.data.login.service

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.login.LoginBody
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class LoginService (var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)
    private var api: LoginApi = retrofit.create(LoginApi::class.java)

    fun post( loginBody: LoginBody, onResponse: ResponseListener<ResponseData<String>>) {
        waprogress.show()
        api.login(loginBody).enqueue( object : Callback<ResponseData<String>>{

            override fun onFailure(call: Call<ResponseData<String>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<String>>, response: Response<ResponseData<String>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<String>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<String>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<String>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    fun facebook( loginBody: LoginBody, onResponse: ResponseListener<ResponseData<Usuario>>) {
        waprogress.show()
        api.loginWithFacebook(loginBody).enqueue( object : Callback<ResponseData<Usuario>>{

            override fun onFailure(call: Call<ResponseData<Usuario>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(call: Call<ResponseData<Usuario>>, response: Response<ResponseData<Usuario>>) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Usuario>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Usuario>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Usuario>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }
}