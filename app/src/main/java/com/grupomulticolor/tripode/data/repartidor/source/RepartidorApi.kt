package com.grupomulticolor.tripode.data.repartidor.source

import com.grupomulticolor.tripode.data.repartidor.Repartidor
import com.grupomulticolor.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RepartidorApi {

    @GET("repartidores/repartidores")
    fun get(@Query("id") id: Int ) : Call<ResponseData<Repartidor>>

}