package com.grupomulticolor.tripode.data.facturacion

import com.google.gson.annotations.SerializedName

class Factura (
    @SerializedName("Emp_Id") var id: Int = 0,
    @SerializedName("Suc_Id") var suc_id: Int = 0,
    @SerializedName("NumeroFactura") var numero_factura: String = "",
    @SerializedName("NumeroFacturaGlobal") var numero_factura_global: String = "",
    @SerializedName("Serie") var serie: String = "",
    @SerializedName("NumeroFacturaDevolucion") var numero_factura_devolucion: String = "",
    @SerializedName("Mensaje_Error") var msj_error: String = "",
    @SerializedName("Codigo_Error") var codigo_error: Int = 0,
    @SerializedName("PDF") var pdf: String = "",
    @SerializedName("XML") var xml: String = ""
    )