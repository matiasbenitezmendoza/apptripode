package com.grupomulticolor.tripode.data.producto

import com.google.gson.annotations.SerializedName

class ProductoCount (
    @SerializedName("Count") var count: Int? = 0,
    @SerializedName("Items") var items: ArrayList<Producto>? = null
)