package com.grupomulticolor.tripode.data.repartidor.source

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.RetrofitBuilder
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.repartidor.Repartidor
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.ConvertBodyError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepartidorService(var context: Context) {

    private var retrofit: Retrofit = RetrofitBuilder.getRetrofit(context)
    private var waprogress = WADialogProgressBuilder.getProgress(context)

    private var api: RepartidorApi

    init {
        api = retrofit.create(RepartidorApi::class.java)
    }

    fun get(id: Int, onResponse: ResponseListener<ResponseData<Repartidor>>) {
        waprogress.show()
        api.get(id).enqueue( object : Callback<ResponseData<Repartidor>>{

            override fun onFailure(call: Call<ResponseData<Repartidor>>, t: Throwable) {
                waprogress.hide()
                onResponse.onError(t)
            }

            override fun onResponse(
                call: Call<ResponseData<Repartidor>>,
                response: Response<ResponseData<Repartidor>>
            ) {
                waprogress.hide()
                if ( response.errorBody() != null ) {
                    when {
                        response.code() in 400..499 -> {
                            val responseModel = ConvertBodyError<ResponseData<Repartidor>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(responseModel?.message))
                        }
                        response.code() in 500..599 -> {
                            val responseModel = ConvertBodyError<ResponseData<Repartidor>>().fromResponseBody(response.errorBody()!!)
                            onResponse.onError(Throwable(context.getString(R.string.error_terms)))
                        }
                        else -> onResponse.onError(Throwable(context.getString(R.string.error_unknown)))
                    }
                } else {
                    try {
                        val responseModel = response.body() as ResponseData<Repartidor>
                        onResponse.onSuccess(responseModel)
                    } catch (e: Exception) {
                        onResponse.onError(Throwable(e.message))
                    }
                }
            }

        })
    }

    companion object {
        @Synchronized fun instance(context: Context) : RepartidorService {
            return RepartidorService(context)
        }
    }


}