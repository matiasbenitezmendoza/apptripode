package com.grupomulticolor.tripode.data.direcciones.source

import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.*

interface DireccionApi {

    @GET("direcciones/direcciones_cp")
    fun getDireccionPostal(
        @Query("codigo_postal") codigoPostal: String)
            : Call<ResponseData<DireccionPostal>>

    @GET("direcciones/direcciones")
    fun getDireccionesUser() : Call<ResponseData<ArrayList<Direccion>>>

    @GET("direcciones/direcciones_estados")
    fun getDirEdos() : Call<ResponseData<ArrayList<Estados>>>

    @GET("direcciones/direcciones_municipios")
    fun getDirMun(
        @Query("Id") est: String): Call<ResponseData<ArrayList<Municipios>>>

    @GET("direcciones/direcciones_colonias")
    fun getDirCol(
        @Query("Id") est: String,
        @Query("Id_municipio") mun: String
    ): Call<ResponseData<ArrayList<Colonias>>>

    @POST("direcciones/direcciones")
    fun postDireccion(@Body direccion: Direccion) : Call<ResponseData<Direccion>>

    @PUT("direcciones/direcciones")
    fun putDireccion(@Body direccion: Direccion) : Call<ResponseData<Direccion>>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "direcciones/direcciones", hasBody = true)
    fun deleteDireccion(@Field("id") id: Int) : Call<ResponseData<Any>>

}