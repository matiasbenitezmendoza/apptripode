package com.grupomulticolor.tripode.data.detallepedido

import com.google.gson.annotations.SerializedName

class DetalleOrden (
    @SerializedName("DetailId") var id: Int? = 0,
    @SerializedName("Item") var id_producto: String? = "",
    @SerializedName("Shipping") var id_carrito: Int? = 0,
    @SerializedName("SaleId") var orden: String? = "",
    @SerializedName("ItemName") var producto: String? = "",
    @SerializedName("Quantity") var cantidad: Int? = 0,
    @SerializedName("Price") var precio: Double? = 0.0,
    @SerializedName("Total") var precio_total: Double? = 0.0
)

