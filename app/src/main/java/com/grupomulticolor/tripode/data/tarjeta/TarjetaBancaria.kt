package com.grupomulticolor.tripode.data.tarjeta

import com.google.gson.annotations.SerializedName

class TarjetaBancaria(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("nombre")
    var nombreCuentaHabiente: String = "",
    @SerializedName("numero")
    var noTarjeta: String = "",
    @SerializedName("cvv")
    var cvv: String = "",
    @SerializedName("mes_ven")
    var mesVen: Int = 0,
    @SerializedName("anio_ven")
    var anioVen: Int = 0,
    @SerializedName("token_id")
    var token: String = "",
    @SerializedName("device_session_id")
    var deviceId: String ="",
    @SerializedName("tipo")
    var tipo: String ="",
    var nombreReferencia: String = "",
    var selected: Boolean = false
) {

    fun detalle() : String {
        return "$nombreCuentaHabiente\n" +
                "**** **** **** $noTarjeta\n${getFechaVen()}"
    }

    private fun getFechaVen() : String {
        var strFechaVen = ""

        strFechaVen += if (mesVen < 10)
            "0$mesVen"
        else
            "$mesVen"
        strFechaVen += if (anioVen < 10)
            "/0$anioVen"
        else
            "/$anioVen"
        return strFechaVen
    }
}