package com.grupomulticolor.tripode.data.detallepedido

import com.google.gson.annotations.SerializedName

class Detallepedido(
                    @SerializedName("id") var id: Int? = 0,
                    @SerializedName("id_pedido") var id_pedido: Int? = 0,
                    @SerializedName("id_producto") var id_producto: String? = "",
                    @SerializedName("id_carrito") var id_carrito: Int? = 0,
                    @SerializedName("orden") var orden: String? = "",
                    @SerializedName("producto") var producto: String? = "",
                    @SerializedName("cantidad") var cantidad: Int? = 0,
                    @SerializedName("precio") var precio: Double? = 0.0,
                    @SerializedName("precio_total") var precio_total: Double? = 0.0
                    )