package com.grupomulticolor.tripode.data.promociones

import com.google.gson.annotations.SerializedName

class Promociones (
    @SerializedName("PromoId") var id: Int? = 0,
    @SerializedName("AgencyId") var agency: Int? = 0,
    @SerializedName("RefundProvider") var provider: String? = "",
    @SerializedName("RefundType") var tipo: String? = "",
    @SerializedName("CuponTypeName") var cuponNombre: String? = "",
    @SerializedName("FinalDate") var fecha_final: String? = "",
    @SerializedName("InitialDate") var fecha_inicial: String? = "",
    @SerializedName("CuponType") var cuponTipo: Int? = 0,
    @SerializedName("Name") var nombre: String? = ""
)

