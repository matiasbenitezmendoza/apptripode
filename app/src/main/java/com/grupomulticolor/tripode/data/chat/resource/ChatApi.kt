package com.grupomulticolor.tripode.data.chat.resource

import com.grupomulticolor.tripode.data.chat.MessageChat
import com.grupomulticolor.tripode.data.service.model.ResponseData
import retrofit2.Call
import retrofit2.http.*

interface ChatApi {

    @GET("chats/chats")
    fun get(@Query("limit") limit: Int, @Query("offset") offset: Int)
            : Call<ResponseData<ArrayList<MessageChat>>>

    @FormUrlEncoded
    @POST("chats/chats")
    fun post(@Field("mensaje") messageChat: String) : Call<ResponseData<MessageChat>>

}