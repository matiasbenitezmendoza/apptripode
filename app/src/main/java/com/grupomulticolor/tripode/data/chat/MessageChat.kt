package com.grupomulticolor.tripode.data.chat

import com.google.gson.annotations.SerializedName

// value -1 to admin

class MessageChat(
    @SerializedName("id_chat") var idChat: Int,
    @SerializedName("id_usuario") var idUsuario: Int,
    @SerializedName("mensaje") var message: String,
    @SerializedName("respuesta") var receptor: Int,
    @SerializedName("fecha") var fecha: String
)