package com.grupomulticolor.tripode

import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.data.departamentos.Departamento
import com.grupomulticolor.tripode.data.menu.ItemMenu
import com.grupomulticolor.tripode.data.pedido.Pedido

class MainPresenter(val mMainView : MainContract.View,
                    val mMainInteractor: MainInteractor) : MainContract.Presenter, MainInteractor.Callback  {



    init {
        mMainView.setPresenter(this)
    }

    /* implementacion de contract presenter*/

    override fun start() {
        // mostrar fragmentActual
        mMainInteractor.crearMenu(this)
    }

    override fun openOrCloseDrawableMenu(cerrado: Boolean) {
        mMainInteractor.openOrCloseDrawerLayout(cerrado, this)
    }

    override fun onItemMenuSelected(idItemMenu: Int) {
        mMainInteractor.menuItemSelected(idItemMenu, this)
    }

    /* implementacion de callback interactor */
    override fun openDrawer() {
        mMainView.openDrawable()
    }

    override fun closeDrawer() {
        mMainView.closeDrawable()
    }

    override fun cambiarFragment(fragment: Fragment) {
        mMainView.mostrarFragment(fragment)
    }

    override fun onEvaluarBusqueda(text: String) {
        if (!TextUtils.isEmpty(text) && text.length > 1)
            mMainView.irABuscar(text)
    }

    override fun sendToken(token: String) {
        mMainInteractor.sendToken(token, this)
    }

    override fun enviarToken() {
        Log.d("TAG", "Enviado")
    }

    override fun limpiarPila() {
        mMainView.clearStack()
    }

    override fun onClickLlamanos() {
        closeDrawer()
        mMainView.intentLlamar()
    }

    override fun loadMenu(listDataHeader: ArrayList<ItemMenu>, listDataChild: HashMap<ItemMenu, List<String>>,
                          listDeptos: ArrayList<Departamento>) {
        mMainView.cargarMenu(listDataHeader, listDataChild, listDeptos)
    }

    override fun onScanCode() {
        mMainView.readBarcode()
    }


    override fun actualizarComentario(id: Int, comentario : String, calificacion : String) {
        var pedido : Pedido = Pedido()
        pedido.id = id
        pedido.comentarios_usuario = comentario
        pedido.calificacion_usuario = calificacion
        mMainInteractor.actualizarPedidoComentarios(pedido, this)
    }


    override fun onRequestScoreSucces(pedido: Pedido) {
        Log.d("TAG", "Se envio los comentarios")
        mMainView.showCalificacion()
    }


    override fun onRequestFailed() {
        mMainView.muestrarError()
    }

}