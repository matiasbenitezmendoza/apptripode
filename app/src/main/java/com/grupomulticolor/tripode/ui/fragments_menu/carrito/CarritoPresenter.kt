package com.grupomulticolor.tripode.ui.fragments_menu.carrito

import android.content.Context
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.carrito.source.CarritoService
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.util.Network
import java.lang.Exception

class CarritoPresenter(val mCarritoView: CarritoContract.View, var context: Context) : CarritoContract.Presenter  {
    private val service: CarritoService = CarritoService.instance(context)

    init {
        mCarritoView.setPresenter(this)
    }

    override fun start() {
        val id_carrito = SessionPrefs.getInstance(context).getIdSC()
        requestProductosEnCarrito(id_carrito)
    }

    override fun requestProductosEnCarrito(id: Int) {

        service.getCarrito( id,"" , object : ResponseListener<ResponseDataTripode<ArrayList<Carrito>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<Carrito>>) {
                try {
                    mCarritoView.showProductos(response.data as ArrayList<Carrito>)
                } catch (e: Exception) {
                    e.printStackTrace()
                    var carrito = ArrayList<Carrito>()
                    mCarritoView.showProductos(carrito)
                }
            }

            override fun onError(t: Throwable) {
                var carrito = ArrayList<Carrito>()
                mCarritoView.showProductos(carrito)
            }


        })
    }

    override fun requestEliminarProducto(id: Int, item: String, carrito: ArrayList<Carrito>) {
        service.deleteCarrito( id ,item, "", object : ResponseListener<ResponseDataTripode<Any>> {
            override fun onSuccess(response: ResponseDataTripode<Any>) {
                try {
                    mCarritoView.showEliminar()
                } catch (e: Exception) {
                    e.printStackTrace()
                    mCarritoView.showErrorEliminar(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                mCarritoView.showErrorEliminar(t.message!!)
            }
        })
    }

    override fun intentaProcederPago(carrito: ArrayList<Carrito>) {
        //val id_carrito = SessionPrefs.getInstance(context).getIdSC()

        /*
        for (l in carrito) {
            service.updateCarrito( id_carrito, l.itemId!!, l.cantidad!! ,"", object : ResponseListener<ResponseDataTripode<Any>> {
                override fun onSuccess(response: ResponseDataTripode<Any>) {

                }
                override fun onError(t: Throwable) {
                }
            })
        }*/


        mCarritoView.irTarjetas(carrito)
    }

    override fun recargarCarrito(carrito: ArrayList<Carrito>) {
        val id_carrito = SessionPrefs.getInstance(context).getIdSC()

        var num = 1

        for (l in carrito) {
            service.updateCarrito( id_carrito, l.itemId!!, l.cantidad!! ,"", object : ResponseListener<ResponseDataTripode<Any>> {
                override fun onSuccess(response: ResponseDataTripode<Any>) {
                     if(num == carrito.size){
                         mCarritoView.showRecargarProductos()
                     }
                    num += 1
                }
                override fun onError(t: Throwable) {
                    mCarritoView.showErrorActualizar(t.message!!)
                }
            })
        }


    }

    override fun intentaProgramarPedido() {
    }

    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mCarritoView.showNetworkError()
        return isAvailable
    }

}