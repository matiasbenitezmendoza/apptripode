package com.grupomulticolor.tripode.ui.fragments_menu.mensajes

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.chat.MessageChat
import com.grupomulticolor.tripode.data.chat.resource.ChatService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener


class CentroMensajesPresenter(var mView: CentroMensajesContract.View,
                              var context: Context
) : CentroMensajesContract.Presenter {

    val service: ChatService = ChatService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        loadMessages(20, 0)
    }

    override fun loadMessages(limit: Int, offset: Int) {
        service.get(limit, offset, object : ResponseListener<ResponseData<ArrayList<MessageChat>>> {
            override fun onSuccess(response: ResponseData<ArrayList<MessageChat>>) {
                try {
                    mView.showMessages( response.data as ArrayList<MessageChat>)
                } catch (e : Exception) {
                    mView.showMessageError(context.getString(R.string.error_parseo_de_datos))
                }
            }
            override fun onError(t: Throwable) {
                mView.showMessageError(t.message)
            }
        })
    }

    override fun sendMessage(messageChat: String) {
        service.post(messageChat, object : ResponseListener<ResponseData<MessageChat>> {

            override fun onSuccess(response: ResponseData<MessageChat>) {
                try {
                    mView.showChatSend( response.data as MessageChat)
                } catch (e: Exception) {
                    mView.showMessageError(context.getString(R.string.error_parseo_de_datos))
                }
            }
            override fun onError(t: Throwable) {
                mView.showMessageError(t.message)
            }
        })
    }


}