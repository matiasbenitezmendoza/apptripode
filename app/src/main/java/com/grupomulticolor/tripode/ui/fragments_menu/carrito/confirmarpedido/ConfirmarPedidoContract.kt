package com.grupomulticolor.tripode.ui.fragments_menu.carrito.confirmarpedido

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.detallepedido.Detallepedido
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria

interface ConfirmarPedidoContract {

    interface View: BaseView<Presenter> {
        fun cancelar()
        fun showConectionError()
        fun showPago(status: Int)
        fun setOrden(orden: String)
        fun intentarAgregarDetalle(pedido: Pedido)
        fun showPedidoFinalizado()
        fun showErrorPedido(msj: String)
        fun showErrorDetallePedido(msj: String)
        fun showErrorPago()
        fun showErrorOrden()
        fun showProductos(carrito: ArrayList<Carrito>)

    }

    interface Presenter: BasePresenter {
        fun requestProductosEnCarrito(id: Int)
        fun requestDetallesProducto(detallepedidos: ArrayList<Detallepedido>)
        fun intentaFinalizarCompra(pedido: Pedido)
        fun agregarDireccionFiscal()
        fun requestPagoCompra(tarjetaBancaria: TarjetaBancaria?, cantidad: Double)
        fun requestOrden(conexion: String, pUbicacion: Int, pPago: String, pSocioId: Int, pTipoEntrega: Int, pSucursalRecogeId: Int)

    }

}