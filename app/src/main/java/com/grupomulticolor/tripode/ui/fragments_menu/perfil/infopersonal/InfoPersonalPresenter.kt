package com.grupomulticolor.tripode.ui.fragments_menu.perfil.infopersonal

import com.grupomulticolor.tripode.data.usuario.Usuario

class InfoPersonalPresenter (val mInfoPersonalContractView: InfoPersonalContract.View,
                             val mInfoPersonalInteractor: InfoPersonalInteractor)
    : InfoPersonalContract.Presenter, InfoPersonalInteractor.Callback {

    init {
        mInfoPersonalContractView.setPresenter(this)
    }

    override fun start() {
        mInfoPersonalInteractor.requestInformacionPersonal(this)
    }

    override fun consultarInformacionUsuario() {
        mInfoPersonalInteractor.requestInformacionPersonal(this)
    }


    override fun intentaActualizarUsuario(user: Usuario) {

    }

    override fun onRequestSuccess(usuario: Usuario) {
        mInfoPersonalContractView.mostrarInformacionUsuario(usuario)
    }

    override fun onRequestFailed() {
        mInfoPersonalContractView.mostrarError("")
    }

    override fun onNetworkConnectionFailed() {
        mInfoPersonalContractView.muestrarErrorConexion()
    }

}