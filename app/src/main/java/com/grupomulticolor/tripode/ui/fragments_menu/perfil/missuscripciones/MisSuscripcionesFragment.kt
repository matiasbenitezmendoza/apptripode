package com.grupomulticolor.tripode.ui.fragments_menu.perfil.missuscripciones

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.suscripciones.Suscripcion
import com.grupomulticolor.tripode.data.temas.Tema
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_mis_suscripciones.*

class MisSuscripcionesFragment : FragmentChildNotFab(), SuscripcionesContract.View {


    private var mPresenter: SuscripcionesContract.Presenter? = null
    private lateinit var vw: View
    private var mTemas: ArrayList<Tema> = ArrayList()
    private var mSuscripcion: Suscripcion = Suscripcion()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_mis_suscripciones, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        SuscripcionPresenter(this, context!!)
        initEvents()
    }

    private fun initEvents() {
        btnBack.setOnClickListener {
            enviarDatos()
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun enviarDatos() {
        mSuscripcion.notificaciones = if (checkboxNotificaciones.isChecked) 1 else 2
        mSuscripcion.novedades = if (checkboxNovedades.isChecked) 1 else 2

        var temas = ""
        val temasSelect = chipCloud.chipsSelected()
        for (i in 0 until temasSelect.size) {
            temas += if (i+1  < temasSelect.size) {
                "${temasSelect[i]},"
            } else
                temasSelect[i]
        }
        mSuscripcion.temas = temas

        mPresenter?.intentaSuscribirme(mSuscripcion)
    }

    override fun irAtras() {
        (context as MainActivity).irAtras()
    }

    override fun mostrarSuscripcion(suscripcion: Suscripcion?) {
        if (suscripcion != null) {
            mSuscripcion = suscripcion
            checkboxNovedades.isChecked = mSuscripcion.novedades != 2
            checkboxNotificaciones.isChecked = mSuscripcion.notificaciones != 2

            val arrSusc = suscripcion.temas.split(",")

            if (arrSusc.isNotEmpty()) {
                for ( sus in arrSusc) {
                    for (i in 0 until mTemas.size) {
                        if (sus == mTemas[i].nombre) {
                            chipCloud.setSelectedChip(i)
                        }
                    }
                }
            }
        }
    }

    override fun mostrarError(msg: String?, resolve: () -> Unit) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Advertencia")
            .setMessage(msg ?: context!!.getString(R.string.error_unknown))
            .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar), null)
            .setButtonPositive("Salir", object : WADialog.OnClickListener {
                override fun onClick() {
                    resolve()
                }
            }).build()
        dialog.show()
    }

    override fun mostrarErrorConexion() {
        (context as MainActivity).mostrarFragment(SinConexionFragment.instance())
    }

    override fun mostrarTemas(temas: ArrayList<Tema>) {
        mTemas.clear()
        mTemas.addAll(temas)

        for (tema in mTemas) {
            chipCloud.addChip(tema.nombre)
        }

    }

    override fun setPresenter(presenter: SuscripcionesContract.Presenter) {
        mPresenter = presenter
    }

    companion object {

        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = MisSuscripcionesFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }

    }
}