package com.grupomulticolor.tripode.ui.fragments_menu.categorias

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleContract
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoPresenter
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab
import kotlinx.android.synthetic.main.fragment_categorias.resultado
import kotlinx.android.synthetic.main.fragment_inicio.mRecyclerView

class CategoriasFragment : FragmentParentNotFab(), CategoriasContract.View {

    private lateinit var mAdapter: CategoriasAdapter
    private var mproductos = ArrayList<Producto>()
    private lateinit var vw: View
    private var mPresenter: CategoriasContract.Presenter? = null

    private var busqueda: Int = 0
    private var departamento: String = ""

    private var limit = 20
    private var offset = 1
    private var aptoParaCargar = true
    private var detalle = false
    private var total = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_categorias, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CategoriasPresenter(this, context!!)
        ViewCompat.setNestedScrollingEnabled(vw,false)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        if(!detalle){
            mPresenter?.getproductos(busqueda,limit, offset)
        }
    }

    private fun initAdapter() {

        if(!detalle){
            mproductos.clear()
            limit = 20
            offset = 1
            aptoParaCargar = true
            resultado.text = "Buscando producto ..."
        }else{
            resultado.text = "Resultado de la búsqueda “" + departamento + "”: " + total + " Productos"
        }

        mAdapter = CategoriasAdapter(mproductos!!,  object : CategoriasAdapter.ListenerView {
            override fun onClick(item: Producto) {
                goDetalleProducto(item)
            }
        })

        mRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(context)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)

        mRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1
                    if (aptoParaCargar) {
                        if (  pastVisibleItem >= totalItemCount) {
                            aptoParaCargar = false
                            offset += 1
                            mPresenter?.getproductos(busqueda,limit, offset)
                        }
                    }
                }

            }


        })


    }

    private fun goDetalleProducto(producto: Producto) {
        setDetalle(true)
        DetalleProductoPresenter(
            DetalleProductoFragment.instance() as DetalleContract.View, producto, context!!)
        fragmentTransaction(DetalleProductoFragment.instance())
    }

    override fun muestraErrorEnConexion() {
        Snackbar.make(vw, "Sin conexion a internet", Snackbar.LENGTH_LONG).show()
    }

    override fun muestraErrorDePeticion(error: String) {
        if(mproductos.isEmpty()) {
            Snackbar.make(vw, "No se encontraron productos", Snackbar.LENGTH_LONG).show()
        }
    }

    override fun muestrarErrorAlCargar(err: String) {
        if(mproductos.isEmpty()) {
            Snackbar.make(vw, "No se encontraron productos", Snackbar.LENGTH_LONG).show()
        }
    }

    override fun agregarProductosFilterList(productos: ArrayList<Producto>, t: Int) {
        this.total = t
        if(mproductos.isEmpty()) {
            resultado.text = "Resultado de la búsqueda “" + departamento + "”: " + total + " Productos"
        }
        aptoParaCargar = true
        mproductos.addAll(productos)
        mAdapter.notifyDataSetChanged()
    }

    override fun setPresenter(presenter: CategoriasContract.Presenter) {
        mPresenter = presenter
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    fun setBusqueda(c : Int){
        this.busqueda = c
    }

    fun setDepartamento(c : String){
        this.departamento = c
    }

    fun setDetalle(b : Boolean){
        this.detalle = b
    }


    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = CategoriasFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}