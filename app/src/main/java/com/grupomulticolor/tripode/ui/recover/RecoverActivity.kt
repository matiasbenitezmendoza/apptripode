package com.grupomulticolor.tripode.ui.recover

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.ui.login.LoginActivity
import io.fabric.sdk.android.Fabric

class RecoverActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this@RecoverActivity, Crashlytics())
        setContentView(R.layout.activity_recover_pass)

        val fragment = RecoverFragment.getInstance() as RecoverFragment
        supportFragmentManager.beginTransaction()
            .add(R.id.recover_container, fragment)
            .commit()

        val loginInteractor = RecoverInteractor(this@RecoverActivity)
        RecoverPresenter(fragment, loginInteractor)
    }

    override fun onBackPressed() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

}