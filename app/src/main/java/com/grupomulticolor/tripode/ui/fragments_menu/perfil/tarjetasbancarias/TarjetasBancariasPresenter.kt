package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancarias

import android.content.Context
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.data.tarjeta.source.TarjetasService
import com.grupomulticolor.tripode.util.Network

class TarjetasBancariasPresenter(val mView: TarjetasBancariasContract.View, var context: Context)
    : TarjetasBancariasContract.Presenter{

    private val service = TarjetasService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        loadTarjetas()
    }

    override fun loadTarjetas() {
        if (isNetworkAvalible()) {
            service.getAll(object : ResponseListener<ResponseData<ArrayList<TarjetaBancaria>>> {

                override fun onSuccess(response: ResponseData<ArrayList<TarjetaBancaria>>) {
                    try {
                        mView.showTarjetas(response.data as ArrayList<TarjetaBancaria>)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mView.showEmpty()
                    }
                }

                override fun onError(t: Throwable) {
                    mView.showEmpty()
                }
            })
        }
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mView.showNetworkError()
        return isAvailable
    }

    override fun eliminarTarjeta(itemId: Int) {
        if (isNetworkAvalible()) {
            service.delete(itemId, object : ResponseListener<Any> {
                override fun onSuccess(response: Any) {
                    loadTarjetas()
                }

                override fun onError(t: Throwable) {
                    mView.showMessageError(t.message)
                }

            })
        }
    }
}