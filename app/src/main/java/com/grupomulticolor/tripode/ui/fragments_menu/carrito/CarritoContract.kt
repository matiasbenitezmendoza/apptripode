package com.grupomulticolor.tripode.ui.fragments_menu.carrito

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.carrito.Carrito

interface CarritoContract {

    interface View : BaseView<Presenter> {
        fun showRecargarProductos()
        fun showProductos(carrito: ArrayList<Carrito>)
        fun showEliminar()
        fun showErrorEliminar(error: String)
        fun showErrorActualizar(error: String)
        fun showNetworkError()
        fun irTarjetas(carrito: ArrayList<Carrito>)
    }

    interface Presenter : BasePresenter {
        fun requestProductosEnCarrito(id: Int)
        fun recargarCarrito(carrito: ArrayList<Carrito>)
        fun intentaProcederPago(carrito: ArrayList<Carrito>)
        fun intentaProgramarPedido()
        fun requestEliminarProducto(id : Int, item: String, carrito: ArrayList<Carrito>)
    }
}