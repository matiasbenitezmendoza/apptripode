package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direcciones

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar.AddDireccionInteractor
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar.AddDireccionPresenter
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar.AgregarDireccionFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar.DireccionAgregarContract
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildWithFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_direcciones.*

class DireccionesFragment : FragmentChildWithFab(), DireccionesMainContract.View {

    private var mPresenter: DireccionesMainContract.Presenter? = null
    private var mAdapter: DireccionesAdapter? = null
    private lateinit var vw: View
    private var direcciones = ArrayList<Direccion>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_direcciones, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initEvents()
        DireccionesPresenter(this, context!!)
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initAdapter() {
        mAdapter = DireccionesAdapter(direcciones, object : Events {
            // editar
            override fun onClick(itemId: Int) {
                val addFrag = AgregarDireccionFragment.instance() as AgregarDireccionFragment
                addFrag.setData(direcciones[itemId])
                val interactor = AddDireccionInteractor(context!!)
                AddDireccionPresenter(addFrag as DireccionAgregarContract.View, interactor)
                goAdd(addFrag)
            }
        }, object : Events{
            override fun onClick(itemId: Int) {
                eliminarElemento(itemId)
            }
        })
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
    }

    private fun initEvents() {
        btnBack.setOnClickListener {
            (context as MainActivity).irAtras()
        }
    }

    private fun goAdd(fragment: AgregarDireccionFragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    private fun eliminarElemento(position: Int) {
        var msg = context!!.getString(R.string.mensaje_delete_element)
        msg = msg.replace("item", direcciones[position].nombreReferencia)
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Advertencia")
            .setMessage(msg)
            .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar), null)
            .setButtonPositive(context!!.getString(R.string.mensaje_btn_estoy_seguro), object : WADialog.OnClickListener {
                override fun onClick() {
                    mPresenter?.eliminarDireccion(direcciones[position].id)
                }
            }).build()
        dialog.show()
    }

    override fun actionFab() {
        val addFrag = AgregarDireccionFragment.instance() as AgregarDireccionFragment
        addFrag.setData(Direccion())
        goAdd(AgregarDireccionFragment.instance() as AgregarDireccionFragment)
    }

    override fun showDirecciones(direcciones: ArrayList<Direccion>) {
        this.direcciones.clear()
        this.direcciones.addAll(direcciones)
        mAdapter?.notifyDataSetChanged()
    }

    override fun showErrorNetwork() {
        (context as MainActivity).mostrarFragment(SinConexionFragment.instance())
    }

    override fun showEmpty() {
        this.direcciones.clear()
        mAdapter?.notifyDataSetChanged()
    }

    override fun setPresenter(presenter: DireccionesMainContract.Presenter) {
        mPresenter = presenter
    }

    override fun showMessageError(err: String?) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err ?: context!!.getString(R.string.mensaje_error_desconocido))

            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DireccionesFragment()

            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}