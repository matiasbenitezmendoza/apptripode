package com.grupomulticolor.tripode.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.grupomulticolor.tripode.R
import io.fabric.sdk.android.Fabric

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this@LoginActivity, Crashlytics())
        setContentView(R.layout.activity_login)

        val loginFragment = LoginFragment.getInstance() as LoginFragment
        supportFragmentManager.beginTransaction()
                .add(R.id.login_container, loginFragment)
                .commit()

        val loginInteractor = LoginInteractor(this@LoginActivity)
        LoginPresenter(loginFragment, loginInteractor)
    }
}