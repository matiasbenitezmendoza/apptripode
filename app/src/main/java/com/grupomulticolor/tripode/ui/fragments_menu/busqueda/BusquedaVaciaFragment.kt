package com.grupomulticolor.tripode.ui.fragments_menu.busqueda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.core.view.ViewCompat
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab
import kotlinx.android.synthetic.main.fragment_busqueda_v.*


class BusquedaVaciaFragment : FragmentParentNotFab() {

    private lateinit var vw: View

    private var busqueda = "pastillas"
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_busqueda_v, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setNestedScrollingEnabled(vw,false)
        mensaje_sin_p.text =
            "Tu búsqueda “" + busqueda + "” no arrojo resultados. Realiza otra búsqueda con criterios diferentes. "
    }


    fun setBusqueda(c : String){
        this.busqueda = c
    }


    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = BusquedaVaciaFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}