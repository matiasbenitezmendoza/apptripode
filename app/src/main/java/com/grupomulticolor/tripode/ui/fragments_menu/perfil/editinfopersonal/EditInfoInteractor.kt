package com.grupomulticolor.tripode.ui.fragments_menu.perfil.editinfopersonal

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.padecimientos.service.PadecimientoService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.data.usuario.source.service.UsuarioService
import com.grupomulticolor.tripode.util.Fecha
import com.grupomulticolor.tripode.util.Network
import com.grupomulticolor.tripode.util.config.Constants
import java.lang.Exception
import java.util.*
import java.util.regex.Pattern

class EditInfoInteractor(var context: Context) {

    private val padecimientosService: PadecimientoService = PadecimientoService(context)
    private val userService: UsuarioService = UsuarioService.instance(context)

    fun actualizarDatosUsuario(userData: Usuario, callback: Callback) {
        if (!isValidNombre(userData.nombre, callback)) return
        if (!isValidApellidoP(userData.apellidoPaterno, callback)) return
        if (!isValidApellidoM(userData.apellidoMaterno, callback)) return
        if (!isValidFechaNac(userData.fechaNac, callback)) return
        if (!isValidEmail(userData.correo, callback)) return
        if (!isValidPasswd(userData.password, callback)) return
        if (!isValidPhone(userData.phone, callback)) return
        if (!isNetworkAvalible(callback)) return

        actualizarDatos(userData, callback)
    }

    fun presentarDatosdeUsuario(callback: Callback) {
        val usu = SessionPrefs(context).currentUsu()
        callback.presentarUsuarioInfo(usu)
    }

    fun getPadecimientos(callback: Callback) {
        consultarPadecimientos(callback)
    }

    /* consultar los padecimientos */
    private fun consultarPadecimientos(callback: Callback) {

        if (isNetworkAvalible(callback)) {
            padecimientosService.get( object : ResponseListener<ResponseData<ArrayList<Padecimiento>>> {
                override fun onSuccess(response: ResponseData<ArrayList<Padecimiento>>) {
                    callback.onRequestPadecimientosSucces(response.data as ArrayList<Padecimiento>)
                }

                override fun onError(t: Throwable) {
                    callback.onRequestFailed(t.message!!)
                }
            })
        }

    }

    private fun actualizarDatos(userData: Usuario, callback: Callback) {

        val arrayFechaNac = userData.fechaNac?.split("/")
        var fecha = ""
        if (arrayFechaNac != null && arrayFechaNac.size == 3)
            fecha = "${arrayFechaNac[2]}-${arrayFechaNac[1]}-${arrayFechaNac[0]}"

        userData.fechaNac = fecha

        if (isNetworkAvalible(callback)) {
            userService.put(userData, object : ResponseListener<ResponseData<Usuario>> {
                override fun onSuccess(response: ResponseData<Usuario>) {
                    val token = response.token
                    if (token != null)
                        SessionPrefs(context).saveToken(token)

                    callback.onUpdateSuccess(context.getString(R.string.mensaje_datos_actualizados))
                }

                override fun onError(t: Throwable) {
                    callback.onUpdateFailed(t.message!!)
                }
            })
        }

    }

    private fun isValidPasswd(passwd: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(passwd)) {
            return true
        }

        val patron = Pattern.compile(Constants.patternPasswd)
        if (!patron.matcher(passwd).matches()) {
            callback.onPasswdError("Si desea mantener su contraseña actual deje vacio este campo")
            return false
        }
        return true
    }

    /* verifica que el nombre ingresado sea correcto*/
    private fun isValidNombre(nombre: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(nombre)) {
            callback.onNombreError(context.getString(R.string.error_item_empty))
            return false
        }
        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(nombre).matches()) {
            callback.onNombreError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* verifica que el apellido paterno ingresado sea correcto*/
    private fun isValidApellidoP(apellidoPaterno: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(apellidoPaterno)) {
            callback.onApellidoPaternoError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(apellidoPaterno).matches()) {
            callback.onApellidoPaternoError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el apellido materno ingresado sea correcto*/
    private fun isValidApellidoM(apellidoMaterno: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(apellidoMaterno)) {
            callback.onApellidoMaternoError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(apellidoMaterno).matches()) {
            callback.onApellidoMaternoError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que la fecha de nacimiento ingresada sea correcta*/
    private fun isValidFechaNac(fechaNac: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(fechaNac)) {
            callback.onFechaNacError(context.getString(R.string.error_item_empty))
            return false
        }

        /* TODO reglas fecha nac */
        val arrFech = fechaNac?.split("/")

        if( arrFech?.size != 3) {
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }
        if (arrFech[2].length != 4) { // anio no es de 4 digitos
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }
        if (arrFech[1].length != 2) { // mes no es de 2 digitos
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }
        if (arrFech[0].length != 2) { // dia no es de 2 digitos
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }

        val anioActual = Fecha.currentYear().toInt()
        val anioNac = arrFech[2].toInt()

        if (anioNac + 12 >= anioActual ) { // menor 1 anio
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }

        try {
            val year = arrFech[2].toInt()
            val month = arrFech[1].toInt()
            val dayOfMonth = arrFech[0].toInt()

            val calendar = Calendar.getInstance()
            calendar.isLenient = false
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month - 1) // [0,...,11]
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val date = calendar.time
            Log.d("date", ""+date)
        } catch (e : Exception) {
            e.printStackTrace()
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el correo ingresado sea valido*/
    private fun isValidEmail(email: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(email)) {
            callback.onCorreoError(context.getString(R.string.error_item_empty))
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onCorreoError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* valida el numero telefonico */
    private fun isValidPhone(phone: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(phone)) {
            callback.onPhoneError(context.getString(R.string.error_item_empty))
            return false
        }

        if (!Patterns.PHONE.matcher(phone).matches()) {
            callback.onPhoneError(context.getString(R.string.text_error_phone))
            return false
        }
        return true
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }


    interface Callback {
        fun onNombreError(err: String)
        fun onApellidoPaternoError(err: String)
        fun onApellidoMaternoError(err: String)
        fun onFechaNacError(err: String)
        fun onCorreoError(err: String)
        fun onPhoneError(err: String)
        fun onPasswdError(err: String)
        fun onNetworkConnectionFailed()
        fun onUpdateSuccess(msg: String)
        fun onUpdateFailed(err: String)
        fun presentarUsuarioInfo(usuario: Usuario)
        fun onRequestFailed(err: String)
        fun onRequestPadecimientosSucces(padecimientos: ArrayList<Padecimiento>)
    }
}