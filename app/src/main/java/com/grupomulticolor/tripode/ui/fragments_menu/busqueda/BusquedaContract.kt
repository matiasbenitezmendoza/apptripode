package com.grupomulticolor.tripode.ui.fragments_menu.busqueda


import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.producto.Producto

interface BusquedaContract {

    interface View : BaseView<Presenter> {
        fun muestraErrorEnConexion()
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)
        fun agregarProductosFilterList(productos: ArrayList<Producto>, total: Int)
    }

    interface Presenter : BasePresenter {
        fun getproductos(search: String, limit: Int, offset : Int, oa: Int, op: Int)
    }


}