package com.grupomulticolor.tripode.ui.fragments_menu.productos


import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.slider.Sliders


class ProductosPresenter( val mProductoView : ProductosContract.View,
                        val mProductoInteractor: ProductosInteractor) : ProductosContract.Presenter, ProductosInteractor.Callback {

    init {
        mProductoView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
        mProductoInteractor.getSlider(this)
    }

    override fun getproductos(search: String, limit: Int, offset: Int, oa: Int, op: Int) {
        mProductoInteractor.getProductos(search, limit, offset,oa, op, this)
    }

    override fun getproductosinicio(search: String, limit: Int, offset: Int, oa: Int, op: Int) {
        mProductoInteractor.getProductosInicio(search, limit, offset,oa, op, this)
    }

    override fun getslider() {
        mProductoInteractor.getSlider(this)
    }


    override fun getcarrito(id : Int) {
        mProductoInteractor.getcarrito(id, this)
    }


    override fun onNetworkConnectionFailed() {
        mProductoView.muestraErrorEnConexion()
    }


    override fun onProductosSuccess(msg: String) {
        mProductoView.muestraMensajesSuccess(msg)
    }

    override fun onSlidersSuccess(msg: String) {
        mProductoView.muestraMensajesSuccess(msg)
    }

    override fun onCarritoSuccess(msg: String) {
        mProductoView.muestraMensajesSuccess(msg)
    }

    override fun onProductosFailed(err: String) {
        mProductoView.muestraErrorDePeticion(err)
    }

    override fun onCarritoFailed(err: String) {
        mProductoView.muestrarCarritoVacio(err)
    }
    override fun onRequestFailed(err: String) {
        mProductoView.muestrarErrorAlCargar(err)
    }

    override fun onRequestProductosSucces(productos: ArrayList<Producto>) {
        mProductoView.agregarProductosFilterList(productos)
    }

    override fun onRequestSlidersSucces(imagenes: ArrayList<Sliders>) {
        mProductoView.agregarSlidersFilterList(imagenes)
    }

    override fun onRequestCarritoSucces(carrito: ArrayList<Carrito>) {
        mProductoView.agregarNumeroItem(carrito)
    }



}