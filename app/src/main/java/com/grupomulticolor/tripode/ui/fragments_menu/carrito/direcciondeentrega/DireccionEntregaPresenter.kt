package com.grupomulticolor.tripode.ui.fragments_menu.carrito.direcciondeentrega

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.direcciones.source.DireccionService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.Network
import java.lang.Exception

class DireccionEntregaPresenter(var mView: DireccionEntregaContract.View,
                                var context: Context)
    : DireccionEntregaContract.Presenter {

    private val service = DireccionService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        requestDireccionesDelUsuario()
    }

    override fun requestDireccionesDelUsuario() {
        if (isNetworkAvalible()) {
            service.getDireccionesUsuario(object : ResponseListener<ResponseData<ArrayList<Direccion>>> {
                override fun onSuccess(response: ResponseData<ArrayList<Direccion>>) {
                    try {
                        mView.showDireccionesDeUsuario(response.data as ArrayList<Direccion>)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        // mView.showMessageError(e.message)
                        mView.showEmpty()
                    }
                }

                override fun onError(t: Throwable) {
                    // mView.showMessageError(t.message)
                    mView.showEmpty()
                }
            })
        }
    }

    override fun intentContinuar(idDir: Int) {
        if (idDir != -1) {
            mView.irConfirmarPedido()
        } else {
            val msg = context.getString(R.string.mensaje_error_sin_direccion)
            mView.showError(msg)
        }
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mView.showErrorNetwork()
        return isAvailable
    }


}