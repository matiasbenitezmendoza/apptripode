package com.grupomulticolor.tripode.ui.tutorial.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.R

class StepRecibeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_step_recibe, container, false)
    }

    companion object {
        fun instace() : Fragment = StepRecibeFragment()
    }
}