package com.grupomulticolor.tripode.ui.fragments_menu.programa_lealtad

import android.content.Context
import com.grupomulticolor.tripode.data.programa.ProgramaLealtad
import com.grupomulticolor.tripode.data.programa.source.ProgramaLealtadService
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs


class ProgramaLealtadPresenter(var mView: ProgramaLealtadContract.View,
                              var context: Context
) : ProgramaLealtadContract.Presenter {

    val service: ProgramaLealtadService = ProgramaLealtadService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        loadSocio()
    }

    override fun loadSocio() {

        val id_socio = SessionPrefs.getInstance(context).getIdLdcom()
        service.get(id_socio, object : ResponseListener<ResponseDataTripode<ArrayList<ProgramaLealtad>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<ProgramaLealtad>>) {

                var datos = response.data as ArrayList<ProgramaLealtad>

                if(datos.isEmpty()){
                    var socio = ProgramaLealtad()
                    socio.fecha = SessionPrefs.getInstance(context).getFechaActivacion()
                    socio.nombre = SessionPrefs.getInstance(context).getNombreUsuario()
                    socio.num_socio = "Sin numero"
                    socio.saldo = "0.00"

                    mView.showSocio(socio)
                }else{
                    var socio: ProgramaLealtad = datos[0]
                    socio.fecha = SessionPrefs.getInstance(context).getFechaActivacion()
                    socio.nombre = SessionPrefs.getInstance(context).getNombreUsuario()
                    mView.showSocio(socio)
                }
            }

            override fun onError(t: Throwable) {
                var socio = ProgramaLealtad()
                socio.fecha = SessionPrefs.getInstance(context).getFechaActivacion()
                socio.nombre = SessionPrefs.getInstance(context).getNombreUsuario()
                socio.num_socio = "Sin numero"
                socio.saldo = "0.00"
                mView.showSocio(socio)
            }

        })

    }



}