package com.grupomulticolor.tripode.ui.registro

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.ui.login.LoginActivity
import io.fabric.sdk.android.Fabric





class RegistroActivity: AppCompatActivity() {

    private var b : Boolean = true
    private var registroFragment : RegistroFragment = RegistroFragment(Usuario())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this@RegistroActivity, Crashlytics())
        setContentView(R.layout.activity_registro)
        var name = getNombre()
        var email = getEmail()
        var apellido = getApellido()
        var usuario : Usuario = Usuario()
        if(name != null){
            usuario.nombre = name
        }
        if(email != null){
            usuario.correo = email
        }
        if(apellido != null){
            usuario.apellidoPaterno = apellido
        }

         registroFragment = RegistroFragment.getInstance(usuario) as RegistroFragment

        supportFragmentManager.beginTransaction()
            .add(R.id.registro_container, registroFragment)
            .commit()

        val loginInteractor = RegistroInteractor(this@RegistroActivity)
        RegistroPresenter(registroFragment, loginInteractor)
    }

    override fun onBackPressed() {
        if(b){
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }else{
            registroFragment.regresarRegistro()
        }
    }

    fun getNombre(): String {
        val bundle = intent.extras
        return bundle.get("usuario").toString()
    }

    fun getEmail(): String {
        val bundle = intent.extras
        return bundle.get("email").toString()
    }

    fun getApellido(): String {
        val bundle = intent.extras
        return bundle.get("apellido").toString()
    }

    fun setBack(login : Boolean){
        this.b = login
    }

}