package com.grupomulticolor.tripode.ui.fragments_menu.perfil.editinfopersonal

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.usuario.Usuario

interface EditInfoContract {

    interface View : BaseView<Presenter> {
        fun presentarDatos(usuario: Usuario)
        fun mostrarErrorEnNombre(err: String)
        fun mostrarErrorEnApellidoPaterno(err: String)
        fun mostrarErrorEnApellidoMaterno(err: String)
        fun mostrarErrorEnFechaDeNacimiento(err: String)
        fun mostrarErrorEnCorreoElectronico(err: String)
        fun mostrarErrorEnTelefono(err: String)
        fun mostrarErrorEnConexion()
        fun muestraErrorEnPasswd(error: String)

        fun muestraMensajeSucces(msg: String)
        fun muestrarMensajeError(err: String)
        fun agregarPadecimientosFilterList(padecimientos: ArrayList<Padecimiento>)
    }

    interface Presenter : BasePresenter {
        fun intentaActulizarDatosDeUsuario(usuario: Usuario)
    }

}