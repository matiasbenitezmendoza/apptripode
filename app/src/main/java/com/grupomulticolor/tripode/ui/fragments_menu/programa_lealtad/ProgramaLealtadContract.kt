package com.grupomulticolor.tripode.ui.fragments_menu.programa_lealtad

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.programa.ProgramaLealtad

interface ProgramaLealtadContract {

    interface View: BaseView<Presenter> {
        fun showSocio(socio : ProgramaLealtad)
        fun showMessageError(err: String?)
        fun showNetworkError()
    }

    interface Presenter : BasePresenter {
        fun loadSocio()
    }
}