package com.grupomulticolor.tripode.ui.fragments_menu.carrito.confirmarpedido

import android.content.Context
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.carrito.source.CarritoService
import com.grupomulticolor.tripode.data.detallepedido.Detallepedido
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.pedido.source.PedidoService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.data.tarjeta.source.TarjetasService
import java.lang.Exception

class ConfirmarPedidoPresenter(var mContractView: ConfirmarPedidoContract.View, var context: Context) : ConfirmarPedidoContract.Presenter {

    private val service: PedidoService = PedidoService.instance(context)
    private val serviceTarjeta: TarjetasService = TarjetasService.instance(context)
    private val serviceCarrito: CarritoService = CarritoService.instance(context)

    init {
        mContractView.setPresenter(this)
    }

    override fun start() {
        val id_carrito = SessionPrefs.getInstance(context).getIdSC()
        requestProductosEnCarrito(id_carrito)
    }

    override fun requestPagoCompra(tarjetaBancaria: TarjetaBancaria?, cantidad: Double) {
        if(tarjetaBancaria != null) {
            serviceTarjeta.pago(tarjetaBancaria.id, cantidad, object : ResponseListener<ResponseData<Any>> {
                override fun onSuccess(response: ResponseData<Any>) {
                    try {
                        mContractView.showPago(1)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mContractView.showErrorPago()
                    }
                }
                override fun onError(t: Throwable) {
                    mContractView.showErrorPago()
                }
            })
        }else{
            mContractView.showPago(2)
        }
    }

    override fun requestOrden(conexion: String, pUbicacion: Int, pPago: String, pSocioId: Int, pTipoEntrega: Int, pSucursalRecogeId: Int) {
        serviceCarrito.checkout("", pUbicacion, pPago, pSocioId, pTipoEntrega, pSucursalRecogeId,  object : ResponseListener<ResponseDataTripode<Any>> {

            override fun onSuccess(response: ResponseDataTripode<Any>) {
                mContractView.setOrden(response.data as String)
            }

            override fun onError(t: Throwable) {
                mContractView.showErrorOrden()

            }

        })


    }

    override fun intentaFinalizarCompra(pedido: Pedido) {
        service.post(pedido, object : ResponseListener<ResponseData<Pedido>> {
            override fun onSuccess(response: ResponseData<Pedido>) {
                try {
                    mContractView.intentarAgregarDetalle(response.data as Pedido)
                } catch (e: Exception) {
                    e.printStackTrace()
                    mContractView.showErrorPedido(e.message!!)
                }
            }

            override fun onError(t: Throwable) {
                mContractView.showErrorPedido(t.message!!)
            }
        })
    }

    override fun requestProductosEnCarrito(id: Int) {

        serviceCarrito.getCarrito( id,"" , object : ResponseListener<ResponseDataTripode<ArrayList<Carrito>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<Carrito>>) {
                try {
                    mContractView.showProductos(response.data as ArrayList<Carrito>)
                } catch (e: Exception) {
                    e.printStackTrace()
                    var carrito = ArrayList<Carrito>()
                    mContractView.showProductos(carrito)
                }
            }

            override fun onError(t: Throwable) {
                var carrito = ArrayList<Carrito>()
                mContractView.showProductos(carrito)
            }


        })
    }


    override fun agregarDireccionFiscal() {
    }

    override fun requestDetallesProducto(detallepedidos: ArrayList<Detallepedido>) {
        var bandera = true
        for (dpedido in detallepedidos) {
                    service.postDetalle(dpedido, object : ResponseListener<ResponseData<Detallepedido>> {
                        override fun onSuccess(response: ResponseData<Detallepedido>) {
                            try {
                            } catch (e: Exception) {
                                e.printStackTrace()
                                bandera = false
                            }
                        }
                        override fun onError(t: Throwable) {
                                bandera = false
                        }
                    })
        }
        if(bandera){
            mContractView.showPedidoFinalizado()
        }else{
            mContractView.showErrorDetallePedido("Error")
        }
    }
}