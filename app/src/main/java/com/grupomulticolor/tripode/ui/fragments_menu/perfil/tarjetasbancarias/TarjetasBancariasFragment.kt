package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancarias

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar.AgregarTarjetaFragment
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildWithFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_tarjetas.*

class TarjetasBancariasFragment : FragmentChildWithFab(), TarjetasBancariasContract.View {


    private lateinit var vw: View
    private var mPresenter: TarjetasBancariasContract.Presenter? = null
    private var tarjetas = ArrayList<TarjetaBancaria>()
    private var mAdapter: TarjetasAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
         vw = inflater.inflate(R.layout.fragment_tarjetas, container, false)
        return  vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        TarjetasBancariasPresenter(this, context!!)
        initAdapter()
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initEvents() {
        btnBack.setOnClickListener { (context as MainActivity).irAtras() }
    }

    private fun initAdapter() {
        mAdapter = TarjetasAdapter(tarjetas, object : Events {
            override fun onClick(itemId: Int) {
                mPresenter?.eliminarTarjeta(tarjetas[itemId].id)
            }
        })
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
    }

    override fun actionFab() {
        (context as MainActivity).mostrarFragment(AgregarTarjetaFragment.instance())
    }

    override fun showTarjetas(tarjetas: ArrayList<TarjetaBancaria>) {
        this.tarjetas.clear()
        this.tarjetas.addAll(tarjetas)
        mAdapter?.notifyDataSetChanged()
    }

    override fun showNetworkError() {
        (context as MainActivity).mostrarFragment(SinConexionFragment.instance())
    }

    override fun setPresenter(presenter: TarjetasBancariasContract.Presenter) {
        mPresenter = presenter
    }

    override fun showEmpty() {

    }

    override fun showMessageError(err: String?) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("¡Error!")
            .setMessage(err ?: "")
            .setButtonPositive("Ok", null).build()
        dialog.show()
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = TarjetasBancariasFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}