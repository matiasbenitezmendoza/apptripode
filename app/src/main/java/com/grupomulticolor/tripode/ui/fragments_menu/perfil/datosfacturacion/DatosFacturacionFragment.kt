package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datosfacturacion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add.AddFacturacionContract
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add.AddFacturacionInteractor
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add.AddFacturacionPresenter
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add.AgregarFacturacionFragment
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildWithFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_tarjetas.*

class DatosFacturacionFragment : FragmentChildWithFab(), DatosFacturacionContract.View {

    private lateinit var vw: View
    private var mPresenter: DatosFacturacionContract.Presenter? = null
    private var datosfacturacion = ArrayList<Facturacion>()
    private var mAdapter: DatosFacturacionAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_facturacion, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DatosFacturacionPresenter(this, context!!)
        initAdapter()
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initAdapter() {
        mAdapter = DatosFacturacionAdapter(datosfacturacion, object : Events {
            // editar
            override fun onClick(itemId: Int) {
               //val addFrag = AgregarFacturacionFragment.instance() as AgregarFacturacionFragment
               // addFrag.setData(datosfacturacion[itemId])
               // goAdd(addFrag)

                val addFrag = AgregarFacturacionFragment.instance() as AgregarFacturacionFragment
                addFrag.setData(datosfacturacion[itemId])
                val interactor = AddFacturacionInteractor(context!!)
                AddFacturacionPresenter(addFrag as AddFacturacionContract.View, interactor)
                goAdd(addFrag)
            }
        }, object : Events{
            override fun onClick(itemId: Int) {
                eliminarElemento(itemId)
            }
        })
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
    }

    private fun goAdd(fragment: AgregarFacturacionFragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    private fun initEvents() {
        btnBack.setOnClickListener {
            (context as MainActivity).irAtras()
        }
    }

    private fun eliminarElemento(position: Int) {
        var msg = context!!.getString(R.string.mensaje_delete_element)
        msg = msg.replace("item", datosfacturacion[position].nombre_razonSocial)
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Advertencia")
            .setMessage(msg)
            .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar), null)
            .setButtonPositive(context!!.getString(R.string.mensaje_btn_estoy_seguro), object : WADialog.OnClickListener {
                override fun onClick() {
                    mPresenter?.eliminarFacturacion(datosfacturacion[position].id)
                }
            }).build()
        dialog.show()
    }

    override fun actionFab() {
        val addFrag = AgregarFacturacionFragment.instance() as AgregarFacturacionFragment
        addFrag.setData(Facturacion())
        goAdd(AgregarFacturacionFragment.instance() as AgregarFacturacionFragment)
    }

    override fun showFacturacion(facturacion: ArrayList<Facturacion>) {
        this.datosfacturacion.clear()
        this.datosfacturacion.addAll(facturacion)
        mAdapter?.notifyDataSetChanged()
    }

    override fun showNetworkError() {
        (context as MainActivity).mostrarFragment(SinConexionFragment.instance())
    }

    override fun showEmpty() {
        this.datosfacturacion.clear()
        mAdapter?.notifyDataSetChanged()
    }

    override fun showMessageError(err: String?) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err ?: context!!.getString(R.string.mensaje_error_desconocido))

            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun setPresenter(presenter: DatosFacturacionContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DatosFacturacionFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}