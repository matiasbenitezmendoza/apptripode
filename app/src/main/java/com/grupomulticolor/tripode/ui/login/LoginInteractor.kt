package com.grupomulticolor.tripode.ui.login

import android.content.Context
import android.os.Bundle
import android.util.Patterns
import android.text.TextUtils
import android.util.Log
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.grupomulticolor.tripode.BuildConfig
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.login.LoginBody
import com.grupomulticolor.tripode.data.login.service.LoginService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.util.config.Constants
import com.grupomulticolor.tripode.util.Network
import java.util.regex.Pattern

class LoginInteractor (private val context: Context) {

    private val min = 6
    private val service = LoginService(context)

    fun loginWithEmail(email: String, passwd: String, callback: Callback) {

        if (!isValidEmail(email, callback)) return
        if (!isValidPassword(passwd, callback)) return
        if (!isNetworkAvalible(callback)) return
        singInWithEmail(email, passwd, callback)
    }

    fun loginWithFacebook(accessToken: AccessToken, callback: Callback) {
        if (!isNetworkAvalible(callback)) return

        val request = GraphRequest.newMeRequest(accessToken) { objeto, response ->
            try {

                val nombre = objeto.getString("first_name")
                val apellido = objeto.getString("last_name")
                val email = objeto.getString("email")
                val token = accessToken.token
                val id = objeto.getString("id")

                // proceso de login con servidord
                val loginBody = LoginBody()

                loginBody.nombre = nombre
                loginBody.apellidoP = apellido
                loginBody.tokenFacebook = token
                loginBody.idFacebook = id
                loginBody.email = email
                loginBody.version = BuildConfig.VERSION_NAME



                singInWithFacebook(loginBody, callback)
               /*
                if(email == "" || email == null || email == "null"){
                      callback.onAuthError("No se pudo encontrar un email publico, el email es necesario para realizar el registro.")
                }
                else{
                      singInWithFacebook(loginBody, callback)
                }*/


            } catch (e: Exception) {
                e.printStackTrace()
                callback.onAuthError(e.message ?: context.getString(R.string.error_unknown))
            }
        }

        val bundle = Bundle()
        bundle.putString("fields", "first_name,last_name,email,id")
        request.parameters = bundle
        request.executeAsync()
    }

    /* verifica que el correo ingresado sea valido*/
    private fun isValidEmail(email: String, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(email)) {
            callback.onEmailError(context.getString(R.string.text_error_correo_vacio))
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onEmailError(context.getString(R.string.text_error_correo))
            return false
        }
        return true
    }

    /* verifica que la contraseña ingresada sea valido*/
    private fun isValidPassword(passwd: String, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(passwd)) {
            callback.onPasswordError(context.getString(R.string.text_passwd_vacio))
            return false
        }

        val patron = Pattern.compile(Constants.patternPasswd)
        if (!patron.matcher(passwd).matches()) {
            callback.onPasswordError(context.getString(R.string.text_passwd_no_valido))
            return false
        }

        if (passwd.length < min) {
            callback.onPasswordError(context.getString(R.string.text_passwd_min))
            return false
        }
        return true
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }

    /* intenta loguearse */
    private fun singInWithEmail(email: String, passwd: String, callback: Callback) {
        Log.d(LoginInteractor::class.java.name, "login with email: $email and pass: $passwd")

        service.post(
            LoginBody(email, passwd, BuildConfig.VERSION_NAME),
            object : ResponseListener<ResponseData<String>> {

            override fun onSuccess(response: ResponseData<String>) {
                SessionPrefs(context).saveToken(response.data!!)
                callback.onAuthSuccess()
            }

            override fun onError(t: Throwable) {
                callback.onAuthError(t.message!!)
            }

        })
    }

    private fun singInWithFacebook(loginBody: LoginBody, callback: Callback) {

        val usuario = Usuario()

        usuario.nombre = loginBody.nombre
        usuario.apellidoPaterno = loginBody.apellidoP
        usuario.correo = loginBody.email
        usuario.version = loginBody.version

        callback.onAuthSuccessFacebook(usuario)
        /*
        service.facebook(loginBody, object : ResponseListener<ResponseData<Usuario>> {
            override fun onSuccess(response: ResponseData<Usuario>) {

                val usuario = Usuario()

                usuario.nombre = loginBody.nombre
                usuario.apellidoPaterno = loginBody.apellidoP
                //usuario. = token
                //usuario.idFacebook = id
                usuario.correo = loginBody.email
                usuario.version = loginBody.version


                callback.onAuthSuccessFacebook(usuario)
                /*
                val token = response.data
                if (token != null) {
                    SessionPrefs(context).saveToken(response.token!!)
                    callback.onAuthSuccess()
                } else {
                    callback.onAuthError(context.getString(R.string.error_en_token))
                }*/

               /* val intent = Intent(activity, RegistroActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
                activity?.startActivity(intent)*/
            }
            override fun onError(t: Throwable) {
                callback.onAuthError(t.message ?: context.getString(R.string.error_unknown))
            }
        })*/
    }

    /* interface para notificar al presentador cualquier evento */
    interface Callback {
        fun onEmailError(err: String)
        fun onPasswordError(err: String)
        fun onNetworkConnectionFailed()
        fun onAuthSuccess()
        fun onAuthSuccessFacebook(usuario: Usuario)
        fun onAuthError(err: String)
    }
}