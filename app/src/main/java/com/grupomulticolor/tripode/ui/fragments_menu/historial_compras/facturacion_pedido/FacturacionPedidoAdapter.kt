package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.detallepedido.DetalleOrden
import com.grupomulticolor.tripode.util.Currency

class FacturacionPedidoAdapter(var detallepedido: ArrayList<DetalleOrden>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vh: RecyclerView.ViewHolder
        val vw: View

        when(viewType) {
            Tipo.PRODUCT -> {
                vw = inflate.inflate(R.layout.item_confirmar_producto, parent, false)
                vh = ViewHolderItemProducto(vw)
            }
            Tipo.TOTAL -> {
                vw = inflate.inflate(R.layout.item_tv_total, parent, false)
                vh = ViewHolderItemTotal(vw)
            }
            else -> {
                vw = inflate.inflate(R.layout.item_empty, parent, false)
                vh = ViewHolderItemEmpty(vw)
            }
        }
        return vh
    }

    override fun getItemCount(): Int {
        var items = 0
        if (detallepedido.isNotEmpty()) {
            items += detallepedido.size + 1
        }

        return items
    }

    override fun getItemViewType(position: Int): Int {

        return when {
            detallepedido.size > position -> Tipo.PRODUCT
            detallepedido.size == position -> Tipo.TOTAL

            else -> -1
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when(holder.itemViewType) {
            Tipo.PRODUCT -> (holder as ViewHolderItemProducto).bind(detallepedido[position])
            Tipo.TOTAL -> {
                var total = 0.0
                for (pro in detallepedido) {
                    total += (pro.precio_total!!)
                }
                (holder as ViewHolderItemTotal).tvTotal.text = Currency.toCurrency(total, "MXN")
            }
            else -> (holder as ViewHolderItemEmpty)
        }
    }

    inner class ViewHolderItemProducto(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitleProducto: TextView = itemView.findViewById(R.id.tvTitleProducto)
        val tvCantidad: TextView = itemView.findViewById(R.id.tvCantidad)
        val tvTotal: TextView = itemView.findViewById(R.id.tvTotal)
        val infoProducto: TextView = itemView.findViewById(R.id.infoProducto)

        fun bind(carrito: DetalleOrden) {
            tvTitleProducto.text = carrito?.producto
            tvCantidad.text = carrito?.cantidad.toString()
            tvTotal.text =
                Currency.toCurrency((carrito?.precio_total!!), "MXN")
        }
    }

    inner class ViewHolderItemTotal(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTotal: TextView = itemView.findViewById(R.id.tvTotal)
    }

/*
    inner class ViewHolderItemFacturacion(itemView: View) : RecyclerView.ViewHolder(itemView) {
       /* val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvDireccion: TextView = itemView.findViewById(R.id.tvDireccion)
        val btnEliminar: ImageButton = itemView.findViewById(R.id.btnEliminar)
        val btnEditar: ImageButton = itemView.findViewById(R.id.btnEditar)
    */
        fun bind(facturacion: Facturacion) {
    /*          tvTitle.text = direccion?.nombreReferencia
            tvDireccion.text = direccion?.detalle()
            btnEliminar.visibility = View.GONE
            btnEditar.visibility = View.GONE*/
        }
    }*/


    inner class ViewHolderItemEmpty(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    object Tipo {
        const val PRODUCT = 1
        const val TOTAL = 2
    }

}