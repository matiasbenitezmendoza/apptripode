package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.util.Input
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_nueva_direccion.*

class AgregarDireccionFragment : FragmentChildNotFab(), DireccionAgregarContract.View {

    private lateinit var cntx: Context
    private lateinit var vw: View
    private var mPresenter: DireccionAgregarContract.Presenter? = null
    private var direccion: Direccion = Direccion()
    private var mEstados: ArrayList<Estados> = ArrayList()
    private var mMunicipios: ArrayList<Municipios> = ArrayList()
    private var mColonias: ArrayList<Colonias> = ArrayList()
    private var mResidencial: ArrayList<Residencial> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw =  inflater.inflate(R.layout.fragment_nueva_direccion, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cntx = context!!

        val interactor = AddDireccionInteractor(cntx)
        AddDireccionPresenter(this, interactor)

        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    fun setData(direccion: Direccion) {
        this.direccion = direccion
    }

    private fun initEvents() {
        Input.addTextListener(cntx, inputNombreReferencia)
        Input.addTextListener(cntx, inputCalle)
        Input.addTextListener(cntx, inputNumExt)
        Input.addTextListener(cntx, inputCP)

        mPresenter?.consultarEstados()
        mPresenter?.consultarResidencias()

        inputCiudad.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                mPresenter?.consultarMunicipios(mEstados[p3.toInt()].Id.toString())
            }

        }

        spinnerMunicipio.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                mPresenter?.consultarColonias(mEstados[inputCiudad.selectedItemPosition].Id.toString() ,mMunicipios[p3.toInt()].Id.toString())
            }

        }

        btnBack.setOnClickListener { (context!! as MainActivity).irAtras() }
        btnGuardar.setOnClickListener { enviarDatos() }


    }

    private fun enviarDatos() {
        direccion.nombreReferencia = inputNombreReferencia.text.toString()
        direccion.calle = inputCalle.text.toString()
        direccion.noExt = inputNumExt.text.toString()
        direccion.noInt = inputNumInt.text.toString()
        direccion.codigoPostal = inputCP.text.toString()

        if (spinnerMunicipio.selectedItem != null)
            direccion.pNivel2 = mMunicipios[spinnerMunicipio.selectedItemPosition].Id
        if (inputCiudad.selectedItem != null)
            direccion.pNivel1 = mEstados[inputCiudad.selectedItemPosition].Id
        if (spinnerColonia.selectedItem != null)
            direccion.pNivel3 = mColonias[spinnerColonia.selectedItemPosition].Id
        if (spinnerResidencial.selectedItem != null)
            direccion.residencial = mResidencial[spinnerResidencial.selectedItemPosition].nombre

        mPresenter?.intentaAgregarDireccion(direccion)
    }

    override fun presentarDatos() {

        inputNombreReferencia.setText(direccion.nombreReferencia)
        inputCalle.setText(direccion.calle)
        inputNumExt.setText(direccion.noExt)
        inputNumInt.setText(direccion.noInt)
        inputCP.setText(direccion.codigoPostal)

    }

    override fun mostrarErrorEnNombre(err: String) {
        Input.onError(cntx, inputNombreReferencia, err)
    }

    override fun mostrarErrorEnCalle(err: String) {
        Input.onError(cntx, inputCalle, err)
    }

    override fun mostrarErrorEnNoExt(err: String) {
        Input.onError(cntx, inputNumExt, err)
    }

    override fun muestraErrorEnCodigoPostal(error: String) {
        Input.onError(cntx, inputCP, error)
    }

    override fun muestraErrorEnResidencia(error: String) {
        spinnerResidencial.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
    }

    override fun muestraErrorEnColonia(error: String) {
        spinnerColonia.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
    }

    override fun muestraErrorEnCiudad(error: String) {
        inputCiudad.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
    }

    override fun muestraErrorEnConexion() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(R.string.msg_sin_conexion)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestraDatosDireccionPostal(direccionPostal: DireccionPostal) {

    }

    override fun muestraDatosResidencia(residencial: ArrayList<Residencial>) {
        mResidencial.clear()
        mResidencial.addAll(residencial)
        val adapter : ArrayAdapter<Residencial> = ArrayAdapter(context!!, R.layout.spinner_text_item, mResidencial)
        spinnerResidencial.adapter = adapter
    }

    override fun muestraDatosEstados(estados: ArrayList<Estados>) {
        mEstados.clear()
        mEstados.addAll(estados)
        val adapter : ArrayAdapter<Estados> = ArrayAdapter(context!!, R.layout.spinner_text_item, mEstados)
        inputCiudad.adapter = adapter

        /*Datos por defaul v1*/
        inputCiudad.setSelection(14)
        inputCiudad.isEnabled = false
        inputCiudad.isClickable = false
        var codigo = "52774"
        inputCP.setText(codigo)
        inputCP.isEnabled = false
    }

    override fun muestraDatosColonias(colonias: ArrayList<Colonias>) {
        mColonias.clear()
        mColonias.addAll(colonias)
        val adapter : ArrayAdapter<Colonias> = ArrayAdapter(context!!, R.layout.spinner_text_item, mColonias)
        spinnerColonia.adapter = adapter

        /*Datos por defaul v1*/
        spinnerColonia.setSelection(69)
        spinnerColonia.isEnabled = false
        spinnerColonia.isClickable = false
    }

    override fun muestraDatosMunicipios(municipios: ArrayList<Municipios>) {
        mMunicipios.clear()
        mMunicipios.addAll(municipios)
        val adapter : ArrayAdapter<Municipios> = ArrayAdapter(context!!, R.layout.spinner_text_item, mMunicipios)
        spinnerMunicipio.adapter = adapter

        /*Datos por defaul v1*/
        spinnerMunicipio.setSelection(36)
        spinnerMunicipio.isEnabled = false
        spinnerMunicipio.isClickable = false
    }

    override fun muestraMensajeDeExito(msg: String) {
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage(msg)
            .setButtonPositive("De acuerdo", object : WADialog.OnClickListener {
                override fun onClick() {
                    (context!! as MainActivity).irAtras()
                }
            }).build()
        dialog.show()
    }

    override fun muestraMensajeDeError(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun setPresenter(presenter: DireccionAgregarContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = AgregarDireccionFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}