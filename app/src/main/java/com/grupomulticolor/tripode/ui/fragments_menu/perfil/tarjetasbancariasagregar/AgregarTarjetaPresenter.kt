package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar

import android.util.Log
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import mx.openpay.android.OperationResult
import mx.openpay.android.exceptions.OpenpayServiceException
import mx.openpay.android.exceptions.ServiceUnavailableException
import mx.openpay.android.model.Token

class AgregarTarjetaPresenter(val mView: AgregarTarjetaContract.View,
                              val mInteractor: AgregarTarjetaInteractor)
    : AgregarTarjetaContract.Presenter, AgregarTarjetaInteractor.Callback {

    init {
        mView.setPresenter(this)
    }

    override fun start() {}

    override fun intentaAgregartarjeta(tarjetaBancaria: TarjetaBancaria) {
        mInteractor.agregaTarjetaBancaria(tarjetaBancaria, this)
    }

    override fun intentaGuardarTarjeta(tarjetaBancaria: TarjetaBancaria) {
        mInteractor.guardarTarjeta(tarjetaBancaria, this)
    }

    override fun onNombreTarjetaError(err: String) {
        mView.mostrarErrorEnNombreTarjeta(err)
    }

    override fun onNumTarjetaError(err: String) {
        mView.mostrarErrorEnNumTarjeta(err)
    }

    override fun onFechaDeVenError(err: String) {
        mView.mostrarErrorEnFechaVencimiento(err)
    }

    override fun onCVVError(err: String) {
        mView.mostrarErrorEnCVV(err)
    }

    override fun onNetworkConnectionFailed() {
        mView.mostrarErrorEnConexion()
    }

    override fun onSaveSucces(resource: Int, resolve: () -> Unit) {
        mView.mostrarMensajedeExito(resource, resolve)
    }

    override fun onSaveFailed(err: String) {
        mView.mostrarMensajeError(err)
    }


    override fun onShowProgress() {
        mView.mostrarProgress(true)
    }

    override fun onHideProgress() {
        mView.mostrarProgress(false)
    }

    override fun onSuccess(result: OperationResult<Token>?) {
        onHideProgress()
        result?.result
        mView.onSuccesToken(result?.result)
    }

    override fun onCommunicationError(error: ServiceUnavailableException?) {
        onHideProgress()
        error?.printStackTrace()
        mView.mostrarMensajeError(R.string.communication_error, null)
    }

    override fun goBack() {
        mView.irAtras()
    }


    override fun onError(error: OpenpayServiceException?) {
        onHideProgress()
        error?.printStackTrace()

        val desc: Int
        var msg : String? = ""

        when (error?.getErrorCode()) {
            3001 -> desc = R.string.declined
            3002 -> desc = R.string.expired
            3003 -> desc = R.string.insufficient_funds
            3004 -> desc = R.string.stolen_card
            3005 -> desc = R.string.suspected_fraud
            2002 -> desc = R.string.already_exists
            else -> {
                desc = R.string.error_creating_card
                msg = error?.getDescription()
                Log.d("Error Open pay", msg)
            }
        }
        mView.mostrarMensajeError(desc, msg)
    }
}