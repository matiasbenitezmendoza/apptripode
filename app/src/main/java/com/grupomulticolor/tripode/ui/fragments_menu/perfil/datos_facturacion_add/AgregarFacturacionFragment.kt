package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.cfdi.Cfdi
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.util.Input
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_form_facturacion.*
import kotlinx.android.synthetic.main.fragment_form_facturacion.btnBack
import kotlinx.android.synthetic.main.fragment_form_facturacion.btnGuardar
import kotlinx.android.synthetic.main.fragment_form_facturacion.inputCP
import kotlinx.android.synthetic.main.fragment_form_facturacion.inputCalle
import kotlinx.android.synthetic.main.fragment_form_facturacion.inputNumExt
import kotlinx.android.synthetic.main.fragment_form_facturacion.inputNumInt
import kotlinx.android.synthetic.main.fragment_form_facturacion.spinnerColonia
import kotlinx.android.synthetic.main.fragment_form_facturacion.spinnerMunicipio
import kotlinx.android.synthetic.main.fragment_form_facturacion.spinnerResidencial

class AgregarFacturacionFragment : FragmentChildNotFab(), AddFacturacionContract.View {

    lateinit var vw: View
    private var mPresenter: AddFacturacionContract.Presenter? = null
    private lateinit var cntx: Context
    private var facturacion = Facturacion()
    private var checked = false


    private var mEstados: ArrayList<Estados> = ArrayList()
    private var mMunicipios: ArrayList<Municipios> = ArrayList()
    private var mColonias: ArrayList<Colonias> = ArrayList()
    private var mcfdi: ArrayList<Cfdi> = ArrayList()
    private var mresidencial: ArrayList<Residencial> = ArrayList()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_form_facturacion, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cntx = context!!

        // init mvp
        val interactor = AddFacturacionInteractor(cntx)
        AddFacturacionPresenter(this, interactor)
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    fun setData(facturacion: Facturacion) {
        this.facturacion = facturacion
    }

    private fun initEvents() {
        Input.addTextListener(cntx, inputRfc)
        Input.addTextListener(cntx, inputRazonSocial)
        Input.addTextListener(cntx, inputCalle)
        Input.addTextListener(cntx, inputNumExt)
        Input.addTextListener(cntx, inputCP)


        btnBack.setOnClickListener { (context!! as MainActivity).irAtras() }
        btnGuardar.setOnClickListener { enviarDatos() }

        mPresenter?.consultarEstados()
        mPresenter?.consultarCfdi()
        mPresenter?.consultarResidencial()


        spinnerEstado.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(p0: AdapterView<*>?) {

                        }
                        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                            mPresenter?.consultarMunicipios(mEstados[p3.toInt()].Id.toString())
                        }

        }

        spinnerMunicipio.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(p0: AdapterView<*>?) {
                            }
                            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                                mPresenter?.consultarColonias(mEstados[spinnerEstado.selectedItemPosition].Id.toString() ,mMunicipios[p3.toInt()].Id.toString())
                            }
        }

        /*
        checkboxDireccion.setOnClickListener {
            checked = checkboxDireccion.isChecked

            if (checked) {
                mPresenter?.consultarDireccionEntrega()
            }
        }*/

    }

    private fun enviarDatos() {
        facturacion.rfc = inputRfc.text.toString()
        facturacion.nombre_razonSocial = inputRazonSocial.text.toString()

        facturacion.calle = inputCalle.text.toString()
        facturacion.noExt = inputNumExt.text.toString()
        facturacion.noInt = inputNumInt.text.toString()
        facturacion.codigoPostal = inputCP.text.toString()

        if (spinnerEstado.selectedItem != null){
            facturacion.pNivel1 = mEstados[spinnerEstado.selectedItemPosition].Id
            facturacion.estado = mEstados[spinnerEstado.selectedItemPosition].name
        }

        if (spinnerMunicipio.selectedItem != null){
            facturacion.pNivel2 = mMunicipios[spinnerMunicipio.selectedItemPosition].Id
            facturacion.municipio = mMunicipios[spinnerMunicipio.selectedItemPosition].name
        }

        if (spinnerColonia.selectedItem != null){
            facturacion.pNivel3 = mColonias[spinnerColonia.selectedItemPosition].Id
            facturacion.colonia = mColonias[spinnerColonia.selectedItemPosition].name
        }


        if (spinnerCfdi.selectedItem != null){
            facturacion.cfdi = mcfdi[spinnerCfdi.selectedItemPosition].Id
            facturacion.cfdi_name = mcfdi[spinnerCfdi.selectedItemPosition].name
        }

        if (spinnerResidencial.selectedItem != null)
            facturacion.residencial = mresidencial[spinnerResidencial.selectedItemPosition].nombre


        mPresenter?.intentaAgregarFacturacion(facturacion)


    }

    override fun presentarDatos() {
        //if (facturacion.id != 0) {
            inputRfc.setText(facturacion.rfc)
            inputRazonSocial.setText(facturacion.nombre_razonSocial)
            inputCalle.setText(facturacion.calle)
            inputNumExt.setText(facturacion.noExt)
            inputNumInt.setText(facturacion.noInt)
            inputCP.setText(facturacion.codigoPostal)
       // }
    }

    override fun mostrarErrorEnRFC(err: String) {
        Input.onError(cntx, inputRfc, err)
    }

    override fun mostrarErrorEnRazon(err: String) {
        Input.onError(cntx, inputRazonSocial, err)
    }

    override fun mostrarErrorEnCalle(err: String) {
        Input.onError(cntx, inputCalle, err)
    }

    override fun mostrarErrorEnNoExt(err: String) {
        Input.onError(cntx, inputNumExt, err)
    }

    override fun muestraErrorEnCodigoPostal(error: String) {
        Input.onError(cntx, inputCP, error)
    }

    override fun muestraErrorEnResidencia(error: String) {
        spinnerResidencial.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
    }

    override fun muestraErrorEnEstado(error: String) {
        spinnerEstado.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)

    }

    override fun muestraErrorEnColonia(error: String) {
        spinnerColonia.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
    }

    override fun muestraErrorEnDelMun(error: String) {
        spinnerMunicipio.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
    }

    override fun muestraErrorEnCfdi(error: String) {
        spinnerEstado.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)

    }

    override fun muestraErrorEnConexion() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(R.string.msg_sin_conexion)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestraMensajeDeExito(msg: String) {
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage(msg)
            .setButtonPositive("De acuerdo", object : WADialog.OnClickListener {
                override fun onClick() {
                    (context!! as MainActivity).irAtras()
                }
            }).build()
        dialog.show()
    }

    override fun muestraMensajeDeError(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestraDatosCfdi(cfdi: ArrayList<Cfdi>) {
        mcfdi.clear()
        mcfdi.addAll(cfdi)
        val adapter : ArrayAdapter<Cfdi> = ArrayAdapter(context!!, R.layout.spinner_text_item, mcfdi)
        spinnerCfdi.adapter = adapter
    }

    override fun muestraDatosResidencial(residencial: ArrayList<Residencial>) {
        mresidencial.clear()
        mresidencial.addAll(residencial)
        val adapter : ArrayAdapter<Residencial> = ArrayAdapter(context!!, R.layout.spinner_text_item, mresidencial)
        spinnerResidencial.adapter = adapter
    }

    override fun muestraDatosEstados(estados: ArrayList<Estados>) {
        mEstados.clear()
        mEstados.addAll(estados)
        val adapter : ArrayAdapter<Estados> = ArrayAdapter(context!!, R.layout.spinner_text_item, mEstados)
        spinnerEstado.adapter = adapter
    }

    override fun muestraDatosColonias(colonias: ArrayList<Colonias>) {
        mColonias.clear()
        mColonias.addAll(colonias)
        val adapter : ArrayAdapter<Colonias> = ArrayAdapter(context!!, R.layout.spinner_text_item, mColonias)
        spinnerColonia.adapter = adapter
    }

    override fun muestraDatosMunicipios(municipios: ArrayList<Municipios>) {
        mMunicipios.clear()
        mMunicipios.addAll(municipios)
        val adapter : ArrayAdapter<Municipios> = ArrayAdapter(context!!, R.layout.spinner_text_item, mMunicipios)
        spinnerMunicipio.adapter = adapter
    }

    override fun setDireccionEntrega(direccion: Direccion) {
        /*inputCalle.setText(direccion.calle)
        inputNumExt.setText(direccion.noExt)
        inputNumInt.setText(direccion.noInt)
        inputDelMun.setText(direccion.ciudad)
        inputEstado.setText(direccion.estado)
        inputCP.setText(direccion.codigoPostal)

        mPresenter?.consultarDireccionPostal(direccion.codigoPostal, 1)*/
    }

    override fun setPresenter(presenter: AddFacturacionContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = AgregarFacturacionFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }

}