package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras

import android.content.Context
import android.util.Log
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.pedido.source.OrdenesService
import com.grupomulticolor.tripode.data.pedido.source.PedidoService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.session.SessionPrefs
import java.lang.Exception

class HistorialPresenter(val mHistorialView: HistorialContract.View, var context: Context) : HistorialContract.Presenter {

    private val service: OrdenesService = OrdenesService.instance(context)
    private val servicePedido: PedidoService = PedidoService.instance(context)

    init {
        mHistorialView.setPresenter(this)
    }

    override fun start() {
        loadPedidos()
    }

    override fun loadOrdenes(pedidos: ArrayList<Pedido>) {
        val id_socio = SessionPrefs.getInstance(context!!).getIdLdcom()
        service.getPedidosFinalizados(id_socio,"",object : ResponseListener<ResponseDataTripode<ArrayList<Ordenes>>> {
            override fun onSuccess(response: ResponseDataTripode<ArrayList<Ordenes>>) {
                try {
                    var ordenes = response.data as ArrayList<Ordenes>
                    mHistorialView.showPedidos(ordenes, pedidos)

                } catch (e: Exception) {
                    e.printStackTrace()
                    mHistorialView.showErrorNetwork()
                }
            }

            override fun onError(t: Throwable) {
                mHistorialView.showErrorNetwork()
            }
        })


    }

    override fun loadPedidos() {
        servicePedido.getHistorial(object : ResponseListener<ResponseData<ArrayList<Pedido>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Pedido>>) {
                try {
                    loadOrdenes(response.data as ArrayList<Pedido>)
                } catch (e: Exception) {
                    e.printStackTrace()
                    mHistorialView.showErrorNetwork()
                }
            }

            override fun onError(t: Throwable) {
                mHistorialView.showErrorNetwork()
            }
        })


    }


}