package com.grupomulticolor.tripode.ui.fragments_menu.programa_lealtad

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.programa.ProgramaLealtad
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_programa_lealtad.*
import kotlinx.android.synthetic.main.item_card_programa_lealtad.*

class ProgramaLealtadFragment : FragmentChildNotFab(), ProgramaLealtadContract.View {


    private lateinit var vw: View
    private var mPresenter: ProgramaLealtadContract.Presenter? = null
    private var socio :ProgramaLealtad? =  null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_programa_lealtad, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ProgramaLealtadPresenter(this, context!!)
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    override fun showSocio(s: ProgramaLealtad) {
        this.saldo.text = s.saldo
        this.nombre_socio.text = s.nombre
        this.no_socio.text = s.num_socio
        var f = s.fecha!!.substring(5,7) + "/"+ s.fecha!!.substring(0,4)
        this.fecha.text = "Miembro desde "+f
    }

    override fun showNetworkError() {
        Snackbar.make(vw, "Sin Conexion", Snackbar.LENGTH_LONG).show()
    }

    override fun showMessageError(err: String?) {

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Advertencia")
            .setMessage(err ?: context!!.getString(R.string.error_unknown))
            .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar), null)
            .setButtonPositive("Ok", null).build()
        dialog.show()
    }

    override fun setPresenter(presenter: ProgramaLealtadContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = ProgramaLealtadFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }

}