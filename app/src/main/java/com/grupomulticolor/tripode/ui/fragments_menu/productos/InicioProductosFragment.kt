package com.grupomulticolor.tripode.ui.fragments_menu.productos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.slider.Sliders
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleContract
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoPresenter
import com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones.PicassoImageLoadingService
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_inicio.mRecyclerView
import kotlinx.android.synthetic.main.toolbar.*
import ss.com.bannerslider.Slider

class InicioProductosFragment : FragmentParentNotFab(), ProductosContract.View {

    private lateinit var mAdapter: ProductosAdapter
    private var mproductos = ArrayList<Producto>()
    private var mimagenes = ArrayList<Sliders>()
    private lateinit var vw: View
    private var mPresenter: ProductosContract.Presenter? = null

    private var limit = 20
    private var offset = 1
    private var position = 0
    private var aptoParaCargar = true
    private var busqueda = "top"
    private var detalle = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_inicio, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val interactor = ProductosInteractor(context!!)
        ProductosPresenter(this, interactor)
        Slider.init(PicassoImageLoadingService(this.context))
        ViewCompat.setNestedScrollingEnabled(vw,false)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        if(!detalle){
            mPresenter?.start()
            mPresenter?.getcarrito( SessionPrefs.getInstance(context!!).getIdSC())
        }
    }

    private fun initAdapter() {
        mAdapter = ProductosAdapter(mproductos, mimagenes, object : ProductosAdapter.ListenerView {
            override fun onClick(item: Producto) {
                goDetalleProducto(item)
            }
        })

        mRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(context)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)

        mRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1
                    if (aptoParaCargar) {
                        if (  pastVisibleItem >= totalItemCount) {
                            aptoParaCargar = false
                            offset += 1
                            mPresenter?.getproductosinicio(busqueda,limit, offset,0,0)
                        }
                    }
                }

            }


        })


    }

    private fun goDetalleProducto(producto: Producto) {
        setDetalle(true)
        DetalleProductoPresenter(
            DetalleProductoFragment.instance() as DetalleContract.View, producto, context!!)
        fragmentTransaction(DetalleProductoFragment.instance())
    }


    fun setDetalle(b : Boolean){
        this.detalle = b
    }

    override fun muestraProgress(show: Boolean) {
    }

    override fun muestraErrorEnConexion() {
        Snackbar.make(vw, "Sin conexion a internet", Snackbar.LENGTH_LONG).show()
    }

    override fun muestraErrorDePeticion(error: String) {
       val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestrarErrorAlCargar(err: String) {

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage("Ocurrio un problema.")
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun muestrarCarritoVacio(err: String) {
        (context as MainActivity).badgeCar.text = "0"
    }

    override fun muestraMensajesSuccess(mensaje: String) {
    }

    override fun agregarProductosFilterList(productos: ArrayList<Producto>) {
        aptoParaCargar = true
        mproductos.addAll(productos)
        mAdapter.notifyDataSetChanged()

    }

    override fun agregarSlidersFilterList(imagenes: ArrayList<Sliders>) {
        mimagenes.clear()
        mimagenes.addAll(imagenes)
        if(mproductos.isEmpty()){
            mPresenter?.getproductosinicio(busqueda,limit, offset, 0,0)
        }
    }

    override fun agregarNumeroItem(carrito: ArrayList<Carrito>) {
        var item_num = carrito.size
        (context as MainActivity).badgeCar.text = item_num.toString()
    }

    override fun setPresenter(presenter: ProductosContract.Presenter) {
        mPresenter = presenter
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }


    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = InicioProductosFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}