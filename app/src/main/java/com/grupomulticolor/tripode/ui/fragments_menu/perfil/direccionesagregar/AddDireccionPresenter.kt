package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar

import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.residencial.Residencial

class AddDireccionPresenter(
    val mView: DireccionAgregarContract.View,
    val mInteractor: AddDireccionInteractor
) : DireccionAgregarContract.Presenter, AddDireccionInteractor.Callback {

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        mView.presentarDatos()
    }

    override fun intentaAgregarDireccion(direccion: Direccion) {
        mInteractor.addDireccion(direccion, this)
    }

    override fun onNombreError(err: String) {
        mView.mostrarErrorEnNombre(err)
    }

    override fun onCalleError(err: String) {
        mView.mostrarErrorEnCalle(err)
    }

    override fun onExtError(err: String) {
        mView.mostrarErrorEnNoExt(err)
    }

    override fun onResidencialError(err: String) {
        mView.muestraErrorEnResidencia(err)
    }

    override fun onCPError(err: String) {
        mView.muestraErrorEnCodigoPostal(err)
    }

    override fun onColoniaError(err: String) {
        mView.muestraErrorEnColonia(err)
    }

    override fun onCiudadError(err: String) {
        mView.muestraErrorEnCiudad(err)
    }

    override fun onNetworkConnectionFailed() {
        mView.muestraErrorEnConexion()
    }

    override fun onRequestFailed(err: String) {
        mView.muestraMensajeDeError(err)
    }

    override fun onRequestAddSuccess(msg: String) {
        mView.muestraMensajeDeExito(msg)
    }

    override fun onRequestUpdateSucces(msg: String) {
        mView.muestraMensajeDeExito(msg)
    }

    override fun onRequestCodigoPostalSucces(direccionPostal: DireccionPostal) {
        mView.muestraDatosDireccionPostal(direccionPostal)
    }

    override fun onRequestResidencias(residencias: ArrayList<Residencial>) {
        mView.muestraDatosResidencia(residencias)
    }

    override fun onRequestEstados(estados: ArrayList<Estados>) {
        mView.muestraDatosEstados(estados)
    }

    override fun onRequestMunicipios(municipios: ArrayList<Municipios>) {
        mView.muestraDatosMunicipios(municipios)
    }

    override fun onRequestColonias(colonias: ArrayList<Colonias>) {
        mView.muestraDatosColonias(colonias)
    }
    /*
    override fun consultarDireccionPostal(cp: String) {
        mInteractor.getDireccionPostal(cp, this)
    }*/

    override fun consultarResidencias() {
        mInteractor.getResidencias(this)
    }

    override fun consultarEstados() {
        mInteractor.getEstados(this)
    }

    override fun consultarMunicipios(mun: String) {
        mInteractor.getMunicipios(mun,this)
    }

    override fun consultarColonias(est: String,mun: String) {
        mInteractor.getColonias(est, mun,this)
    }
}