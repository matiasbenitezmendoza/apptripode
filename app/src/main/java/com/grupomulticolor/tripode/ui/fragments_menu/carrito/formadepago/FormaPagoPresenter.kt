package com.grupomulticolor.tripode.ui.fragments_menu.carrito.formadepago

import android.content.Context
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.data.tarjeta.source.TarjetasService
import com.grupomulticolor.tripode.util.Network

class FormaPagoPresenter(var mView: FormaPagoContract.View,
                         var context: Context) : FormaPagoContract.Presenter {

    private val service = TarjetasService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        requestTarjetasDeUsuario()
    }

    override fun requestTarjetasDeUsuario() {
        if (isNetworkAvalible()) {
            service.getAll(object : ResponseListener<ResponseData<ArrayList<TarjetaBancaria>>> {
                override fun onSuccess(response: ResponseData<ArrayList<TarjetaBancaria>>) {
                    try {
                        mView.showTarjetasDeUsuario(response.data as ArrayList<TarjetaBancaria>)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mView.showEmpty()
                    }
                }
                override fun onError(t: Throwable) {
                    mView.showEmpty()
                }
            })
        }
    }

    override fun intentaContinuar(idTar: Int, tipoPago: Int) {
        if (tipoPago == 1 && idTar != -1) {
            mView.irDirecciones()
        }
        else if (tipoPago > 1) {
            mView.irDirecciones()
        } else {
            val msg = context.getString(R.string.mensaje_error_sin_pago)
            mView.showError(msg)
        }
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mView.showErrorNetwork()
        return isAvailable
    }

}