package com.grupomulticolor.tripode.ui.fragments_menu.carrito

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.MainContract
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.ui.fragments_menu.carrito.formadepago.FormaPagoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.productos.InicioProductosFragment
import com.grupomulticolor.tripode.util.Currency
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_carrito.*
import kotlinx.android.synthetic.main.toolbar.*

class CarritoFragment : FragmentParentNotFab(), CarritoContract.View {

    private lateinit var vw: View
    private var carrito = ArrayList<Carrito>()
    private lateinit var mAdapter: CarritoAdapter
    private var mPresenter: CarritoContract.Presenter? = null
    private var presionar_recarga = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw =  inflater.inflate(R.layout.fragment_carrito, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnContinuarComprando.paintFlags = btnContinuarComprando.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        CarritoPresenter(this,  context!!)
        initAdapter()
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initAdapter() {
        mAdapter = CarritoAdapter(carrito,mPresenter!!,object : CarritoAdapter.ListenerView {
            override fun onClick() {
                presionar_recarga = false
                btnRecargar.visibility = View.VISIBLE
            }
        })
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mAdapter
    }

    private fun initEvents() {
        btnBack.setOnClickListener { (context as MainActivity).irAtras() }
        btnContinuarComprando.setOnClickListener {
            (context as MainContract.View).mostrarFragment(InicioProductosFragment.instance())
        }
        btnPagar.setOnClickListener {
            if(presionar_recarga){
                mPresenter?.intentaProcederPago(this.carrito)
            }else{
                val dialog = WADialog.Builder(context!!)
                    .error()
                    .setTitle(" ")
                    .setMessage("Para poder continuar debes recalcular tú carrito")
                    .setButtonPositive("Ok", object : WADialog.OnClickListener {
                        override fun onClick() {
                        }
                    }).build()
                dialog.show()
            }

        }
        btnRecargar.setOnClickListener {
            mPresenter?.recargarCarrito(this.carrito)
        }
    }

    override fun showProductos(carr: ArrayList<Carrito>) {
        this.carrito.clear()
        this.carrito.addAll(carr)
        var item_num = this.carrito.size
        (context as MainActivity).badgeCar.text = item_num.toString()
        mAdapter.notifyDataSetChanged()

        if(carrito.size == 0){
            layout_connproductos.visibility = View.GONE
            layout_sinproductos.visibility = View.VISIBLE
        }else{
            layout_connproductos.visibility = View.VISIBLE
            layout_sinproductos.visibility = View.GONE
            var total = 0.0
            for (pro in carrito!!) {
                total += pro.total!!
            }
            var preciomxm = Currency.toCurrency(total, "MXN")
            tvTotal.text = "Total "+ preciomxm
            presionar_recarga = true
            btnRecargar.visibility = View.GONE
        }

    }

    override fun showRecargarProductos() {
        val id_carrito = SessionPrefs.getInstance(context!!).getIdSC()
        mPresenter?.requestProductosEnCarrito(id_carrito)
    }

    override fun showEliminar() {
        var cadena = (context as MainActivity).badgeCar.text.toString()
        var item_num = cadena.toInt() - 1
        (context as MainActivity).badgeCar.text = item_num.toString()
        val id_carrito = SessionPrefs.getInstance(context!!).getIdSC()
        mPresenter?.requestProductosEnCarrito(id_carrito)
    }

    override fun showErrorEliminar(err : String) {
        Snackbar.make(vw, err, Snackbar.LENGTH_LONG).show()
    }

    override fun showErrorActualizar(err : String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun showNetworkError() {
        Snackbar.make(vw, "Sin conexion a internet", Snackbar.LENGTH_LONG).show()
    }

    override fun setPresenter(presenter: CarritoContract.Presenter) {
        mPresenter = presenter
    }

    override fun irTarjetas(c: ArrayList<Carrito>) {
        val fragment = FormaPagoFragment.instance() as FormaPagoFragment
        fragment.setCarrito(c)
        (context as MainActivity).mostrarFragment(fragment)
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = CarritoFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }

}