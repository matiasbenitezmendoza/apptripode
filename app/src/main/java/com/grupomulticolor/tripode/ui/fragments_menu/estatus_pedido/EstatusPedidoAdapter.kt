package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.util.Currency

class EstatusPedidoAdapter(var pedidos: ArrayList<Pedido>, var listenerView: ListenerView)
    : RecyclerView.Adapter<EstatusPedidoAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val vw = LayoutInflater.from(context).inflate(R.layout.item_cardview_pedido, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return pedidos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = pedidos[position]
        holder.bind(item)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvStatusPedido: TextView = itemView.findViewById(R.id.tvStatusPedido)
        val tvNumPedido: TextView = itemView.findViewById(R.id.tvNumPedido)
        val tvCantidadProductos: TextView = itemView.findViewById(R.id.tvCantidadProductos)
        val tvInfoProductos: TextView = itemView.findViewById(R.id.tvInfoProductos)
        val tvPrecioProducto: TextView = itemView.findViewById(R.id.tvPrecioProducto)

        fun bind(pedido: Pedido) {
           itemView.setOnClickListener { listenerView.onClick(pedido) }
            tvNumPedido.text = "#"+String.format("%08d",Integer.parseInt(pedido.orden))
            tvCantidadProductos.text = "Producto"
            tvInfoProductos.text = ""

            tvStatusPedido.text = "EN CURSO"

            if(pedido.status_pedido!! > 3){
                tvStatusPedido.text = "ENTREGADO"
            }

            if(pedido.detallePedido != null) {
                var prefix = ""
                if (pedido.detallePedido!!.size > 1) {
                    prefix = "s"
                }
                tvCantidadProductos.text = ""+pedido.detallePedido!!.size+" Producto" + prefix

                var infoProd = ""
                var total : Double = 0.0
                for (prod in pedido.detallePedido!!) {
                    infoProd += "${prod.producto},\n"
                    total += prod.precio_total!!
                }
                tvInfoProductos.text = infoProd
                tvPrecioProducto.text = Currency.toCurrency(total, "MXN")
            }

        }
    }

    interface ListenerView {
        fun onClick(item: Pedido)
    }
}