package com.grupomulticolor.tripode.ui.fragments_menu.productos

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.slider.Sliders

interface ProductosContract {

    interface View : BaseView<Presenter> {
        fun muestraProgress(show: Boolean)
        fun muestraErrorEnConexion()
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)
        fun muestrarCarritoVacio(err: String)
        fun muestraMensajesSuccess(mensaje: String)
        fun agregarProductosFilterList(productos: ArrayList<Producto>)
        fun agregarSlidersFilterList(imagenes: ArrayList<Sliders>)
        fun agregarNumeroItem(carrito: ArrayList<Carrito>)

    }

    interface Presenter : BasePresenter {
        fun getproductos(search: String, limit: Int, offset : Int, oa: Int, op: Int)
        fun getproductosinicio(search: String, limit: Int, offset : Int, oa: Int, op: Int)
        fun getslider()
        fun getcarrito(id: Int)
    }

}