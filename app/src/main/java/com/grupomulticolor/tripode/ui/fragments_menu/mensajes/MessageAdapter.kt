package com.grupomulticolor.tripode.ui.fragments_menu.mensajes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.chat.MessageChat
import com.grupomulticolor.tripode.util.Fecha
import com.grupomulticolor.tripode.util.config.Constants

class MessageAdapter(var messages: ArrayList<MessageChat>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var context: Context

    object TIPO {
        const val ADMIN = 1
        const val USER = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        return if (viewType == TIPO.ADMIN) {
            val vw = LayoutInflater.from(context).inflate(R.layout.item_chat_receptor, parent, false)
            ViewHolderAdminMessage(vw)
        } else {
            val vw = LayoutInflater.from(context).inflate(R.layout.item_chat_emiter, parent, false)
            ViewHolderUserMessage(vw)
        }
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == TIPO.ADMIN) {
            (holder as ViewHolderAdminMessage).bind(
                messages[position]
            )
        } else {
            (holder as ViewHolderUserMessage).bind(
                messages[position]
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = messages[position]
        if (item.idUsuario == -1) return TIPO.ADMIN
        return TIPO.USER
    }


    inner class ViewHolderAdminMessage(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvNombreReceptor: TextView = itemView.findViewById(R.id.tvNombreReceptor)
        val tvMsgReceptor: TextView = itemView.findViewById(R.id.tvMsgReceptor)
        val tvFecha: TextView = itemView.findViewById(R.id.tvFecha)

        fun bind(item: MessageChat) {
            tvNombreReceptor.text = context.getString(R.string.farmacias_tripode)
            tvMsgReceptor.text = item.message
            val cal = Fecha.strToCalendar(item.fecha, Constants.FORMAT_DATE)
            tvFecha.text = Fecha.formatDDMM_HOUR(cal).toLowerCase()
        }
    }

    inner class ViewHolderUserMessage(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvMsgReceptor: TextView = itemView.findViewById(R.id.tvMsgReceptor)
        val tvFecha: TextView = itemView.findViewById(R.id.tvFecha)

        fun bind(item: MessageChat) {
            tvMsgReceptor.text = item.message
            val cal = Fecha.strToCalendar(item.fecha, Constants.FORMAT_DATE)
            tvFecha.text = Fecha.formatDDMM_HOUR(cal).toLowerCase()
        }
    }
}