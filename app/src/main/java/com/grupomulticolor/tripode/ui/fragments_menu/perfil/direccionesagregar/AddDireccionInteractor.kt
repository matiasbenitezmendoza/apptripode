package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar

import android.content.Context
import android.text.TextUtils
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.direcciones.source.DireccionService
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.residencial.source.ResidencialService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.Network
import com.grupomulticolor.tripode.util.config.Constants
import java.util.regex.Pattern

class AddDireccionInteractor(var context: Context) {

    private val direccionService = DireccionService.instance(context)
    private val residencialService = ResidencialService.instance(context)

    fun addDireccion(direccion: Direccion, callback: Callback) {
        if (!isValidNombre(direccion.nombreReferencia, callback)) return
        if (!isValidCalle(direccion.calle, callback)) return
        if (!isValidNumExt(direccion.noExt, callback)) return
        if (!isValidCondigoPostal(direccion.codigoPostal, callback)) return
        //if (!isValidEstado(direccion.estado, callback)) return
        //if (!isValidColonia(direccion.colonia, callback)) return
        //if (!isValidCiudad(direccion.ciudad, callback)) return
        if (!isNetworkAvalible(callback)) return

        if (direccion.id != 0)
            putDireccion(direccion, callback)
        else
            postDireccion(direccion, callback)
    }

    /*
    fun getDireccionPostal(cp: String?, callback: Callback) {
        if (!isValidCondigoPostal(cp, callback)) return
        consultarCodigoPostal(cp!!, callback)
    }*/

    private fun postDireccion(direccion: Direccion, callback: Callback) {

        direccionService.postDireccion(direccion, object : ResponseListener<ResponseData<Direccion>> {
            override fun onSuccess(response: ResponseData<Direccion>) {
                val msg = context.getString(R.string.mensaje_direccion_agregada)
                callback.onRequestUpdateSucces(msg)
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })

    }

    fun getResidencias(callback: Callback) {
        residencialService.get(object : ResponseListener<ResponseData<ArrayList<Residencial>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Residencial>>) {
                try {
                    callback.onRequestResidencias(response.data as ArrayList<Residencial>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getEstados(callback: Callback) {
        direccionService.getEstados(object : ResponseListener<ResponseData<ArrayList<Estados>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Estados>>) {
                try {
                    callback.onRequestEstados(response.data as ArrayList<Estados>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getMunicipios(mun: String, callback: Callback) {
        direccionService.getMunicipios(mun,object : ResponseListener<ResponseData<ArrayList<Municipios>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Municipios>>) {
                try {
                    callback.onRequestMunicipios(response.data as ArrayList<Municipios>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getColonias(est: String, mun: String, callback: Callback) {
        direccionService.getColonias(est,mun,object : ResponseListener<ResponseData<ArrayList<Colonias>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Colonias>>) {
                try {
                    callback.onRequestColonias(response.data as ArrayList<Colonias>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    private fun putDireccion(direccion: Direccion, callback: Callback) {
        direccionService.putDireccion(direccion, object : ResponseListener<ResponseData<Direccion>> {
            override fun onSuccess(response: ResponseData<Direccion>) {
                val msg = context.getString(R.string.mensaje_direccion_actualizada)
                callback.onRequestUpdateSucces(msg)
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    /* comprobar codigo postal */
    /* private fun consultarCodigoPostal(cp: String, callback: Callback) {
        direccionService.getDireccionPostal(cp, object : ResponseListener<ResponseData<DireccionPostal>> {
            override fun onSuccess(response: ResponseData<DireccionPostal>) {
                try {
                    callback.onRequestCodigoPostalSucces(response.data as DireccionPostal)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }*/

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }

    /* verifica que el nombre ingresado sea correcto*/
    private fun isValidNombre(nombre: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(nombre)) {
            callback.onNombreError(context.getString(R.string.error_item_empty))
            return false
        }
        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(nombre).matches()) {
            callback.onNombreError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* verifica que la calle sea correcta*/
    private fun isValidCalle(calle: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(calle)) {
            callback.onCalleError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(calle).matches()) {
            callback.onCalleError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el num ext ingresado sea correcto*/
    private fun isValidNumExt(numExt: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(numExt)) {
            callback.onExtError(context.getString(R.string.error_item_empty))
            return false
        }

        return true
    }

    /* verifica que la residencia seleccionada sea correcta*/
    private fun isValidResidencial(residencial: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(residencial)) {
            callback.onResidencialError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidCondigoPostal(cp: String?, callback: Callback) : Boolean  {
        if (TextUtils.isEmpty(cp)) {
            callback.onCPError(context.getString(R.string.error_item_empty))
            return false
        }
        val patron = Pattern.compile(Constants.patternNumber)
        if (!patron.matcher(cp).matches()) {
            callback.onCPError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidEstado(estado: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(estado)) {
            //callback.onEstadoError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidColonia(colonia: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(colonia)) {
            callback.onColoniaError(context.getString(R.string.error_match))
            return false
        }

        if (context.getString(R.string.spinner_header_colonia) == colonia) {
            callback.onColoniaError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    private fun isValidCiudad(ciudad: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(ciudad)) {
            callback.onCiudadError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    interface Callback {
        fun onNombreError(err: String)
        fun onCalleError(err: String)
        fun onExtError(err: String)
        fun onResidencialError(err: String)
        fun onCPError(err: String)
        fun onColoniaError(err: String)
        fun onCiudadError(err: String)

        fun onNetworkConnectionFailed()
        fun onRequestFailed(err: String)
        fun onRequestAddSuccess(msg: String)
        fun onRequestUpdateSucces(msg: String)
        fun onRequestCodigoPostalSucces(direccionPostal: DireccionPostal)
        fun onRequestEstados(estados: ArrayList<Estados>)
        fun onRequestColonias(estados: ArrayList<Colonias>)
        fun onRequestMunicipios(municipios: ArrayList<Municipios>)
        fun onRequestResidencias(residencias: ArrayList<Residencial>)

    }
}