package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.cfdi.Cfdi
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.residencial.Residencial

interface AddFacturacionContract {

    interface View : BaseView<Presenter> {
        fun presentarDatos()
        fun mostrarErrorEnRFC(err: String)
        fun mostrarErrorEnRazon(err: String)
        fun mostrarErrorEnCalle(err: String)
        fun mostrarErrorEnNoExt(err: String)
        fun muestraErrorEnCodigoPostal(error: String)
        fun muestraErrorEnResidencia(error: String)
        fun muestraErrorEnEstado(error: String)
        fun muestraErrorEnColonia(error: String)
        fun muestraErrorEnDelMun(error: String)
        fun muestraErrorEnCfdi(error: String)

        fun muestraErrorEnConexion()
        fun muestraMensajeDeExito(msg: String)
        fun muestraMensajeDeError(error: String)
        fun setDireccionEntrega(direccion: Direccion)
        fun muestraDatosResidencial(residencial: ArrayList<Residencial>)
        fun muestraDatosEstados(estados: ArrayList<Estados>)
        fun muestraDatosMunicipios(estados: ArrayList<Municipios>)
        fun muestraDatosColonias(colonias: ArrayList<Colonias>)
        fun muestraDatosCfdi(cfdi: ArrayList<Cfdi>)

    }

    interface Presenter : BasePresenter {
        fun intentaAgregarFacturacion(facturacion: Facturacion)
        fun consultarDireccionEntrega()
        fun consultarEstados()
        fun consultarMunicipios(mun: String)
        fun consultarColonias(mun: String,col: String)
        fun consultarCfdi()
        fun consultarResidencial()
    }
}