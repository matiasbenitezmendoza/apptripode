package com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones


import android.content.Context
import android.util.Log
import com.grupomulticolor.tripode.data.producto.ProductoPromocion
import com.grupomulticolor.tripode.data.producto.source.ProductoService
import com.grupomulticolor.tripode.data.promociones.Promociones
import com.grupomulticolor.tripode.data.promociones.source.PromocionesService
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener


class PromocionesPresenter( val mProductoView : PromocionesContract.View, var context: Context) : PromocionesContract.Presenter{

    private val service: ProductoService = ProductoService.instance(context)
    private val servicePromociones: PromocionesService = PromocionesService.instance(context)

    init {
        mProductoView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
            getpromociones()
    }

    override fun getpromociones() {

        servicePromociones.get(object :
            ResponseListener<ResponseDataTripode<ArrayList<Promociones>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<Promociones>>) {
                    var promociones = response.data as ArrayList<Promociones>
                        for (l in promociones) {
                             getproductosPromocion(l.id!!)
                            Log.d("TAG","d "+l.id)
                        }
            }

            override fun onError(t: Throwable) {
                mProductoView.muestrarErrorAlCargar("")
            }

        })
    }


    override fun getproductosPromocion(idPromocion: Int) {

        service.getProductosPromocion(idPromocion, object :
            ResponseListener<ResponseDataTripode<ArrayList<ProductoPromocion>>> {
            override fun onSuccess(response: ResponseDataTripode<ArrayList<ProductoPromocion>>) {
               //  var productoCount = response.data as ProductoCount
                mProductoView.agregarProductosFilterList(response.data as ArrayList<ProductoPromocion>,0)
            }
            override fun onError(t: Throwable) {
                mProductoView.muestrarErrorAlCargar(t.message!!)
            }

        })
    }


}