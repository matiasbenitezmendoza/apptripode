package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.repartidor.Repartidor

interface DetalleEstatusContract {

    interface View : BaseView<Presenter> {
        fun showInfoRepartidor(repartidor: Repartidor)
        fun showInfoSinRepartidor()
        fun showErrorNetwork()
        fun showDetalleEstatus(status: Int, orden: String)
    }

    interface Presenter: BasePresenter {
        fun cargarPedido()
        fun RequestRepartidor(id: Int)
        fun RequestPedido()
    }
}