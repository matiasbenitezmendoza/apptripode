package com.grupomulticolor.tripode.ui.fragments_menu.carrito.confirmarpedido

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.detallepedido.Detallepedido
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleEstatusContract
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleEstatusFragment
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleEstatusPresenter
import com.grupomulticolor.tripode.ui.fragments_menu.productos.InicioProductosFragment
import com.grupomulticolor.tripode.util.Currency
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_confirmar_pedido.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

class ConfirmarPedidoFragment: FragmentChildNotFab(), ConfirmarPedidoContract.View {

    private var mPresenter: ConfirmarPedidoContract.Presenter? = null
    private lateinit var vw: View
    private lateinit var mAdapter: ConfirmarPedidoAdapter
    private var pedido: Pedido? = null
    private val detallepedidos: ArrayList<Detallepedido> = ArrayList()
    private lateinit var prefs: SessionPrefs
    private var total_compra = 0.0

    private var carrito = ArrayList<Carrito>()
    private var tarjeta: TarjetaBancaria? = null
    private var direccion: Direccion? = null
    private var status_pago_pedido: Int = 2



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_confirmar_pedido, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ConfirmarPedidoPresenter(this, context!!)
        prefs = SessionPrefs(context!!)

        initAdapter()
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initAdapter() {
        mAdapter = ConfirmarPedidoAdapter(carrito!!,direccion,tarjeta)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter

    }

    private fun initEvents() {

        btnBack.setOnClickListener { (context as MainActivity).irAtras() }
        btnContinuar.setOnClickListener {
            var total = 0.0
            for (pro in carrito!!) {
                total += pro.total!!
            }

            var preciomxm = Currency.toCurrency(total, "MXN")
            var tamanio = preciomxm.length
            var preciomxmf = preciomxm.replace(',','.')
            var totalredon = preciomxmf.substring(1, tamanio-4)

            Log.d("TAG", "Orden mxm:" + preciomxm)
            Log.d("TAG", "Orden redondea:" + totalredon)

            var t = totalredon.toDouble()

            Log.d("TAG", "Orden parseado:" + t)

            total_compra = t
            mPresenter?.requestPagoCompra(tarjeta, t)
        }

    }

    override fun setOrden(orden: String) {
      /*
        var id_orden = orden.substring(3)
        Log.d("TAG", "Orden :" + id_orden)

        val tPago = prefs.getTipoPago()
        val tEnvio = prefs.getTipoEnvio()

        pedido =  Pedido(0, 0, -1, id_orden,"2019-12-12",1,"","", "",
            "", tPago,status_pago_pedido, "", "", "", "", "", "","", "",
            "", "","","","","","","","","",tEnvio,null)
        pedido!!.setDireccion(direccion)
        mPresenter?.intentaFinalizarCompra(pedido!!)
      */
    }

    override fun showErrorOrden() {
        Log.d("TAG", "Error Orden :"   )

    }

    override fun showPago(status: Int) {

        status_pago_pedido = status
        val tPago = prefs.getTipoPago()
        val tEnvio = prefs.getTipoEnvio()
        var phubicacion: Int = 0
        var tarjetanumero: String = ""
        var mes_tarjeta: Int = 0
        var anio_tarjeta: Int = 0

        if(direccion != null){
            phubicacion = direccion!!.id
        }

        if(tarjeta != null){
             tarjetanumero = tarjeta!!.noTarjeta
             mes_tarjeta =  tarjeta!!.mesVen
             anio_tarjeta =  tarjeta!!.anioVen
        }

        pedido =  Pedido(0, 0, -1, "",phubicacion,"2019-12-12",1,"","", "",
            "", tPago,status_pago_pedido, "", "", "", "", "", "","", "",
            "", "","","","","","","","","",tEnvio,total_compra,tarjetanumero, mes_tarjeta, anio_tarjeta, null)
        pedido!!.setDireccion(direccion)
        mPresenter?.intentaFinalizarCompra(pedido!!)
        /*
        var phubicacion: Int = 0
        var gson = Gson()
            var jsonString = gson.toJson(Pago("","xxxxxxxxxxxxxxxx",0,0,0,2,0.0,0.0,0,false,2))

        if(direccion != null){
            phubicacion = direccion!!.id
        }
        val id_ldcom = SessionPrefs.getInstance(context!!).getIdLdcom()

        mPresenter?.requestOrden("", phubicacion, jsonString, id_ldcom, 0, 0)*/

    }

    override fun showErrorPago() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(context!!.getString(R.string.mensaje_error_pedido_finalizado))
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun intentarAgregarDetalle(ped_init: Pedido){
        detallepedidos.clear()
        pedido = ped_init
        for (l in carrito!!) {
            detallepedidos.add(
                Detallepedido(
                             0,
                              ped_init.id,
                              l.itemId,
                              l.cardId,
                              ped_init.orden,
                              l.nombre,
                              l.cantidad,
                              l.precio,
                              l.total
                             )
            )
        }
        mPresenter?.requestDetallesProducto(detallepedidos)

    }

    override fun showPedidoFinalizado(){
        (context as MainActivity).badgeCar.text = "0"

        if(pedido!!.tipo_pago == 1){
            val dialog = WADialog.Builder(context!!)
                .succes()
                .setTitle("Completado")
                .setMessage(context!!.getString(R.string.mensaje_pedido_finalizado))
                .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar_pedido_finalizado), object : WADialog.OnClickListener {
                    override fun onClick() {
                        fragmentTransaction(InicioProductosFragment.instance())
                    }
                })
                .setButtonPositive(context!!.getString(R.string.mensaje_btn_pedido_finalizado), object : WADialog.OnClickListener {
                    override fun onClick() {
                        DetalleEstatusPresenter(
                            DetalleEstatusFragment.instance() as DetalleEstatusContract.View, pedido!!, context!!)
                        fragmentTransaction(DetalleEstatusFragment.instance())

                    }
                }).build()
            dialog.show()
        }
        else{
            val dialog = WADialog.Builder(context!!)
                .succes()
                .setTitle("Completado")
                .setMessage("Su pago será al momento de recibir su pedido")
                .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar_pedido_finalizado), object : WADialog.OnClickListener {
                    override fun onClick() {
                        fragmentTransaction(InicioProductosFragment.instance())
                    }
                })
                .setButtonPositive(context!!.getString(R.string.mensaje_btn_pedido_finalizado), object : WADialog.OnClickListener {
                    override fun onClick() {
                        DetalleEstatusPresenter(
                            DetalleEstatusFragment.instance() as DetalleEstatusContract.View, pedido!!, context!!)
                        fragmentTransaction(DetalleEstatusFragment.instance())

                    }
                }).build()
            dialog.show()
        }

    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    override fun showErrorDetallePedido(msj : String){
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(context!!.getString(R.string.mensaje_error_pedido_finalizado))
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun showErrorPedido(msj : String){
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(msj)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()

    }

    override fun showProductos(carr: ArrayList<Carrito>) {
        carrito.clear()
        carrito.addAll(carr)
        mAdapter.notifyDataSetChanged()
    }

    override fun cancelar() {
    }

    override fun showConectionError() {
        Snackbar.make(vw, "ir a confirmar", Snackbar.LENGTH_LONG).show()
    }

    override fun setPresenter(presenter: ConfirmarPedidoContract.Presenter) {
        mPresenter = presenter
    }


    fun setDatos(c : ArrayList<Carrito>?, t : TarjetaBancaria?, d : Direccion?){
        //carrito = c
        tarjeta = t
        direccion = d
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
                    if (fragment == null) {
                        fragment = ConfirmarPedidoFragment()
                    }
                    return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }

}