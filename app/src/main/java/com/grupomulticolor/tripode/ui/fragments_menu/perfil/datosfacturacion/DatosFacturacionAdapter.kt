package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datosfacturacion

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.interfaces.Events

class DatosFacturacionAdapter(var datosFacturacion: ArrayList<Facturacion>,
                              var onClickEditar: Events,
                              var onClickEliminar: Events
) : RecyclerView.Adapter<DatosFacturacionAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val vw = inflater.inflate(R.layout.item_cardview_facturacion, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return datosFacturacion.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = datosFacturacion[position]
        holder.bind(item)
        holder.btnEliminar.setOnClickListener { onClickEliminar.onClick(position) }
        holder.btnEditar.setOnClickListener { onClickEditar.onClick(position) }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val btnEliminar: ImageButton = itemView.findViewById(R.id.btnEliminar)
        val btnEditar: ImageButton = itemView.findViewById(R.id.btnEditar)
        val tvDireccion: TextView = itemView.findViewById(R.id.tvDireccion)

        fun bind(item: Facturacion) {
            tvTitle.text = item.nombre_razonSocial
            tvDireccion.text = detalleFacturacion(item)
        }

        private fun detalleFacturacion(item: Facturacion) : String {
            return "${item.rfc}\n${item.calle}\n" +
                    "Col ${item.colonia}\n" +
                    "C.P. ${item.codigoPostal}\n" +
                    "${item.municipio}\n" +
                    "Cfdi: ${item.cfdi_name}"
        }
    }
}