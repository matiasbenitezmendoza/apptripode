package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add

import com.grupomulticolor.tripode.data.cfdi.Cfdi
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.residencial.Residencial

class AddFacturacionPresenter(
    val mView: AddFacturacionContract.View,
    val mInteractor: AddFacturacionInteractor
) : AddFacturacionContract.Presenter, AddFacturacionInteractor.Callback{



    init {
        mView.setPresenter(this)
    }

    override fun start() {
        mView.presentarDatos()
    }

    override fun intentaAgregarFacturacion(facturacion: Facturacion) {
        mInteractor.addFacturacion(facturacion, this)
    }

    override fun onDireccionEntrega(direccion: Direccion) {
        mView.setDireccionEntrega(direccion)
    }

    override fun onRFCError(err: String) {
        mView.mostrarErrorEnRFC(err)
    }

    override fun onRazonError(err: String) {
        mView.mostrarErrorEnRazon(err)
    }

    override fun onCalleError(err: String) {
        mView.mostrarErrorEnCalle(err)
    }

    override fun onExtError(err: String) {
        mView.mostrarErrorEnNoExt(err)
    }

    override fun onResidencialError(err: String) {
        mView.muestraErrorEnResidencia(err)
    }

    override fun onCPError(err: String) {
        mView.muestraErrorEnCodigoPostal(err)
    }


    override fun onCfdiError(err: String) {
        mView.muestraErrorEnCfdi(err)
    }


    override fun onEstadoError(err: String) {
        mView.muestraErrorEnEstado(err)
    }

    override fun onColoniaError(err: String) {
        mView.muestraErrorEnColonia(err)
    }

    override fun onMunDelError(err: String) {
        mView.muestraErrorEnDelMun(err)
    }

    override fun onNetworkConnectionFailed() {
        mView.muestraErrorEnConexion()
    }

    override fun onRequestFailed(err: String) {
        mView.muestraMensajeDeError(err)
    }

    override fun onRequestAddSuccess(msg: String) {
        mView.muestraMensajeDeExito(msg)
    }

    override fun onRequestUpdateSucces(msg: String) {
        mView.muestraMensajeDeExito(msg)
    }

    override fun consultarDireccionEntrega() {
        mInteractor.existeDireccionEntregaSeleccionada(this)
    }


    override fun onRequestEstados(estados: ArrayList<Estados>) {
        mView.muestraDatosEstados(estados)
    }

    override fun onRequestResidencial(residencial: ArrayList<Residencial>) {
        mView.muestraDatosResidencial(residencial)
    }

    override fun onRequestMunicipios(municipios: ArrayList<Municipios>) {
        mView.muestraDatosMunicipios(municipios)
    }

    override fun onRequestColonias(colonias: ArrayList<Colonias>) {
        mView.muestraDatosColonias(colonias)
    }

    override fun onRequestCfdi(cfdi: ArrayList<Cfdi>) {
        mView.muestraDatosCfdi(cfdi)
    }

    override fun consultarEstados() {
        mInteractor.getEstados(this)
    }

    override fun consultarMunicipios(mun: String) {
        mInteractor.getMunicipios(mun,this)
    }

    override fun consultarColonias(est: String,mun: String) {
        mInteractor.getColonias(est, mun,this)
    }

    override fun consultarCfdi() {
        mInteractor.getCfdi(this)
    }

    override fun consultarResidencial() {
        mInteractor.getResidencial(this)
    }

}