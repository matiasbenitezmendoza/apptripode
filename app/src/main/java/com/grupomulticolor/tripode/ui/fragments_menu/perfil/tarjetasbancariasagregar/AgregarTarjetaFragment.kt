package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.grupomulticolor.tripode.App
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.objects.WADialogProgressBuilder
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.util.Input
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import com.grupomulticolor.walibray.dialog.WADialogProgress
import kotlinx.android.synthetic.main.fragment_nueva_tarjeta.*
import com.grupomulticolor.tripode.util.DeviceIdFragment
import mx.openpay.android.model.Token


class AgregarTarjetaFragment : FragmentChildNotFab(), AgregarTarjetaContract.View {

    private lateinit var vw: View
    private lateinit var waprogress: WADialogProgress
    private var mPresenter: AgregarTarjetaContract.Presenter? = null
    private lateinit var cntx: Context
    private var deviceIdFragment: DeviceIdFragment? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_nueva_tarjeta, container, false)
        return  vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cntx = context!!
        waprogress = WADialogProgressBuilder.getProgress(cntx)
        val openpay = ((context as MainActivity).application as App).openpay
        val interactor = AgregarTarjetaInteractor(cntx, openpay)
        AgregarTarjetaPresenter(this, interactor)

        this.deviceIdFragment = fragmentManager?.findFragmentByTag("DeviceCollector") as DeviceIdFragment?
        // If not retained (or first time running), we need to create it.
        if (this.deviceIdFragment == null) {
            this.deviceIdFragment = DeviceIdFragment()
            fragmentManager?.beginTransaction()?.add(deviceIdFragment!!, "DeviceCollector")?.commit()
        }
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initEvents() {
        inputNombre.requestFocus()
        agregaTextListenerAInput(inputNombre)
        agregaTextListenerAInput(inputNoTarjeta)
        agregaTextListenerAInput(inputFechaVencimiento)
        agregaTextListenerAInput(inputCvv)
        btnBack.setOnClickListener { irAtras() }
        btnGuardar.setOnClickListener { guardarTarjeta() }


        // mask para tarjeta bancaria
        inputNoTarjeta.inputType = InputType.TYPE_CLASS_NUMBER
        inputNoTarjeta.keyListener = DigitsKeyListener.getInstance("1234567890+-() ")
        val listenerNumberCard = MaskedTextChangedListener("[0000] [0000] [0000] [0000]", inputNoTarjeta)
        inputNoTarjeta.addTextChangedListener(listenerNumberCard)
        inputNoTarjeta.onFocusChangeListener = listenerNumberCard

        //mask para fecha de vencimiento
        inputFechaVencimiento.inputType = InputType.TYPE_CLASS_NUMBER
        inputFechaVencimiento.keyListener = DigitsKeyListener.getInstance("1234567890+/-() ")
        val listenerfechaCard = MaskedTextChangedListener("[00]{/}[00]", inputFechaVencimiento)
        inputFechaVencimiento.addTextChangedListener(listenerfechaCard)
        inputFechaVencimiento.onFocusChangeListener = listenerfechaCard

        // mask para cvv
        inputCvv.inputType = InputType.TYPE_CLASS_NUMBER
        inputCvv.keyListener = DigitsKeyListener.getInstance("1234567890+-() ")
        val listenerCvv = MaskedTextChangedListener("[000]", inputCvv)
        inputCvv.addTextChangedListener(listenerCvv)
        inputCvv.onFocusChangeListener = listenerCvv
    }


    private fun agregaTextListenerAInput(editText: EditText) {
        editText.addTextChangedListener( object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Input.onClear(cntx, editText)
            }
        })
    }

    private fun guardarTarjeta() {
        val tarjeta = TarjetaBancaria()


        tarjeta.nombreCuentaHabiente = inputNombre.text.toString()
        tarjeta.noTarjeta = inputNoTarjeta.text.toString()
        tarjeta.noTarjeta = tarjeta.noTarjeta.replace(" ", "")
        try {
            val fechaVenc = inputFechaVencimiento.text.toString().split("/")

            if (fechaVenc.size == 2) {
                tarjeta.anioVen = fechaVenc[1].toInt()
                tarjeta.mesVen = fechaVenc[0].toInt()
            }
        } catch (e : Exception) {
            e.printStackTrace()
        }

        tarjeta.cvv = inputCvv.text.toString()
        mPresenter?.intentaAgregartarjeta(tarjeta)

    }

    override fun mostrarErrorEnNombreTarjeta(err: String) {
        Input.onError(cntx, inputNombre, err)
    }

    override fun mostrarErrorEnNumTarjeta(err: String) {
        Input.onError(cntx, inputNoTarjeta, err)
    }

    override fun mostrarErrorEnFechaVencimiento(err: String) {
        Input.onError(cntx, inputFechaVencimiento, err)
    }

    override fun mostrarErrorEnCVV(err: String) {
        Input.onError(cntx, inputCvv, err)
    }

    override fun mostrarProgress(show: Boolean) {
        if (show)
            waprogress.show()
        else
            waprogress.hide()
    }

    override fun mostrarErrorEnConexion() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(R.string.msg_sin_conexion)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }


    override fun mostrarMensajeError(resource: Int, msg: String?) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("¡Error!")
            .setMessage(resource)
            .setButtonPositive("Ok", null).build()
        dialog.show()
    }

    override fun mostrarMensajedeExito(resource: Int, resolve: () -> Unit) {
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage(resource)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                    resolve()
                }
            }).build()
        dialog.show()
    }

    override fun mostrarMensajeError(msg: String) {

        var mensaje = msg

        if (mensaje == "The card was declined") {
            mensaje = context!!.getString(R.string.declined)
        }

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("¡Error!")
            .setMessage(mensaje)
            .setButtonPositive("Ok", null).build()
        dialog.show()
    }

    override fun setPresenter(presenter: AgregarTarjetaContract.Presenter) {
        mPresenter = presenter
    }

    override fun onSuccesToken(token: Token?) {
        val card = token?.card

        if (card != null) {
            val tarjeta = TarjetaBancaria()
            tarjeta.noTarjeta = card.cardNumber.substring(12,16) ?: "" // ultimos 4 digitos de la tarjeta
            tarjeta.nombreCuentaHabiente = card.holderName ?: ""
            tarjeta.cvv = card.cvv2 ?: ""
            tarjeta.mesVen = card.expirationMonth.toInt()
            tarjeta.anioVen = card.expirationYear.toInt()
            tarjeta.token = token.id ?: ""
            tarjeta.tipo = card.brand
            if (deviceIdFragment != null) {
                tarjeta.deviceId = deviceIdFragment!!.deviceId.substring(4) ?: ""
            }
            mPresenter?.intentaGuardarTarjeta(tarjeta)
        }

    }

    override fun irAtras() {
        (context!! as MainActivity).irAtras()
    }

    companion object {

        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = AgregarTarjetaFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }

    }

}