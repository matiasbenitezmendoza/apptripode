package com.grupomulticolor.tripode.ui.tutorial.fragments

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.ui.tutorial.TutorialActivity
import kotlinx.android.synthetic.main.fragment_step_compra.*

class StepCompraFragment : Fragment() {




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_step_compra, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSaltar.paintFlags = btnSaltar.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        btnSaltar.setOnClickListener {
            (context as TutorialActivity).mostrarInicio()
        }

    }

    companion object {
        fun instace() : Fragment = StepCompraFragment()
    }
}