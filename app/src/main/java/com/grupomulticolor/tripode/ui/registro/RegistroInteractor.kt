package com.grupomulticolor.tripode.ui.registro

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.Colonias
import com.grupomulticolor.tripode.data.direcciones.DireccionPostal
import com.grupomulticolor.tripode.data.direcciones.Estados
import com.grupomulticolor.tripode.data.direcciones.Municipios
import com.grupomulticolor.tripode.data.direcciones.source.DireccionService
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.padecimientos.service.PadecimientoService
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.residencial.source.ResidencialService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.config.Constants
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.data.usuario.source.service.UsuarioService
import com.grupomulticolor.tripode.util.Fecha
import com.grupomulticolor.tripode.util.Network
import java.lang.Exception
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class RegistroInteractor(private val context: Context) {

    private val service: UsuarioService = UsuarioService.instance(context)
    private val direccionService: DireccionService = DireccionService(context)
    private val padecimientosService: PadecimientoService = PadecimientoService(context)
    private val residencialesService: ResidencialService = ResidencialService(context)

    fun singupUser(user: Usuario, callback: Callback) {
        if (!isValidNombre(user.nombre, callback)) return
        if (!isValidApellidoP(user.apellidoPaterno, callback)) return
        if (!isValidApellidoM(user.apellidoMaterno, callback)) return
        if (!isValidFechaNac(user.fechaNac, callback)) return
        if (!isValidCalle(user.calle, callback)) return
        if (!isValidNumExt(user.numExt, callback)) return
        // if (!isValidNumInt(user.numInt, callback)) return
        //if (!isValidResidencial(user.residencialId, callback)) return
        if (!isValidCondigoPostal(user.codigoPostal, callback)) return

        if (!isValidEmail(user.correo, callback)) return
        if (!isValidConfirmEmail(user.correo, user.correoConfirm, callback)) return
        if (!isValidPasswd(user.password, callback)) return
        if (!isValidConfirmPasswd(user.password, user.passwordConfirm, callback)) return
        if (!isValidTermsCondi(user.terminosCondiciones, callback)) return
        if (!isNetworkAvalible(callback)) return
        registrarUsuario(user, callback)
    }

    fun getDireccionPostal(cp: String?, callback: Callback) {
        if (!isValidCondigoPostal(cp, callback)) return
        consultarCodigoPostal(cp!!, callback)
    }

    fun getEstados(callback: Callback) {
        consultarEstados(callback)
    }

    fun getMunicipios(id: String, callback: Callback) {
        consultarMunicipios(id, callback)
    }

    fun getColonias(id: String, id_municipio: String, callback: Callback) {
        consultarColonias(id, id_municipio, callback)
    }

    fun getResidenciales(callback: Callback) {
        consultarResidenciales(callback)
    }


    fun getPadecimientos(callback: Callback) {
        consultarPadecimientos(callback)
    }

    private fun registrarUsuario(user: Usuario, callback: Callback) {
        Log.d(RegistroInteractor::class.java.name, "Registrar: $user")

        val arrayFechaNac = user.fechaNac?.split("/")
        var fecha = ""
        if (arrayFechaNac != null && arrayFechaNac.size == 3)
            fecha = "${arrayFechaNac[2]}-${arrayFechaNac[1]}-${arrayFechaNac[0]}"

        user.fechaNac = fecha

        service.post(user, object : ResponseListener<ResponseData<Usuario>> {
            override fun onSuccess(response: ResponseData<Usuario>) {
                callback.onSingupSuccess(response.message)
            }

            override fun onError(t: Throwable) {
                callback.onSingupFailed(t.message!!)
            }
        })
    }

    private fun consultarEstados(callback: Callback) {
        direccionService.getEstados(object : ResponseListener<ResponseData<ArrayList<Estados>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Estados>>) {
                try {
                    callback.onRequestEstadoSucces(response.data as ArrayList<Estados>)
                } catch (e: Exception) {
                    callback.onListadoError(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onListadoError(t.message!!)
            }
        })
    }

    private fun consultarMunicipios(id: String, callback: Callback) {
        direccionService.getMunicipios(id, object : ResponseListener<ResponseData<ArrayList<Municipios>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Municipios>>) {
                try {
                    callback.onRequestMunicipiosSucces(response.data as ArrayList<Municipios>)
                } catch (e: Exception) {
                    callback.onListadoError(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onListadoError(t.message!!)
            }
        })
    }

    private fun consultarColonias(id: String, id_mun : String, callback: Callback) {
        direccionService.getColonias(id, id_mun, object : ResponseListener<ResponseData<ArrayList<Colonias>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Colonias>>) {
                try {
                    callback.onRequestColoniaSucces(response.data as ArrayList<Colonias>)
                } catch (e: Exception) {
                    callback.onListadoError(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onListadoError(t.message!!)
            }
        })
    }

    private fun consultarResidenciales(callback: Callback) {
        residencialesService.get(object : ResponseListener<ResponseData<ArrayList<Residencial>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Residencial>>) {
                try {
                    callback.onRequestResidencialSucces(response.data as ArrayList<Residencial>)
                } catch (e: Exception) {
                    callback.onListadoError(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onListadoError(t.message!!)
            }
        })
    }


    /* comprobar codigo postal */
    private fun consultarCodigoPostal(cp: String, callback: Callback) {
        direccionService.getDireccionPostal(cp, object : ResponseListener<ResponseData<DireccionPostal>> {
            override fun onSuccess(response: ResponseData<DireccionPostal>) {
                try {
                    callback.onRequestCodigoPostalSucces(response.data as DireccionPostal)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    /* consultar los padecimientos */
    private fun consultarPadecimientos(callback: Callback) {
        padecimientosService.get( object : ResponseListener<ResponseData<ArrayList<Padecimiento>>> {

            override fun onSuccess(response: ResponseData<ArrayList<Padecimiento>>) {
                callback.onRequestPadecimientosSucces(response.data as ArrayList<Padecimiento>)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }

    /* verifica que el nombre ingresado sea correcto*/
    private fun isValidNombre(nombre: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(nombre)) {
            callback.onNombreError(context.getString(R.string.error_item_empty))
            return false
        }
        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(nombre).matches()) {
            callback.onNombreError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* verifica que el apellido paterno ingresado sea correcto*/
    private fun isValidApellidoP(apellidoPaterno: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(apellidoPaterno)) {
            callback.onApellidoPaternoError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(apellidoPaterno).matches()) {
            callback.onApellidoPaternoError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el apellido materno ingresado sea correcto*/
    private fun isValidApellidoM(apellidoMaterno: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(apellidoMaterno)) {
            callback.onApellidoMaternoError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(apellidoMaterno).matches()) {
            callback.onApellidoMaternoError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que la fecha de nacimiento ingresada sea correcta*/
    private fun isValidFechaNac(fechaNac: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(fechaNac)) {
            callback.onFechaNacError(context.getString(R.string.error_item_empty))
            return false
        }

        /* TODO reglas fecha nac */
        val arrFech = fechaNac?.split("/")

        if( arrFech?.size != 3) {
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }
        if (arrFech[2].length != 4) { // anio no es de 4 digitos
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }
        if (arrFech[1].length != 2) { // mes no es de 2 digitos
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }
        if (arrFech[0].length != 2) { // dia no es de 2 digitos
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }

        val anioActual = Fecha.currentYear().toInt()
        val anioNac = arrFech[2].toInt()

        if (anioNac >= anioActual) { // menor 1 anio
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }

        try {
            val year = arrFech[2].toInt()
            val month = arrFech[1].toInt()
            val dayOfMonth = arrFech[0].toInt()

            val calendar = Calendar.getInstance()
            calendar.isLenient = false
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month - 1) // [0,...,11]
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val date = calendar.time
            Log.d("date", ""+date)
        } catch (e : Exception) {
            e.printStackTrace()
            callback.onFechaNacError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que la calle sea correcta*/
    private fun isValidCalle(calle: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(calle)) {
            callback.onCalleError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(calle).matches()) {
            callback.onCalleError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el num ext ingresado sea correcto*/
    private fun isValidNumExt(numExt: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(numExt)) {
            callback.onNumExtError(context.getString(R.string.error_item_empty))
            return false
        }

        /*
        val patron = Pattern.compile(Constants.patternResidencial)
        if (!patron.matcher(numExt).matches()) {
            callback.onNumExtError(context.getString(R.string.error_match))
            return false
        }
        */

        return true
    }

    /* verifica que el num Int ingresado sea correcto*/
    private fun isValidNumInt(numExt: String?, callback: Callback) : Boolean {

        val patron = Pattern.compile(Constants.patternResidencial)
        if ( !TextUtils.isEmpty(numExt) && !patron.matcher(numExt).matches()) {
            callback.onNumIntError(context.getString(R.string.error_item_empty))
            return false
        }
        return true
    }

    private fun isValidCondigoPostal(cp: String?, callback: Callback) : Boolean  {
        if (TextUtils.isEmpty(cp)) {
            callback.onCodigoPostalError(context.getString(R.string.error_item_empty))
            return false
        }
        val patron = Pattern.compile(Constants.patternNumber)
        if (!patron.matcher(cp).matches()) {
            callback.onCodigoPostalError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* verifica que el correo ingresado sea valido*/
    private fun isValidEmail(email: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(email)) {
            callback.onCorreoError(context.getString(R.string.error_item_empty))
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onCorreoError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* verifica que el correo ingresado sea valido*/
    private fun isValidConfirmEmail(email: String?, emailConfirm: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(emailConfirm)) {
            callback.onCorreoConfirmError(context.getString(R.string.error_item_empty))
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailConfirm).matches()) {
            callback.onCorreoConfirmError(context.getString(R.string.error_match))
            return false
        }

        if (email != emailConfirm) {
            callback.onCorreoConfirmError(context.getString(R.string.error_email_confirm))
            return false
        }
        return true
    }

    /* verifica que el password ingresado sea valido*/
    private fun isValidPasswd(passwd: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(passwd)) {
            callback.onPasswdError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternPasswd)
        if (!patron.matcher(passwd).matches()) {
            callback.onPasswdError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    /* verifica que el password ingresado sea valido*/
    private fun isValidConfirmPasswd(passwd: String?, passwdConfirm: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(passwdConfirm)) {
            callback.onPasswdConfirmError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternPasswd)
        if (!patron.matcher(passwdConfirm).matches()) {
            callback.onPasswdConfirmError(context.getString(R.string.error_match))
            return false
        }

        if (passwd != passwdConfirm) {
            callback.onPasswdConfirmError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidTermsCondi(terms: Boolean, callback: Callback) : Boolean {
        if (!terms) {
            callback.onTermsCondiError(context.getString(R.string.error_terms))
            return false
        }
        return true
    }

    /* interface para notificar al presentador cualquier evento */
    interface Callback {
        fun onNombreError(err: String)
        fun onApellidoPaternoError(err: String)
        fun onApellidoMaternoError(err: String)
        fun onFechaNacError(err: String)
        fun onCalleError(err: String)
        fun onNumExtError(err: String)
        fun onNumIntError(err: String)
        fun onResidencialError(err: String)
        fun onCodigoPostalError(err: String)
        fun onListadoError(err: String)
        fun onCorreoError(err: String)
        fun onCorreoConfirmError(err: String)
        fun onPasswdError(err: String)
        fun onPasswdConfirmError(err: String)
        fun onTermsCondiError(err: String)
        fun onNetworkConnectionFailed()
        fun onSingupSuccess(msg: String)
        fun onSingupFailed(err: String)
        fun onRequestCodigoPostalSucces(direccionPostal: DireccionPostal)
        fun onRequestFailed(err: String)
        fun onRequestPadecimientosSucces(padecimientos: ArrayList<Padecimiento>)
        fun onRequestEstadoSucces(estados: ArrayList<Estados>)
        fun onRequestMunicipiosSucces(municipios: ArrayList<Municipios>)
        fun onRequestColoniaSucces(colonias: ArrayList<Colonias>)
        fun onRequestResidencialSucces(residencial: ArrayList<Residencial>)

    }
}
