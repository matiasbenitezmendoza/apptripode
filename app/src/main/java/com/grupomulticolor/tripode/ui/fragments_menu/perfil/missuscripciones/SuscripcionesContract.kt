package com.grupomulticolor.tripode.ui.fragments_menu.perfil.missuscripciones

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.suscripciones.Suscripcion
import com.grupomulticolor.tripode.data.temas.Tema

interface SuscripcionesContract {

    interface View : BaseView<Presenter> {
        fun irAtras()
        fun mostrarSuscripcion(suscripcion: Suscripcion?)
        fun mostrarError(msg: String?, resolve: () -> Unit)
        fun mostrarErrorConexion()
        fun mostrarTemas(temas: ArrayList<Tema>)
    }

    interface Presenter : BasePresenter {
        fun intentaSuscribirme(suscripcion: Suscripcion)
        fun getSuscripcion()
        fun getTemas()

    }
}