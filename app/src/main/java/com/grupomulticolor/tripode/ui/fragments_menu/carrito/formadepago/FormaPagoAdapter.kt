package com.grupomulticolor.tripode.ui.fragments_menu.carrito.formadepago

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.interfaces.Events

class FormaPagoAdapter(var tarjetas: ArrayList<TarjetaBancaria>,
                       private var onClickCheck: Events?)
    : RecyclerView.Adapter<FormaPagoAdapter.ViewHolder>() {

    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vw = inflate.inflate(R.layout.item_pago_tarjeta, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return tarjetas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = tarjetas[position]
        holder.bind(item)
        holder.cbSeleccionar.setOnClickListener {
            tarjetas[position].selected = !tarjetas[position].selected
            onClickCheck?.onClick(position)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        private val ivTipoTarjeta: ImageView = itemView.findViewById(R.id.ivTipoTarjeta)
        private val tvDetalle: TextView = itemView.findViewById(R.id.tvDetalle)
        val cbSeleccionar: CheckBox = itemView.findViewById(R.id.cbSeleccionar)

        fun bind(tarjeta: TarjetaBancaria) {
            tvDetalle.text = tarjeta.detalle()
            cbSeleccionar.isChecked = tarjeta.selected

            if(tarjeta.tipo == "mastercard") {
                Picasso
                    .get()
                    .load(R.drawable.ico_mc)
                    .into(ivTipoTarjeta)
                tvTitle.text = "Tarjeta MasterCard"

            }else{
                Picasso
                    .get()
                    .load(R.drawable.visa)
                    .into(ivTipoTarjeta)
                tvTitle.text = "Tarjeta Visa"

            }

        }
    }
}