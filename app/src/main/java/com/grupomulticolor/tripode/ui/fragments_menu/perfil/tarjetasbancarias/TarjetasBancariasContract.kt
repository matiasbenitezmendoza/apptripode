package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancarias

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria

interface TarjetasBancariasContract {

    interface View : BaseView<Presenter> {
        fun showTarjetas(tarjetas: ArrayList<TarjetaBancaria>)
        fun showNetworkError()
        fun showEmpty()
        fun showMessageError(err: String?)
    }

    interface Presenter : BasePresenter {
        fun loadTarjetas()
        fun eliminarTarjeta(itemId: Int)
    }
}