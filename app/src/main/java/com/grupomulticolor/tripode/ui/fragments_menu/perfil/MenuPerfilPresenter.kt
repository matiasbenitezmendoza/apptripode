package com.grupomulticolor.tripode.ui.fragments_menu.perfil

class MenuPerfilPresenter(val mMenuPerfilView: MenuPerfilContract.View,
                          val mMenuPerfilInteractor: MenuPerfilInteractor)
    : MenuPerfilContract.Presenter, MenuPerfilInteractor.Callback {

    init {
        mMenuPerfilView.setPresenter(this)
    }

    override fun cerrarSesion() {
        mMenuPerfilInteractor.cerrarSesion(this)
    }

    override fun start() {
        // cargar datos de usuario
        mMenuPerfilView.muestraInformacionUsuario()
    }

    override fun onLogOut() {
        mMenuPerfilView.irALogin()
    }

}