package com.grupomulticolor.tripode.ui.fragments_menu.perfil.infopersonal

import android.content.Context
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.usuario.Usuario

class InfoPersonalInteractor(private val context: Context) {

    fun requestInformacionPersonal(callback: Callback) {
        val usuario = SessionPrefs(context).currentUsu()
        callback.onRequestSuccess(usuario)
    }


    interface Callback {
        fun onRequestSuccess(usuario: Usuario)
        fun onRequestFailed()
        fun onNetworkConnectionFailed()
    }

}