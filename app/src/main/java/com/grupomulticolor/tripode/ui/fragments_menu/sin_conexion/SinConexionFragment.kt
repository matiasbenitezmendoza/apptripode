package com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab

class SinConexionFragment : FragmentParentNotFab() {

    private var vw: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.layout_sin_internet, container, false)
        return vw
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = SinConexionFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}