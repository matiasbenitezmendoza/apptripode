package com.grupomulticolor.tripode.ui.fragments_menu.perfil.missuscripciones

import android.content.Context
import android.util.Log
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.suscripciones.Suscripcion
import com.grupomulticolor.tripode.data.suscripciones.source.SuscripcionService
import com.grupomulticolor.tripode.data.temas.Tema
import com.grupomulticolor.tripode.data.temas.source.TemasService
import com.grupomulticolor.tripode.util.Network

class SuscripcionPresenter(val mView: SuscripcionesContract.View,
                           val context: Context) : SuscripcionesContract.Presenter {

    private val service: SuscripcionService = SuscripcionService.instance(context)
    private val temaService: TemasService = TemasService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        getTemas()
    }

    override fun intentaSuscribirme(suscripcion: Suscripcion) {
        if (isNetworkAvalible()) {
            if (suscripcion.id != 0)
                put(suscripcion)
            else
                post(suscripcion)
        }
    }

    private fun post(suscripcion: Suscripcion) {

        if (isNetworkAvalible()) {
            service.post(suscripcion, object : ResponseListener<ResponseData<Suscripcion>> {
                override fun onSuccess(response: ResponseData<Suscripcion>) {
                    mView.irAtras()
                }

                override fun onError(t: Throwable) {
                    mView.mostrarError(t.message) { mView.irAtras() }
                }
            })
        }
    }

    private fun put(suscripcion: Suscripcion) {
        if (isNetworkAvalible()) {
            service.put(suscripcion, object : ResponseListener<ResponseData<Suscripcion>> {
                override fun onSuccess(response: ResponseData<Suscripcion>) {
                    mView.irAtras()
                }

                override fun onError(t: Throwable) {
                    mView.mostrarError(t.message) { mView.irAtras() }
                }
            })
        }
    }

    override fun getSuscripcion() {
        if (isNetworkAvalible()) {
            service.get(object : ResponseListener<ResponseData<Suscripcion>> {
                override fun onSuccess(response: ResponseData<Suscripcion>) {
                    mView.mostrarSuscripcion(response.data)
                }

                override fun onError(t: Throwable) {
                    Log.d("ERROR", t.message)
                    //mView.mostrarError(t.message) {}
                }
            })
        }
    }

    override fun getTemas() {
        if (isNetworkAvalible()) {
            temaService.get(object : ResponseListener<ResponseData<ArrayList<Tema>>> {
                override fun onSuccess(response: ResponseData<ArrayList<Tema>>) {
                    try {
                        mView.mostrarTemas(response.data as ArrayList<Tema>)
                        getSuscripcion()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onError(t: Throwable) {
                }
            })
        }
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mView.mostrarErrorConexion()
        return isAvailable
    }


}