package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datosfacturacion

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.facturacion.Facturacion

interface DatosFacturacionContract {

    interface View : BaseView<Presenter> {
        fun showFacturacion(facturacion: ArrayList<Facturacion>)
        fun showEmpty()
        fun showMessageError(err: String?)
        fun showNetworkError()
    }

    interface Presenter : BasePresenter {
        fun loadFacturacion()
        fun eliminarFacturacion(itemId: Int)
    }

}