package com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.ui.fragments_menu.carrito.CarritoFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_detalle_producto.*
import kotlinx.android.synthetic.main.toolbar.*

class DetalleProductoFragment : FragmentChildNotFab(), DetalleContract.View {

    private lateinit var producto: Producto
    private val  mcarrito = ArrayList<Carrito>()
    lateinit var mAdapter: DetalleProductoAdapter
    private val productosRelacionados = ArrayList<Producto>()
    private lateinit var vw: View
    private var mPresenter: DetalleContract.Presenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw =  inflater.inflate(R.layout.fragment_detalle_producto, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
        val id_carrito = SessionPrefs.getInstance(context!!).getIdSC()
        mPresenter?.requestCarrito(id_carrito)
    }

    private fun initAdapter() {
        mAdapter = DetalleProductoAdapter(mPresenter!!, mcarrito)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.layoutManager = LinearLayoutManager(context!!)
    }

    override fun showProducto(producto: Producto) {
        this.producto = producto
        mAdapter.producto = producto
    }


    override fun showAddProducto() {
        val id_carrito = SessionPrefs.getInstance(context!!).getIdSC()
        mPresenter?.editarNotificacion(id_carrito)
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("Completado")
            .setMessage("¡Se agrego el producto a tu carrito!")
            .setButtonNegative(context!!.getString(R.string.mensaje_btn_productos_carrito), object : WADialog.OnClickListener {
                override fun onClick() {
                    (context as MainActivity).irAtras()
                }
            })
            .setButtonPositive(context!!.getString(R.string.mensaje_btn_ir_pago), object : WADialog.OnClickListener {
                override fun onClick() {
                    fragmentTransaction(CarritoFragment.instance())
                }
            }).build()
        dialog.show()
    }

    override fun showNotificacion(carrito : ArrayList<Carrito>) {
        var item_num = carrito.size
        (context as MainActivity).badgeCar.text = item_num.toString()
        mcarrito.clear()
        mcarrito.addAll(carrito)
        mAdapter.notifyDataSetChanged()
    }

    override fun setCarrito(carrito : ArrayList<Carrito>) {
        mcarrito.clear()
        mcarrito.addAll(carrito)
        mAdapter.notifyDataSetChanged()
    }


    override fun showError(msj: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(msj)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun showNotificacionError() {

    }

    override fun showCarritoVacio() {
        mcarrito.clear()
        mAdapter.notifyDataSetChanged()
    }

    override fun showProductosRelacionados(productos: ArrayList<Producto>) {
        productosRelacionados.clear()
        productosRelacionados.addAll(productos)
        mAdapter.productosRelacionados = productosRelacionados
        mAdapter.notifyDataSetChanged()
    }


    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }


    override fun setPresenter(presenter: DetalleContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DetalleProductoFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}