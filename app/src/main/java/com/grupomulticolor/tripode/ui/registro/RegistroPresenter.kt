package com.grupomulticolor.tripode.ui.registro

import com.grupomulticolor.tripode.data.direcciones.Colonias
import com.grupomulticolor.tripode.data.direcciones.DireccionPostal
import com.grupomulticolor.tripode.data.direcciones.Estados
import com.grupomulticolor.tripode.data.direcciones.Municipios
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.usuario.Usuario

class RegistroPresenter( val mRegistroView : RegistroContract.View,
                         val mRegistroInteractor: RegistroInteractor) : RegistroContract.Presenter, RegistroInteractor.Callback {

    init {
        mRegistroView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
        mRegistroInteractor.getPadecimientos(this)

    }

    override fun intentaRegistrarUsuario(user: Usuario) {
        mRegistroInteractor.singupUser(user, this)
    }

    override fun verTerminosYCondiciones() {
        // mostrar terminos y condiciones
    }

    /* TODO implementacion de interactor callback */
    override fun onNombreError(err: String) {
        mRegistroView.muestraErrorEnNombre(err)
    }

    override fun onApellidoPaternoError(err: String) {
        mRegistroView.muestraErrorEnApellidoPaterno(err)
    }

    override fun onApellidoMaternoError(err: String) {
        mRegistroView.muestraErrorEnApellidoMaterno(err)
    }

    override fun onFechaNacError(err: String) {
        mRegistroView.muestraErrorEnFechaNac(err)
    }

    override fun onCalleError(err: String) {
        mRegistroView.muestraErrorEnCalle(err)
    }

    override fun onNumExtError(err: String) {
        mRegistroView.muestraErrorEnNumExt(err)
    }

    override fun onNumIntError(err: String) {
        mRegistroView.muestraErrorEnNumInt(err)
    }

    override fun onResidencialError(err: String) {
        mRegistroView.muestraErrorEnResidencia(err)
    }

    override fun onCodigoPostalError(err: String) {
        mRegistroView.muestraErrorEnCodigoPostal(err)
    }

    override fun onListadoError(err: String) {
        mRegistroView.muestraErrorListado(err)
    }


    override fun onCorreoError(err: String) {
        mRegistroView.muestraErrorEnCorreo(err)
    }

    override fun onCorreoConfirmError(err: String) {
        mRegistroView.muestraErrorEnConfirmCorreo(err)
    }

    override fun onPasswdError(err: String) {
        mRegistroView.muestraErrorEnPasswd(err)
    }

    override fun onPasswdConfirmError(err: String) {
        mRegistroView.muestraErrorEnConfirmPasswd(err)
    }

    override fun onTermsCondiError(err: String) {
        mRegistroView.muestraErrorEnTerminosyCondiciones(err)
    }

    override fun onNetworkConnectionFailed() {
        mRegistroView.muestraErrorEnConexion()
    }

    override fun onRequestCodigoPostalSucces(direccionPostal: DireccionPostal) {
        mRegistroView.muestraDatosDeCP(direccionPostal)
    }

    override fun onSingupSuccess(msg: String) {
        mRegistroView.muestraMensajesSuccess(msg)
    }

    override fun onSingupFailed(err: String) {
        mRegistroView.muestraErrorDePeticion(err)
    }

    override fun consultarDireccionPostal(cp: String) {
        mRegistroInteractor.getDireccionPostal(cp, this)
    }

    override fun consultarEstados() {
        mRegistroInteractor.getEstados( this)
    }

    override fun consultarMunicipios(id: String) {
        mRegistroInteractor.getMunicipios(id, this)
    }

    override fun consultarColonias(id: String, id_mum : String) {
        mRegistroInteractor.getColonias(id, id_mum, this)
    }

    override fun consultarResidenciales() {
        mRegistroInteractor.getResidenciales( this)
    }

    override fun onRequestFailed(err: String) {
        mRegistroView.muestrarErrorAlCargar(err)
    }

    override fun onRequestEstadoSucces(estados: ArrayList<Estados>) {
        mRegistroView.muestraDatosEstados(estados)
    }

    override fun onRequestMunicipiosSucces(municipios: ArrayList<Municipios>) {
        mRegistroView.muestraDatosMunicipios(municipios)
    }

    override fun onRequestColoniaSucces(colonias: ArrayList<Colonias>) {
        mRegistroView.muestraDatosColonias(colonias)
    }

    override fun onRequestResidencialSucces(residencial: ArrayList<Residencial>) {
        mRegistroView.muestraDatosResidencial(residencial)
    }

    override fun onRequestPadecimientosSucces(padecimientos: ArrayList<Padecimiento>) {
        mRegistroView.agregarPadecimientosFilterList(padecimientos)
    }



}