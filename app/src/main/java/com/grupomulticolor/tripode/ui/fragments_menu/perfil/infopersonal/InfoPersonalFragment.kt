package com.grupomulticolor.tripode.ui.fragments_menu.perfil.infopersonal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.editinfopersonal.EditInfoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.Fecha
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import kotlinx.android.synthetic.main.fragment_informacion_personal.*

class InfoPersonalFragment : FragmentChildNotFab(), InfoPersonalContract.View {

    private var mPresenter: InfoPersonalContract.Presenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_informacion_personal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        events()

        val interactor = InfoPersonalInteractor(context!!)
        InfoPersonalPresenter(this, interactor)
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun events() {
        btnBack.setOnClickListener { irAtras() }
        fabEdit.setOnClickListener { irAEditarInformacionUsuario() }
    }

    override fun mostrarInformacionUsuario(usuario: Usuario) {

        val arrFecha = usuario.fechaNac?.split("-")
        var fecha = ""
        if (arrFecha != null && arrFecha.size == 3)
            fecha = Fecha.formatFechaEs(arrFecha[2], arrFecha[1], arrFecha[0])

        tvNombre.text = usuario.nombre?.toUpperCase()
        tvApellidoPaterno.text = usuario.apellidoPaterno?.toUpperCase()
        tvApellidoMaterno.text = usuario.apellidoMaterno?.toUpperCase()
        tvFechaNac.text = fecha.toUpperCase()
        tvCorreo.text = usuario.correo
        tvTelefono.text = usuario.phone
        tvPassword.text = "******"
        tvPadecimientos.text = usuario.padecimientos
    }

    override fun mostrarError(msg: String) {
    }

    override fun muestrarErrorConexion() {
        (context as MainActivity).mostrarFragment(SinConexionFragment.instance())
    }

    override fun irAEditarInformacionUsuario() {
        (context as MainActivity).mostrarFragment(EditInfoFragment.instance())
    }

    override fun irAtras() {
        (context!! as MainActivity).irAtras()
    }

    override fun setPresenter(presenter: InfoPersonalContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = InfoPersonalFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}