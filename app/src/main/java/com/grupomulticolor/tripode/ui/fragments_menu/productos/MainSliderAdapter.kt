package com.grupomulticolor.tripode.ui.fragments_menu.productos

import com.grupomulticolor.tripode.data.slider.Sliders
import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class MainSliderAdapter (var imagenes: ArrayList<Sliders> ): SliderAdapter() {

    override fun getItemCount(): Int {
        return imagenes.size
    }

    override fun onBindImageSlide(position: Int, viewHolder: ImageSlideViewHolder?) {
        when(position) {
            position -> viewHolder?.bindImageSlide(imagenes[position].imagen)
        }
    }
}