package com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones

import android.content.Context
import android.graphics.Color
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.producto.ProductoPromocion
import com.grupomulticolor.tripode.util.Currency


class PromocionesAdapter(var productos: ArrayList<ProductoPromocion>, var listenerView: ListenerView) :  RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    lateinit var context: Context
    private lateinit var displayMetrics: DisplayMetrics
    private var dpHeight = 0f
    private var dpWidth = 0f

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        // displayMetrics = parent.context.resources.displayMetrics
        // dpHeight = displayMetrics.heightPixels / displayMetrics.density
        //dpWidth = displayMetrics.widthPixels / displayMetrics.density

        val vw = LayoutInflater.from(context).inflate(R.layout.item_producto, parent, false)
        return ViewHolder(vw)
    }


    override fun getItemCount(): Int {
        return productos.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = productos[position]
        (holder as ViewHolder).bind(item)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nombreProducto : TextView = itemView.findViewById(R.id.nombreProducto)
        var detalleProducto : TextView = itemView.findViewById(R.id.detalleProducto)
        var imgProducto : ImageView = itemView.findViewById(R.id.imgProducto)
        var imgPromocion : LinearLayout = itemView.findViewById(R.id.icono_promociones)

        //  var layoutDescuento: LinearLayout = itemView.findViewById(R.id.itemDescuento)
        // var precioOriginalProducto: TextView = itemView.findViewById(R.id.precioOriginalProducto)
        // var descuentoProducto: TextView = itemView.findViewById(R.id.descuentoProducto)
        var precioProducto: TextView = itemView.findViewById(R.id.precioProducto)
        var imgPrograma : LinearLayout = itemView.findViewById(R.id.icono_programal)

        fun bind(item: ProductoPromocion) {
            itemView.setOnClickListener { listenerView.onClick(item) }
            nombreProducto.text = item.nombre
            detalleProducto.text = item.caregory

            var url = R.drawable.img_nodisponible
            if(item.img != "" && item.img != null && item.img != "null") {
                Picasso
                    .get()
                    .load(item.img)
                    .placeholder(url)
                    .error(url)
                    .fit()
                    .noFade()
                    .into(imgProducto)
            }else{
                Picasso
                    .get()
                    .load(url)
                    .into(imgProducto)
            }

            imgPromocion.visibility = View.VISIBLE
            precioProducto.text = Currency.toCurrency(item.precio!!, "mxn")
            precioProducto.setTextColor(Color.parseColor("#d68500"))


            if (item.HasLoyaltyPlan!!) {
                imgPrograma.visibility = View.VISIBLE
            } else {
                imgPrograma.visibility = View.GONE
            }

        }
    }


    interface ListenerView {
        fun onClick(item: ProductoPromocion)
    }

}



