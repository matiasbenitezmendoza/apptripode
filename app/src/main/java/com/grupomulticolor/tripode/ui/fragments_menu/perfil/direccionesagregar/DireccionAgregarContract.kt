package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.residencial.Residencial

interface DireccionAgregarContract {

    interface View: BaseView<Presenter> {
        fun presentarDatos()
        fun mostrarErrorEnNombre(err: String)
        fun mostrarErrorEnCalle(err: String)
        fun mostrarErrorEnNoExt(err: String)
        fun muestraErrorEnCodigoPostal(error: String)
        fun muestraErrorEnResidencia(error: String)

        fun muestraErrorEnColonia(error: String)
        fun muestraErrorEnCiudad(error: String)
        fun muestraErrorEnConexion()
        fun muestraMensajeDeExito(msg: String)
        fun muestraMensajeDeError(error: String)
        fun muestraDatosDireccionPostal(direccionPostal: DireccionPostal)
        fun muestraDatosEstados(estados: ArrayList<Estados>)
        fun muestraDatosMunicipios(estados: ArrayList<Municipios>)
        fun muestraDatosColonias(colonias: ArrayList<Colonias>)
        fun muestraDatosResidencia(residencial: ArrayList<Residencial>)

    }

    interface Presenter: BasePresenter {
        fun intentaAgregarDireccion(direccion: Direccion)
        //fun consultarDireccionPostal(cp: String)
        fun consultarEstados()
        fun consultarMunicipios(mun: String)
        fun consultarColonias(mun: String,col: String)
        fun consultarResidencias()

    }
}