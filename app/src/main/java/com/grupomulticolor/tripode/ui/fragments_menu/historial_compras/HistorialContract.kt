package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.pedido.Pedido

interface HistorialContract {

    interface View : BaseView<Presenter> {
        fun showPedidos(ordenes: ArrayList<Ordenes>, pedidos: ArrayList<Pedido>)
        fun showErrorNetwork()
    }

    interface Presenter : BasePresenter {
        fun loadOrdenes(pedidos: ArrayList<Pedido>)
        fun loadPedidos()

    }
}