package com.grupomulticolor.tripode.ui.fragments_menu.productos

import android.content.Context
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.carrito.source.CarritoService
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.producto.ProductoCount
import com.grupomulticolor.tripode.data.producto.source.ProductoService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.slider.Sliders
import com.grupomulticolor.tripode.data.slider.source.SliderService
import com.grupomulticolor.tripode.util.Network


class ProductosInteractor(private val context: Context) {

    private val service: ProductoService = ProductoService.instance(context)
    private val serviceSlider: SliderService = SliderService.instance(context)
    private val serviceCarrito: CarritoService = CarritoService.instance(context)



    fun getProductos(search: String, limit: Int, offset: Int, oa: Int, op: Int, callback: Callback) {
        consultarProducto(search, limit, offset, oa, op, callback)
    }

    fun getProductosInicio(search: String, limit: Int, offset: Int, oa: Int, op: Int, callback: Callback) {
        consultarProductoInicio(search, limit, offset, oa, op, callback)
    }

    fun getSlider(callback: Callback) {
        consultarSlider(callback)
    }

    fun getcarrito(id: Int,callback: Callback) {
        consultarCarrito(id, callback)
    }

    private fun consultarSlider(callback: Callback) {

        serviceSlider.get(object : ResponseListener<ResponseData<ArrayList<Sliders>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Sliders>>) {
                callback.onRequestSlidersSucces(response.data as ArrayList<Sliders>)
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    private fun consultarProducto(search: String, limit: Int, offset: Int, oa: Int, op: Int, callback: Callback) {

        service.getProductos("", search, limit, offset, oa, op, object : ResponseListener<ResponseDataTripode<ProductoCount>> {

            override fun onSuccess(response: ResponseDataTripode<ProductoCount>) {
                  var productoCount = response.data as ProductoCount
                  callback.onRequestProductosSucces(productoCount.items!!)
            }

            override fun onError(t: Throwable) {
                  callback.onRequestFailed(t.message!!)
            }

        })
    }

    private fun consultarProductoInicio(search: String, limit: Int, offset: Int, oa: Int, op: Int, callback: Callback) {

        service.getProductosInicio("", search, limit, offset, oa, op, object : ResponseListener<ResponseDataTripode<ProductoCount>> {

            override fun onSuccess(response: ResponseDataTripode<ProductoCount>) {
                var productoCount = response.data as ProductoCount
                callback.onRequestProductosSucces(productoCount.items!!)
            }

            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }

        })
    }

    private fun consultarCarrito(id: Int, callback: Callback) {

        serviceCarrito.getCarrito(id,"",   object : ResponseListener<ResponseDataTripode<ArrayList<Carrito>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<Carrito>>) {
                callback.onRequestCarritoSucces(response.data as ArrayList<Carrito>)
            }

            override fun onError(t: Throwable) {
                callback.onCarritoFailed(t.message!!)
            }

        })
    }


    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }




    /* interface para notificar al presentador cualquier evento */
    interface Callback {

        fun onNetworkConnectionFailed()
        fun onRequestFailed(err: String)
        fun onProductosSuccess(msg: String)
        fun onProductosFailed(err: String)
        fun onCarritoFailed(err: String)
        fun onRequestProductosSucces(pedido: ArrayList<Producto>)
        fun onSlidersSuccess(msg: String)
        fun onRequestSlidersSucces(imagenes: ArrayList<Sliders>)
        fun onCarritoSuccess(msg: String)
        fun onRequestCarritoSucces(carrito: ArrayList<Carrito>)

    }
}
