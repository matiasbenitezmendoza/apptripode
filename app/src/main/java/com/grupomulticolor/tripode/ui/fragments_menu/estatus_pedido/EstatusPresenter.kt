package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido

import android.content.Context
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.pedido.source.PedidoService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import java.lang.Exception

class EstatusPresenter(val mEstatusView: EstatusContract.View, var context: Context) : EstatusContract.Presenter {

    private val service: PedidoService = PedidoService.instance(context)

    init {
        mEstatusView.setPresenter(this)
    }

    override fun start() {
        loadOrdenes(10, 0)
    }

    override fun loadOrdenes(limit: Int, offset: Int) {

        service.get(limit, offset, object : ResponseListener<ResponseData<ArrayList<Pedido>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Pedido>>) {
                try {

                    var ordenes = response.data as ArrayList<Pedido>
                    mEstatusView.showPedidos(ordenes)

                } catch (e: Exception) {
                    e.printStackTrace()
                    mEstatusView.showErrorNetwork()
                }
            }

            override fun onError(t: Throwable) {
                mEstatusView.showErrorNetwork()
            }
        })
        /*val productos = ArrayList<Producto>()
        productos.clear()
        productos.add(
            Producto(
                "Motrin Pediátrico",
                "15 ML Suspensión Frasco Ibuprofeno 40 MG",
                "http://tripode.webandando.com/admin/images/logo.png", "Bayern", "1234567",
                68.00, 2.0
            )
        )

        productos.add(
            Producto(
                "Bioyetyin Intección 6 piezas jeringa",
                "Eritropoyetina humana recombin 2000 UI",
                "http://tripode.webandando.com/admin/images/logo.png", "Bayern", "1234567",
                1507.00, 10.0
            )
        )*/

       // val ordenes = ArrayList<Pedido>()
       /*
        ordenes.add(
            Pedido("000013", productos, "2019-03-06 11:12:12", 1)
        )*/

       // mEstatusView.showPedidos(ordenes)

    }


}