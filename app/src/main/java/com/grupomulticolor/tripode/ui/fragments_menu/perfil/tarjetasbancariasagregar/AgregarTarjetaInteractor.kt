package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar

import android.content.Context
import android.text.TextUtils
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.data.tarjeta.source.TarjetasService
import com.grupomulticolor.tripode.util.Network
import com.grupomulticolor.tripode.util.config.Constants
import mx.openpay.android.Openpay
import mx.openpay.android.OperationCallBack
import mx.openpay.android.model.Card
import mx.openpay.android.model.Token
import mx.openpay.android.validation.CardValidator
import java.util.regex.Pattern

class AgregarTarjetaInteractor(var context: Context, var openpay: Openpay) {

    val service: TarjetasService = TarjetasService.instance(context)


    fun agregaTarjetaBancaria(tarjetaBancaria: TarjetaBancaria, callback: Callback) {
        if (!isValidNombre(tarjetaBancaria.nombreCuentaHabiente, callback)) return
        if (!isValidCardNumber(tarjetaBancaria.noTarjeta, callback)) return
        if (!isValidFechaVen(tarjetaBancaria.mesVen, tarjetaBancaria.anioVen, callback)) return
        if (!isValidCVV(tarjetaBancaria.cvv, tarjetaBancaria.noTarjeta, callback)) return

        crearToken(tarjetaBancaria, callback)
    }

    fun guardarTarjeta(tarjetaBancaria: TarjetaBancaria, callback: Callback) {
        if (isNetworkAvalible(callback)) {
            service.post(tarjetaBancaria, object : ResponseListener<ResponseData<TarjetaBancaria>> {
                override fun onSuccess(response: ResponseData<TarjetaBancaria>) {
                    callback.onSaveSucces(R.string.mensaje_tarjeta_agregada) {
                        callback.goBack()
                    }
                }

                override fun onError(t: Throwable) {
                    callback.onSaveFailed(t.message ?: context.getString(R.string.error_unknown))
                }

            })
        }
    }

    private fun crearToken(tarjetaBancaria: TarjetaBancaria, callback: Callback) {
        if (isNetworkAvalible(callback)) {
            callback.onShowProgress()
            val card = Card()
            card.holderName(tarjetaBancaria.nombreCuentaHabiente)
            card.cardNumber(tarjetaBancaria.noTarjeta)
            card.expirationMonth(tarjetaBancaria.mesVen)
            card.expirationYear(tarjetaBancaria.anioVen)
            card.cvv2(tarjetaBancaria.cvv)
            openpay.createToken(card, callback)
        }

    }

    /* verifica que el nombre ingresado sea correcto*/
    private fun isValidNombre(nombre: String, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(nombre)) {
            callback.onNombreTarjetaError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(nombre).matches()) {
            callback.onNombreTarjetaError(context.getString(R.string.error_match))
            return false
        }

        if (!CardValidator.validateHolderName(nombre)) {
            callback.onNombreTarjetaError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidCVV(cvv: String, cardNumber: String, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(cvv)) {
            callback.onCVVError(context.getString(R.string.error_item_empty))
            return false
        }

        if (!CardValidator.validateCVV(cvv, cardNumber)) {
            callback.onCVVError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    private fun isValidCardNumber(cardNumber: String, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(cardNumber)) {
            callback.onNumTarjetaError(context.getString(R.string.error_item_empty))
            return false
        }

        if (!CardValidator.validateNumber(cardNumber)) {
            callback.onNumTarjetaError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    private fun isValidFechaVen(monthVen: Int, anioVen: Int, callback: Callback) : Boolean {

        if (!CardValidator.validateExpiryDate(monthVen, anioVen)) {
            callback.onFechaDeVenError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el numero de tarjeta sea valido*/

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }


    interface Callback : OperationCallBack<Token> {
        fun onNombreTarjetaError(err: String)
        fun onNumTarjetaError(err: String)
        fun onFechaDeVenError(err: String)
        fun onCVVError(err: String)
        fun onNetworkConnectionFailed()
        fun onSaveSucces(resource: Int, resolve: () -> Unit)
        fun onSaveFailed(err: String)
        fun onShowProgress()
        fun onHideProgress()
        fun goBack()
    }
}