package com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.producto.ProductoPromocion

interface PromocionesContract {

    interface View : BaseView<Presenter> {
        fun muestraErrorEnConexion()
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)
        fun agregarProductosFilterList(ProductoPromocion: ArrayList<ProductoPromocion>, total: Int)
    }

    interface Presenter : BasePresenter {
        fun getproductosPromocion(idPromocion: Int)
        fun getpromociones()

    }

}