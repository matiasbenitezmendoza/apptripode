package com.grupomulticolor.tripode.ui.fragments_menu.perfil.editinfopersonal

import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.usuario.Usuario
import java.util.ArrayList

class EditInfoPresenter(val mEditInfoView: EditInfoContract.View,
                        val mEditInfoInteractor: EditInfoInteractor)
    : EditInfoContract.Presenter, EditInfoInteractor.Callback {

    init {
        mEditInfoView.setPresenter(this)
    }

    override fun start() {
        mEditInfoInteractor.getPadecimientos(this)
        mEditInfoInteractor.presentarDatosdeUsuario(this)
    }

    override fun intentaActulizarDatosDeUsuario(usuario: Usuario) {
        mEditInfoInteractor.actualizarDatosUsuario(usuario, this)
    }

    override fun onNombreError(err: String) {
        mEditInfoView.mostrarErrorEnNombre(err)
    }

    override fun onApellidoPaternoError(err: String) {
        mEditInfoView.mostrarErrorEnApellidoPaterno(err)
    }


    override fun onPasswdError(err: String) {
        mEditInfoView.muestraErrorEnPasswd(err)
    }

    override fun onApellidoMaternoError(err: String) {
        mEditInfoView.mostrarErrorEnApellidoMaterno(err)
    }

    override fun onFechaNacError(err: String) {
        mEditInfoView.mostrarErrorEnFechaDeNacimiento(err)
    }

    override fun onCorreoError(err: String) {
        mEditInfoView.mostrarErrorEnCorreoElectronico(err)
    }

    override fun onPhoneError(err: String) {
        mEditInfoView.mostrarErrorEnTelefono(err)
    }

    override fun onNetworkConnectionFailed() {
        mEditInfoView.mostrarErrorEnConexion()
    }

    override fun onUpdateSuccess(msg: String) {
        mEditInfoView.muestraMensajeSucces(msg)
    }

    override fun onUpdateFailed(err: String) {
        mEditInfoView.muestrarMensajeError(err)
    }

    override fun presentarUsuarioInfo(usuario: Usuario) {
        mEditInfoView.presentarDatos(usuario)
    }

    override fun onRequestFailed(err: String) {
        mEditInfoView.muestrarMensajeError(err)
    }

    override fun onRequestPadecimientosSucces(padecimientos: ArrayList<Padecimiento>) {
        mEditInfoView.agregarPadecimientosFilterList(padecimientos)
    }

}