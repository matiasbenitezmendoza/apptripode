package com.grupomulticolor.tripode.ui.recover

class RecoverPresenter(val mRecoverView: RecoverContract.View,
                       val mRecoverInteractor: RecoverInteractor)
    : RecoverContract.Presenter, RecoverInteractor.Callback {

    init {
        mRecoverView.setPresenter(this)
    }

    override fun start() {
        //
    }

    override fun intentaEnviarPeticion(correo: String) {
        mRecoverInteractor.recuperarPassword(correo, this)
    }


    override fun onEmailError(msg: String) {
        mRecoverView.mostrarErrorEnCorreo(msg)
    }

    override fun onNetworkConnectionFailed() {
        mRecoverView.errorEnConexion()
    }

    override fun onRequestSuccess(msg: String) {
        mRecoverView.mostrarMensajeSucces(msg)
    }

    override fun onRequestError(msg: String) {
        mRecoverView.mostrarMensajeDeError(msg)
    }

}