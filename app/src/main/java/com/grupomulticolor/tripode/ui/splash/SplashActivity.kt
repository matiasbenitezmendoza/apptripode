package com.grupomulticolor.tripode.ui.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.ui.login.LoginActivity
import java.util.*

class SplashActivity : AppCompatActivity() {

    var id_pedido = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        /*Log.d("TAG","BUNDLEEE SPLASHHH")
        try {
            val bundle = intent.extras
            if(bundle != null){
                var calificacion: String? = bundle.get("id_pedido").toString()
                Log.d("TAG","BUNDLEEE"+ calificacion)

                if(calificacion != null){
                        if(calificacion.isNotEmpty()){
                            id_pedido = calificacion
                        }
                    }
            }else{
                    id_pedido = ""
                    Log.d("TAG","BUNDLEEE SIN DATOS")
            }
        } catch (e: Exception) {
            id_pedido = ""
        }*/
        time()
    }

    private fun time() {
        val task = object : TimerTask() {
            override fun run() {

                if(SessionPrefs(this@SplashActivity).isLogin()) {
                    val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                   /* if(id_pedido != ""){
                        mainIntent.putExtra("id_pedido", id_pedido)
                    }else{
                        mainIntent.putExtra("id_pedido", "0")
                    }*/
                    startActivity(mainIntent)
                    finish()
                } else {
                    val mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                    startActivity(mainIntent)
                    finish()
                }

            }
        }

        val timer = Timer()
        timer.schedule(task, 3000)
    }
}