package com.grupomulticolor.tripode.ui.fragments_menu.perfil.editinfopersonal

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.util.Fecha
import com.grupomulticolor.tripode.util.Input
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_informacion_personal_edit.*

class EditInfoFragment : FragmentChildNotFab(), EditInfoContract.View {

    private lateinit var cntx: Context
    private var mPresenter: EditInfoContract.Presenter? = null
    private val mPadecimientos: ArrayList<Padecimiento> = ArrayList()
    private lateinit var vw: View
    private var usuario: Usuario? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_informacion_personal_edit, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cntx = context!!
        usuario = SessionPrefs(cntx).currentUsu()

        val interactor = EditInfoInteractor(cntx)
        EditInfoPresenter(this, interactor)
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initEvents() {

        inputNombre.requestFocus()
        agregaTextListenerAInput(inputNombre)
        agregaTextListenerAInput(inputApellidoPaterno)
        agregaTextListenerAInput(inputApellidoMaterno)
        agregaTextListenerAInput(inputFechNac)
        agregaTextListenerAInput(inputCorreo)
        agregaTextListenerAInput(inputPhone)
        agregaTextListenerAInput(inputPassword)


        inputFechNac.inputType = InputType.TYPE_CLASS_NUMBER
        inputFechNac.keyListener = DigitsKeyListener.getInstance("1234567890+-()/")
        val birthday = MaskedTextChangedListener("[00]/[00]/[0000]", inputFechNac)
        inputFechNac.addTextChangedListener(birthday)
        inputFechNac.onFocusChangeListener = birthday

        btnBack.setOnClickListener {  (context!! as MainActivity).irAtras() }
        btnGuardar.setOnClickListener { actualizaDatos() }
    }

    private fun agregaTextListenerAInput(editText: EditText) {
        editText.addTextChangedListener( object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Input.onClear(cntx, editText)
            }
        })
    }

    private fun actualizaDatos() {
        val user = Usuario()
        user.nombre = inputNombre.text.toString()
        user.apellidoPaterno = inputApellidoPaterno.text.toString()
        user.apellidoMaterno = inputApellidoMaterno.text.toString()
        user.fechaNac = inputFechNac.text.toString()
        user.correo = inputCorreo.text.toString()
        user.phone = inputPhone.text.toString()
        user.padecimientos = getCadenaPadecimientos()
        user.password = inputPassword.text.toString()

        mPresenter?.intentaActulizarDatosDeUsuario(user)
    }

    override fun presentarDatos(usuario: Usuario) {
        inputNombre.setText(usuario.nombre)
        inputApellidoPaterno.setText(usuario.apellidoPaterno)
        inputApellidoMaterno.setText(usuario.apellidoMaterno)
        inputFechNac.setText(Fecha.formatDBFechtoEs(usuario.fechaNac ?: ""))
        inputCorreo.setText(usuario.correo)
        inputPhone.setText(usuario.phone)
    }

    override fun mostrarErrorEnNombre(err: String) {
        Input.onError(cntx, inputNombre, err)
    }

    override fun mostrarErrorEnApellidoPaterno(err: String) {
        Input.onError(cntx, inputApellidoPaterno, err)
    }

    override fun mostrarErrorEnApellidoMaterno(err: String) {
        Input.onError(cntx, inputApellidoMaterno, err)
    }

    override fun mostrarErrorEnFechaDeNacimiento(err: String) {
        Input.onError(cntx, inputFechNac, err)
    }

    override fun mostrarErrorEnCorreoElectronico(err: String) {
        Input.onError(cntx, inputCorreo, err)
    }


    override fun muestraErrorEnPasswd(error: String) {
        Input.onError(cntx, inputPassword, error)
    }

    override fun mostrarErrorEnTelefono(err: String) {
        Input.onError(cntx, inputPhone, err)
    }

    override fun setPresenter(presenter: EditInfoContract.Presenter) {
        mPresenter = presenter
    }

    override fun mostrarErrorEnConexion() {
        (context as MainActivity).mostrarFragment(SinConexionFragment.instance())
    }

    override fun muestraMensajeSucces(msg: String) {
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage(msg)
            .setButtonPositive("De acuerdo", object : WADialog.OnClickListener {
                override fun onClick() {
                    (context!! as MainActivity).irAtras()
                }
            }).build()
        dialog.show()
    }

    override fun muestrarMensajeError(err: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun agregarPadecimientosFilterList(padecimientos: ArrayList<Padecimiento>) {
        mPadecimientos.clear()
        mPadecimientos.addAll(padecimientos)

        for (tema in mPadecimientos) {
            chipsInput.addChip(tema.nombre)
        }

    }

    private fun getCadenaPadecimientos() : String {
        var cadenaPadecimients = ""
        val temasSelect = chipsInput.chipsSelected()
        for (i in 0 until temasSelect.size) {
            cadenaPadecimients += if (i+1  < temasSelect.size) {
                "${temasSelect[i]},"
            } else
                temasSelect[i]
        }
        Log.d("TAG", cadenaPadecimients)
        return cadenaPadecimients
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = EditInfoFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}