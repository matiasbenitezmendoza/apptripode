package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direcciones

import android.content.Context
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.direcciones.source.DireccionService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.Network
import java.lang.Exception

class DireccionesPresenter(val mView: DireccionesMainContract.View,
                           var context: Context)
    : DireccionesMainContract.Presenter  {

    private val direccionService = DireccionService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        loadDirecciones()
    }

    override fun loadDirecciones() {

        if (isNetworkAvalible()) {
            direccionService.getDireccionesUsuario(object : ResponseListener<ResponseData<ArrayList<Direccion>>> {
                override fun onSuccess(response: ResponseData<ArrayList<Direccion>>) {
                    try {
                        mView.showDirecciones(response.data as ArrayList<Direccion>)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        // mView.showMessageError(e.message)
                        mView.showEmpty()
                    }
                }

                override fun onError(t: Throwable) {
                    // mView.showMessageError(t.message)
                    mView.showEmpty()
                }
            })
        }
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mView.showErrorNetwork()
        return isAvailable
    }

    override fun eliminarDireccion(itemId: Int) {
        if (isNetworkAvalible()) {
            direccionService.deleteDireccion(itemId, object : ResponseListener<ResponseData<Any>> {

                override fun onSuccess(response: ResponseData<Any>) {
                    loadDirecciones()
                }

                override fun onError(t: Throwable) {
                    mView.showMessageError(t.message)
                }
            })
        }
    }



}