package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus

import android.content.Context
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.data.pedido.source.PedidoService
import com.grupomulticolor.tripode.data.repartidor.Repartidor
import com.grupomulticolor.tripode.data.repartidor.source.RepartidorService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import java.lang.Exception

class DetalleEstatusPresenter(var mContractView: DetalleEstatusContract.View, var pedido: Pedido, var context: Context) : DetalleEstatusContract.Presenter {

    private val service: RepartidorService = RepartidorService.instance(context)
    private val servicePedido: PedidoService = PedidoService.instance(context)


    init {
        mContractView.setPresenter(this)

       /*
        val productos = ArrayList<Producto>()
        productos.clear()
        productos.add(
            Producto(
                "Motrin Pediátrico",
                "15 ML Suspensión Frasco Ibuprofeno 40 MG",
                "http://tripode.webandando.com/admin/images/logo.png", "Bayern", "1234567",
                68.00, 2.0
            )
        )

        productos.add(
            Producto(
                "Bioyetyin Intección 6 piezas jeringa",
                "Eritropoyetina humana recombin 2000 UI",
                "http://tripode.webandando.com/admin/images/logo.png", "Bayern", "1234567",
                1507.00, 10.0
            )
        )

        pedido =  Pedido(1, 63, -1, "","",1,"","",
            "","",2,1,"","","", "",
            "","","","","","","","","","",
            "","","","",1)*/

    }

    override fun cargarPedido() {
         mContractView.showDetalleEstatus(pedido.status_pedido!!, pedido.orden!!)
    }

    override fun RequestRepartidor(id: Int) {
        if(id != -1){
                    service.get(id, object : ResponseListener<ResponseData<Repartidor>> {
                        override fun onSuccess(r: ResponseData<Repartidor>) {
                            try {
                                mContractView.showInfoRepartidor(r.data as Repartidor)

                            } catch (e: Exception) {
                                e.printStackTrace()
                                mContractView.showInfoSinRepartidor()
                            }
                        }
                        override fun onError(t: Throwable) {
                            mContractView.showInfoSinRepartidor()
                        }
                    })
        }else{
            mContractView.showInfoSinRepartidor()
        }

    }


    override fun RequestPedido() {
        servicePedido.getPedido(pedido.id!!, object : ResponseListener<ResponseData<Pedido>> {
                override fun onSuccess(r: ResponseData<Pedido>) {
                    try {
                        pedido = r.data as Pedido
                        mContractView.showDetalleEstatus(pedido.status_pedido!!, pedido.orden!!)
                        RequestRepartidor(pedido.id_repartidor!!)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                override fun onError(t: Throwable) {

                }
            })
    }


    override fun start() {
        RequestPedido()
       // RequestRepartidor(pedido.id_repartidor!!)
    }
}