package com.grupomulticolor.tripode.ui.recover

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.ui.registro.RegistroActivity
import com.grupomulticolor.tripode.util.lib.TextDrawable
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_recover_pass.*

class RecoverFragment : Fragment(), RecoverContract.View {
    private var mPresenter: RecoverContract.Presenter? = null
    private lateinit var cntx: Context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recover_pass, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cntx = context!!
        setEvents()
        mPresenter?.start()
    }

    private fun setEvents() {
        btnBack.setOnClickListener { irALogin() }
        btnBack.paintFlags = btnBack.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        btnRegistrate.paintFlags = btnRegistrate.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        btnRegistrate.setOnClickListener { irAregistro() }
        btnEnviar.setOnClickListener { mPresenter?.intentaEnviarPeticion(inputCorreo.text.toString()) }
    }

    override fun errorEnConexion() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(R.string.msg_sin_conexion)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun mostrarErrorEnCorreo(msg: String) {
        showInputError(inputCorreo, msg)
    }

    override fun mostrarMensajeDeError(msg: String) {
        tvInfo.text = msg
        tvInfo.setTextColor(ContextCompat.getColor(cntx, R.color.scarlet))
        ivInfo.setImageDrawable(ContextCompat.getDrawable(cntx, R.drawable.ico_vector_error))
        ivInfo.visibility = View.VISIBLE

        btnRegistrate.visibility = View.VISIBLE
        btnEnviar.visibility = View.VISIBLE
        btnBack.visibility = View.GONE
    }

    override fun mostrarMensajeSucces(msg: String) {
        tvInfo.text = msg
        tvInfo.setTextColor(ContextCompat.getColor(cntx, R.color.mid_green))
        ivInfo.setImageDrawable(ContextCompat.getDrawable(cntx, R.drawable.ico_vector_check))
        ivInfo.visibility = View.VISIBLE

        btnBack.visibility = View.VISIBLE
        btnEnviar.visibility = View.GONE
        btnRegistrate.visibility = View.GONE
    }

    override fun irALogin() {
        (activity as RecoverActivity).onBackPressed()
    }

    override fun irAregistro() {
        val intent = Intent(activity, RegistroActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun setPresenter(presenter: RecoverContract.Presenter) {
        this.mPresenter = presenter
    }

    private fun showInputError(editText: EditText, error: String) {
        editText.background = ContextCompat.getDrawable(cntx, R.drawable.input_white_error)
        editText.setCompoundDrawables(null, null, TextDrawable(cntx, error), null)
    }

    companion object {
        fun getInstance() : Fragment {
            return RecoverFragment()
        }
    }
}