package com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.util.Currency

class DetalleProductoAdapter( var Presenter: DetalleContract.Presenter,var mcarrito: ArrayList<Carrito>)  : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var producto: Producto? = null
    var productosRelacionados = ArrayList<Producto>()
    var c: Int = 1
    companion object {
        const val VERTICAL = 1
        const val HORIZONTAL = 2
    }

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vh : RecyclerView.ViewHolder
        val vw: View
        when(viewType) {
            VERTICAL -> {
                vw = inflate.inflate(R.layout.item_detalle_producto, parent, false)
                vh = ViewHolderProducto(vw)
            }
            HORIZONTAL -> {
                vw = inflate.inflate(R.layout.item_productos_relacionados, parent, false)
                vh = ViewHolderProductosRelacionados(vw)
            }
            else -> {
                vw = inflate.inflate(R.layout.item_empty, parent, false)
                vh = ViewHolderEmpty(vw)
            }
        }
        return vh
    }

    override fun getItemCount(): Int {
        if (producto == null) return 0
        return 2 // detalle producto + productos relacionados
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            VERTICAL -> (holder as ViewHolderProducto).bind(producto!!)
            HORIZONTAL -> (holder as ViewHolderProductosRelacionados).bind(productosRelacionados)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> VERTICAL
            1 -> HORIZONTAL
            else -> -1
        }
    }

    fun existe_en_carrito(itemNum: String): Boolean{
        for (l in mcarrito) {
            if(l.itemId == itemNum){
                return true
            }
        }
        return false
    }

    inner class ViewHolderProducto(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val btnBack: ImageButton = itemView.findViewById(R.id.btnBack)
        val tvNombreProducto: TextView = itemView.findViewById(R.id.tvNombreProducto)
        val tvDetalleProducto: TextView = itemView.findViewById(R.id.tvDetalleProducto)
        val tvNumProducto: TextView = itemView.findViewById(R.id.tvNumProducto)
        val imageProducto: ImageView = itemView.findViewById(R.id.imgProducto)
        val tvPrecioProducto: TextView = itemView.findViewById(R.id.tvPrecioProducto)
        val btnAgregarAlCarrito: Button = itemView.findViewById(R.id.btnAgregarAlCarrito)
        val btnmas: Button = itemView.findViewById(R.id.btnsumarcarrito)
        val btnmenos: Button = itemView.findViewById(R.id.btnrestarrcarrito)
        val cantidad: Button = itemView.findViewById(R.id.valorcantidad)
        var promociones: LinearLayout = itemView.findViewById(R.id.content_promociones)
        val namePromociones: TextView = itemView.findViewById(R.id.textopromociones)
        var programale: LinearLayout = itemView.findViewById(R.id.content_programa_lealtad)

        //content_programa_lealtad


        fun bind(producto: Producto) {
            tvNombreProducto.text = producto.nombre
            tvDetalleProducto.text = producto.caregory
            tvNumProducto.text = "item: "+producto.itemNum
            cantidad.text = c.toString()

            var url = R.drawable.img_nodisponible
            if(producto.img != "" && producto.img != null && producto.img != "null") {
                Picasso
                    .get()
                    .load(producto.img)
                    .into(imageProducto)
            }else{
                Picasso
                    .get()
                    .load(url)
                    .into(imageProducto)
            }


            tvPrecioProducto.text = Currency.toCurrency(producto.precio!!, "MXN")

            btnAgregarAlCarrito.setOnClickListener {
                val id_carrito = SessionPrefs.getInstance(context).getIdSC()
                if( existe_en_carrito(producto.itemNum!!) ){
                    Presenter.editarCarrito(id_carrito, producto.itemNum!!, c)
                }
                else{
                    Presenter.agregarAlCarrito(id_carrito, producto.itemNum!!, c)
                }
            }

            btnBack.setOnClickListener {
                (context as MainActivity).irAtras()
            }

            btnmas.setOnClickListener {
                if(c < 99){
                    c++
                    cantidad.text = c.toString()
                }
            }

            btnmenos.setOnClickListener {
                if(c > 1){
                    c--
                    cantidad.text = c.toString()
                }

            }

            if (producto.PromotionName != null && producto.PromotionName != "" ) {
                promociones.visibility = View.VISIBLE
                namePromociones.text = "Este artículo pertenece a la promoción: " + producto.PromotionName
                tvPrecioProducto.setTextColor(Color.parseColor("#d68500"))

            } else {
                promociones.visibility = View.GONE
            }

            if (producto.HasLoyaltyPlan!!) {
                programale.visibility = View.VISIBLE
            } else {
                programale.visibility = View.GONE
            }

        }
    }

    inner class ViewHolderProductosRelacionados(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rvProductos : RecyclerView = itemView.findViewById(R.id.rvProductosRelacionados)

        fun bind(productos: ArrayList<Producto>) {


            val mAdapter = ProductosRelacionadosAdapter(productos, object : ProductosRelacionadosAdapter.ListenerView {
                override fun onClick(item: Producto) {
                    //Toast.makeText(context, item.nombre, Toast.LENGTH_LONG).show()
                    DetalleProductoFragment.destroy()

                    DetalleProductoPresenter(
                        DetalleProductoFragment.instance() as DetalleContract.View, item, context!!)
                    fragmentTransaction(DetalleProductoFragment.instance())
                }
            })
            rvProductos.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvProductos.adapter = mAdapter
        }
    }

    inner class ViewHolderEmpty(itemView: View) : RecyclerView.ViewHolder(itemView)

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)

    }


}