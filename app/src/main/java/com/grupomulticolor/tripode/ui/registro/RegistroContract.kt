package com.grupomulticolor.tripode.ui.registro

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.direcciones.Colonias
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.direcciones.DireccionPostal
import com.grupomulticolor.tripode.data.direcciones.Estados
import com.grupomulticolor.tripode.data.direcciones.Municipios
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.usuario.Usuario

interface RegistroContract {

    interface View : BaseView<Presenter> {
        fun muestraProgress(show: Boolean)
        fun muestraErrorEnNombre(error: String)
        fun muestraErrorEnApellidoPaterno(error: String)
        fun muestraErrorEnApellidoMaterno(error: String)
        fun muestraErrorEnFechaNac(error: String)
        fun muestraErrorEnCalle(error: String)
        fun muestraErrorEnNumExt(error: String)
        fun muestraErrorEnNumInt(error: String)
        fun muestraErrorEnResidencia(error: String)
        fun muestraErrorEnCodigoPostal(error: String)
        fun muestraErrorEnCorreo(error: String)
        fun muestraErrorEnConfirmCorreo(error: String)
        fun muestraErrorEnPasswd(error: String)
        fun muestraErrorEnConfirmPasswd(error: String)
        fun muestraErrorEnTerminosyCondiciones(error: String)
        fun muestraErrorEnConexion()
        fun irAIniciarSesion() // iniciar sesion o tutorial u otra cosa
        fun muestraErrorDePeticion(error: String)
        fun muestraTerminosyCondiciones()
        fun muestrarErrorAlCargar(err: String)
        fun muestraDatosDeCP(direccionPostal: DireccionPostal)
        fun muestraMensajesSuccess(mensaje: String)
        fun agregarPadecimientosFilterList(padecimientos: ArrayList<Padecimiento>)

        fun muestraDatosEstados(estados: ArrayList<Estados>)
        fun muestraDatosMunicipios(estados: ArrayList<Municipios>)
        fun muestraDatosColonias(colonias: ArrayList<Colonias>)
        fun muestraDatosResidencial(residencial: ArrayList<Residencial>)

        fun muestraErrorListado(error: String)

    }

    interface Presenter : BasePresenter {
        fun intentaRegistrarUsuario(user: Usuario)
        fun consultarDireccionPostal(cp : String)
        fun verTerminosYCondiciones()
        fun consultarEstados()
        fun consultarMunicipios(mun: String)
        fun consultarColonias(mun: String,col: String)
        fun consultarResidenciales()

    }


}