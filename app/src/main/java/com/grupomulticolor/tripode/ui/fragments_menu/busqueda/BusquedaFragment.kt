package com.grupomulticolor.tripode.ui.fragments_menu.busqueda

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleContract
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoPresenter
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab
import kotlinx.android.synthetic.main.fragment_busqueda.*
import kotlinx.android.synthetic.main.fragment_inicio.mRecyclerView

class BusquedaFragment : FragmentParentNotFab(), BusquedaContract.View {

    private lateinit var mAdapter: BusquedaAdapter
    private var mproductos = ArrayList<Producto>()
    private lateinit var vw: View
    private var mPresenter: BusquedaContract.Presenter? = null
   // private var mproductos: ArrayList<Producto>? = null

    private var busqueda = "pastillas"
    private var limit = 20
    private var offset = 1
    private var orden_alfabetico = 2
    private var orden_precio = 0
    private var aptoParaCargar = true
    private var detalle = false
    private var total = 0

    private var btn1 = 1
    private var btn2 = 0
    private var btn3 = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_busqueda, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        BusquedaPresenter(this, context!!)
        ViewCompat.setNestedScrollingEnabled(vw,false)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        if(!detalle){
            mPresenter?.getproductos(busqueda,limit, offset,orden_alfabetico,orden_precio)
        }
    }

    private fun initAdapter() {

        if(!detalle){
              mproductos.clear()
              limit = 20
              offset = 1
              orden_alfabetico = 2
              orden_precio = 0
              aptoParaCargar = true
              btn1 = 1
              btn2 = 0
              btn3 = 0
              resultado.text = "Buscando producto ..."

        }else{
            resultado.text = "Resultado de la búsqueda “" + busqueda + "”: " + total + " Productos"
        }

        cambiarIcoAlfabeticamente()
        cambiarIcoPrecio()

        mAdapter = BusquedaAdapter(mproductos!!,  object : BusquedaAdapter.ListenerView {
            override fun onClick(item: Producto) {
                goDetalleProducto(item)
            }
        })

        mRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(context)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)

        mRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1
                    if (aptoParaCargar) {
                        if (  pastVisibleItem >= totalItemCount) {
                            aptoParaCargar = false
                            offset += 1
                            mPresenter?.getproductos(busqueda,limit, offset,orden_alfabetico,orden_precio)
                        }
                    }
                }

            }


        })


        btn_alfabetico.setOnClickListener {
                                                btn1++
                                                onClickOrdenarAlfabeticamente()
                                          }
        btn_precio.setOnClickListener {
                                                btn2++
                                                onClickOrdenarPrecio()
                                      }


    }

    private fun goDetalleProducto(producto: Producto) {
        setDetalle(true)
        DetalleProductoPresenter(
            DetalleProductoFragment.instance() as DetalleContract.View, producto, context!!)
        fragmentTransaction(DetalleProductoFragment.instance())
    }

    override fun muestraErrorEnConexion() {
        Snackbar.make(vw, "Sin conexion a internet", Snackbar.LENGTH_LONG).show()
    }

    override fun muestraErrorDePeticion(error: String) {
       if(mproductos.isEmpty()) {
          /* mRecyclerView.visibility = View.GONE
           actionInfo.visibility = View.GONE
           mensaje.visibility = View.VISIBLE
           resultado.text = "Tu búsqueda “" + busqueda + "” no arrojo resultados. Realiza otra búsqueda con criterios diferentes."*/
           BusquedaVaciaFragment.destroy()
           val fragment = BusquedaVaciaFragment.instance() as BusquedaVaciaFragment
           fragment.setBusqueda(busqueda)
           this.fragmentTransaction(fragment)
        }
    }

    override fun muestrarErrorAlCargar(err: String) {
        if(mproductos.isEmpty()) {
            BusquedaVaciaFragment.destroy()
            val fragment = BusquedaVaciaFragment.instance() as BusquedaVaciaFragment
            fragment.setBusqueda(busqueda)
            this.fragmentTransaction(fragment)

           /* mRecyclerView.visibility = View.GONE
            actionInfo.visibility = View.GONE
            mensaje.visibility = View.VISIBLE
            resultado.text = "Tu búsqueda “" + busqueda + "” no arrojo resultados. Realiza otra búsqueda con criterios diferentes."*/
        }

    }

    override fun agregarProductosFilterList(productos: ArrayList<Producto>, t: Int) {
        this.total = t
        if(mproductos.isEmpty()) {
            mensaje.visibility = View.GONE
            actionInfo.visibility = View.VISIBLE
            mRecyclerView.visibility = View.VISIBLE
            resultado.text = "Resultado de la búsqueda “" + busqueda + "”: " + total + " Productos"
        }

        aptoParaCargar = true
        mproductos.addAll(productos)
        mAdapter.notifyDataSetChanged()
    }

    override fun setPresenter(presenter: BusquedaContract.Presenter) {
        mPresenter = presenter
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    fun setBusqueda(c : String){
        this.busqueda = c
    }

    fun setDetalle(b : Boolean){
        this.detalle = b
    }

    fun cambiarIcoAlfabeticamente() {
        if(btn1 > 2){
            btn1 = 0
        }
        var url = R.drawable.az_off
        if(btn1 == 1){
            url = R.drawable.az_on
        }
        if(btn1 == 2){
            url = R.drawable.az_on_inv
        }

        Picasso
            .get()
            .load(url)
            .into(btn_alfabetico)
    }

    fun onClickOrdenarAlfabeticamente() {
        if(btn1 > 2){
            btn1 = 0
        }
        var url = R.drawable.az_off
        orden_alfabetico = 0

        if(btn1 == 1){
            url = R.drawable.az_on
            orden_alfabetico = 2
        }
        if(btn1 == 2){
            url = R.drawable.az_on_inv
            orden_alfabetico = 1
        }

        Picasso
            .get()
            .load(url)
            .into(btn_alfabetico)

        mproductos.clear()
        limit = 20
        offset = 1
        aptoParaCargar = true
        mPresenter?.getproductos(busqueda,limit, offset,orden_alfabetico,orden_precio)
    }

    fun cambiarIcoPrecio() {
        if(btn2 > 2){
            btn2 = 0
        }
        var url = R.drawable.price_off
        if(btn2 == 1){
            url = R.drawable.precio_on
        }
        if(btn2 == 2){
            url = R.drawable.precio_inv_on
        }

        Picasso
            .get()
            .load(url)
            .into(btn_precio)

    }

    fun onClickOrdenarPrecio() {
        if(btn2 > 2){
            btn2 = 0
        }
        var url = R.drawable.price_off
        orden_precio = 0
        if(btn2 == 1){
            url = R.drawable.precio_on
            orden_precio = 2
        }
        if(btn2 == 2){
            url = R.drawable.precio_inv_on
            orden_precio = 1
        }

        Picasso
            .get()
            .load(url)
            .into(btn_precio)

        mproductos.clear()
        limit = 20
        offset = 1
        aptoParaCargar = true
        mPresenter?.getproductos(busqueda,limit, offset,orden_alfabetico,orden_precio)
    }


    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = BusquedaFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}