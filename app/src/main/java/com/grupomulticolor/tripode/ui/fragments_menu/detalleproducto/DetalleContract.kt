package com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.producto.Producto

interface DetalleContract {

    interface View : BaseView<Presenter> {
        fun showProducto(producto: Producto)
        fun showError(error: String)
        fun showNotificacionError()
        fun showCarritoVacio()
        fun showProductosRelacionados(productos: ArrayList<Producto>)
        fun showAddProducto()
        fun showNotificacion(carrito: ArrayList<Carrito>)
        fun setCarrito(carrito: ArrayList<Carrito>)


    }

    interface Presenter : BasePresenter {
        fun requestProductosRelacionados()
        fun agregarAlCarrito(id : Int, item: String, cantidad: Int)
        fun editarCarrito(id : Int, item: String, cantidad: Int)
        fun editarNotificacion(id: Int)
        fun requestCarrito(id: Int)

    }
}