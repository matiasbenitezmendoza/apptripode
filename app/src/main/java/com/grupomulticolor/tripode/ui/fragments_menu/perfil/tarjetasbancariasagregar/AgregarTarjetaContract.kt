package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import mx.openpay.android.model.Token

interface AgregarTarjetaContract {

    interface View: BaseView<Presenter> {
        fun mostrarErrorEnNombreTarjeta(err: String)
        fun mostrarErrorEnNumTarjeta(err: String)
        fun mostrarErrorEnFechaVencimiento(err: String)
        fun mostrarErrorEnCVV(err: String)
        fun mostrarProgress(show: Boolean)
        fun mostrarMensajeError(resource: Int, msg: String?)
        fun mostrarMensajedeExito(resource: Int, resolve: () -> Unit)
        fun mostrarMensajeError(msg: String)
        fun mostrarErrorEnConexion()
        fun irAtras()
        fun onSuccesToken(token: Token?)
    }

    interface Presenter: BasePresenter {
        fun intentaAgregartarjeta(tarjetaBancaria: TarjetaBancaria)
        fun intentaGuardarTarjeta(tarjetaBancaria: TarjetaBancaria)
    }
}