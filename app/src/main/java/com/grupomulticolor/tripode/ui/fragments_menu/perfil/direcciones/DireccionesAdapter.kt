package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direcciones

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.interfaces.Events

class DireccionesAdapter(var direcciones: ArrayList<Direccion>,
                         var onClickEditar: Events,
                         var onClickEliminar: Events
                         ) : RecyclerView.Adapter<DireccionesAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val vw = inflater.inflate(R.layout.item_cardview_direccion, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return direcciones.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = direcciones[position]
        holder.bind(item)
        holder.btnEliminar.setOnClickListener { onClickEliminar.onClick(position) }
        holder.btnEditar.setOnClickListener { onClickEditar.onClick(position) }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val btnEliminar: ImageButton = itemView.findViewById(R.id.btnEliminar)
        val btnEditar: ImageButton = itemView.findViewById(R.id.btnEditar)
        val tvDireccion: TextView = itemView.findViewById(R.id.tvDireccion)

        fun bind(item: Direccion) {
            tvTitle.text = item.nombreReferencia
            tvDireccion.text = direccionCompleta(item)
        }

        private fun direccionCompleta(item: Direccion) : String {
            var noint = ""
            if (item.noInt.isNotEmpty())
                noint = "int ${item.noInt}"
            return "${item.calle} ${item.noExt} $noint\nCol. ${item.colonia}\n"+
                    "Residencial ${item.residencial} C.P. ${item.codigoPostal}\n"+
                    "${item.estado}\n${item.referencias}"
        }
    }

}