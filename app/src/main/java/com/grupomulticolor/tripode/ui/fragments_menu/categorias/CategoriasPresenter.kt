package com.grupomulticolor.tripode.ui.fragments_menu.categorias

import android.content.Context
import com.grupomulticolor.tripode.data.producto.ProductoCount
import com.grupomulticolor.tripode.data.producto.source.ProductoService
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener

class CategoriasPresenter ( val mProductoView : CategoriasContract.View, var context: Context) : CategoriasContract.Presenter{

    private val service: ProductoService = ProductoService.instance(context)

    init {
        mProductoView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {

    }

    override fun getproductos(id_dep: Int, limit: Int, offset: Int) {
        service.getProductosRelacionados("D", id_dep,"",limit,offset, object : ResponseListener<ResponseDataTripode<ProductoCount>> {

            override fun onSuccess(response: ResponseDataTripode<ProductoCount>) {
                var productoCount = response.data as ProductoCount
                mProductoView.agregarProductosFilterList(productoCount.items!!, productoCount.count!!)
            }

            override fun onError(t: Throwable) {
                mProductoView.muestrarErrorAlCargar("")
            }

        })

    }


}