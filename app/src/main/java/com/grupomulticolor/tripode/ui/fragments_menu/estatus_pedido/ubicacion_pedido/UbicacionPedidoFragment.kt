package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.grupomulticolor.tripode.MainActivity
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.mapa.Mapa
import com.grupomulticolor.tripode.data.repartidor.Repartidor
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import kotlinx.android.synthetic.main.fragment_mapa.*
import kotlinx.android.synthetic.main.item_cardview_repartidor.*

class UbicacionPedidoFragment : FragmentChildNotFab(), OnMapReadyCallback {

    private var gMap: GoogleMap? = null
    private var mapView: MapView? = null
    private var vw: View? = null
    private var orden = ""
    private var repartidor : Repartidor = Repartidor()
    private var marcador: Marker? =  null

    private lateinit var database: DatabaseReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_mapa, container, false)
        mapView = vw!!.findViewById(R.id.map)
        database = FirebaseDatabase.getInstance().reference

        if (mapView != null) {
            mapView!!.onCreate(null)
            mapView!!.onResume()
            mapView!!.getMapAsync(this)
        }
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnVerStatus.setOnClickListener {
            (context!! as MainActivity).irAtras()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Tvorden.text = "#"+String.format("%08d",Integer.parseInt(this.orden))
        nombreRepartidor.text = repartidor.nombre + " " + repartidor.apellido_paterno +" "+repartidor.apellido_materno
        avatarRepartidor
        if(repartidor.imagen != "") {
            Picasso
                .get()
                .load(repartidor.imagen)
                .into(avatarRepartidor)
        }

        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                        var punto: Mapa? = dataSnapshot.getValue(Mapa::class.java)
                        var lat = 19.4268195
                        var lon = -99.2947647

                        if(punto != null){
                                val lat = punto!!.latitud
                                val lon = punto!!.longitud
                        }

                        gMap = googleMap
                        gMap!!.clear()
                        gMap!!.addMarker( MarkerOptions()
                            .position(LatLng(lat!!, lon!!))
                            .title("")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_ft))
                        )

                        val cameraPosition = CameraPosition.builder()
                            .target(LatLng( lat!!,lon!!))
                            .zoom(15f)
                            .bearing(0f)
                            .tilt(7f)
                            .build()
                        gMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))


            }
            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        if(repartidor.id != 0 && repartidor.id != -1){
                var url = "repartidores/"+repartidor.id+"/"+orden
                database.child(url).addValueEventListener(postListener)
        }else{
                Snackbar.make(vw!!, "Su pedido aun se encuentra en la sucursal.", Snackbar.LENGTH_LONG).show()
        }



    }

    fun setOrden(orden: String){
        this.orden = orden
    }

    fun setIdRepartidor(rep: Repartidor){
        this.repartidor = rep
    }

    companion object {

        private var fragment: Fragment? = null

        @Synchronized fun instance() : Fragment {
            return UbicacionPedidoFragment()
        }
    }
}