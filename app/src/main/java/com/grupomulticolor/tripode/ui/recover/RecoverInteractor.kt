package com.grupomulticolor.tripode.ui.recover

import android.content.Context
import android.text.TextUtils
import android.util.Patterns
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.data.usuario.source.service.UsuarioService
import com.grupomulticolor.tripode.util.Network

class RecoverInteractor(private val context: Context) {

    private val service: UsuarioService = UsuarioService.instance(context)

    fun recuperarPassword(email: String, callback: Callback) {
        if (!isValidEmail(email, callback)) return
        if (!isNetworkAvalible(callback)) return
        recoverPass(email, callback)
    }

    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }

    /* verifica que el correo ingresado sea valido*/
    private fun isValidEmail(email: String, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(email)) {
            callback.onEmailError(context.getString(R.string.text_error_correo_vacio))
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onEmailError(context.getString(R.string.text_error_correo))
            return false
        }
        return true
    }

    /* intenta enviar peticion */
    private fun recoverPass(email: String, callback: Callback) {
        // realizar peticion con retrofit
        service.recoverPass(email, object : ResponseListener<ResponseData<Any>> {

            override fun onSuccess(response: ResponseData<Any>) {
                callback.onRequestSuccess(response.message)
            }

            override fun onError(t: Throwable) {
                callback.onRequestError(t.message!!)
            }
        })
    }

    /* interface para notificar al presentador cualquier evento */
    interface Callback {
        fun onEmailError(msg: String)
        fun onNetworkConnectionFailed()
        fun onRequestSuccess(msg: String)
        fun onRequestError(msg: String)
    }
}