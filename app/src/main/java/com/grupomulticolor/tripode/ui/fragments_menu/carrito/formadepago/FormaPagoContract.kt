package com.grupomulticolor.tripode.ui.fragments_menu.carrito.formadepago

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria

interface FormaPagoContract {

    interface View: BaseView<Presenter> {
        fun dataChanged()
        fun showTarjetasDeUsuario(tarjetas: ArrayList<TarjetaBancaria>)
        fun showError(error: String)
        fun showErrorNetwork()
        fun showEmpty()
        fun irDirecciones()
    }

    interface Presenter: BasePresenter {
        fun requestTarjetasDeUsuario()
        fun intentaContinuar(idTar: Int, tipoPago: Int)
    }
}