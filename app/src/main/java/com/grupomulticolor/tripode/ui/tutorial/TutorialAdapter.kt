package com.grupomulticolor.tripode.ui.tutorial

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.grupomulticolor.tripode.ui.tutorial.fragments.StepCompraFragment
import com.grupomulticolor.tripode.ui.tutorial.fragments.StepRecibeFragment


class TutorialAdapter(fm: FragmentManager, private val tabs: Int) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> StepCompraFragment.instace()
            1 -> StepRecibeFragment.instace()
            else -> StepRecibeFragment.instace()
        }
    }

    override fun getCount(): Int {
        return tabs
    }

}
