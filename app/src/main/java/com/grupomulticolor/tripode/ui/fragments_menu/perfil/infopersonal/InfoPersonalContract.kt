package com.grupomulticolor.tripode.ui.fragments_menu.perfil.infopersonal

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.usuario.Usuario

interface InfoPersonalContract {

    interface View : BaseView<Presenter> {
        fun mostrarInformacionUsuario(usuario: Usuario)
        fun mostrarError(msg: String)
        fun muestrarErrorConexion()
        fun irAEditarInformacionUsuario()
        fun irAtras()
    }

    interface Presenter : BasePresenter {
        fun consultarInformacionUsuario()
        fun intentaActualizarUsuario(user: Usuario)
    }

}