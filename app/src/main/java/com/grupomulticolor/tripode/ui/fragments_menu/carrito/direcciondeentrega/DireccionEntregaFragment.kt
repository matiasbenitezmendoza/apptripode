package com.grupomulticolor.tripode.ui.fragments_menu.carrito.direcciondeentrega

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.carrito.confirmarpedido.ConfirmarPedidoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar.AgregarDireccionFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_direccion_envio.*

class DireccionEntregaFragment : FragmentChildNotFab(), DireccionEntregaContract.View {

    private lateinit var vw: View
    private val direcciones: ArrayList<Direccion> = ArrayList()
    private lateinit var mAdapter: DireccionEntregaAdapter
    private lateinit var prefs: SessionPrefs
    private var mPresenter: DireccionEntregaContract.Presenter? = null
    private var carrito: ArrayList<Carrito>? = null
    private var tarjeta: TarjetaBancaria? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_direccion_envio, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DireccionEntregaPresenter(this, context!!)
        prefs = SessionPrefs(context!!)
        initEvents()
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initEvents() {

        btnAgregarDireccion.paintFlags = btnAgregarDireccion.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        btnBack.setOnClickListener { (context as MainActivity).irAtras() }
        btnContinuar.setOnClickListener {
            val idDir = prefs.getDireccionEntregId()
            mPresenter?.intentContinuar(idDir)
        }
        btnAgregarDireccion.setOnClickListener {
            // agregarDireccion
            (AgregarDireccionFragment.instance() as AgregarDireccionFragment).setData(Direccion())
            (context as MainActivity).mostrarFragment(AgregarDireccionFragment.instance())
        }

        checkboxTC.setOnClickListener {
            if (checkboxTC.isChecked) {
                prefs.saveDireccionEntrega(0)
                prefs.saveTipoEnvio(2)
                desactivarDirecciones(-1)
            } else {
                prefs.saveDireccionEntrega(-1)
                prefs.saveTipoEnvio(-1)
                desactivarDirecciones(-1)
            }
        }
    }

    private fun initAdapter() {
        mAdapter = DireccionEntregaAdapter(direcciones, object: Events {
            override fun onClick(itemId: Int) { // position
                if (direcciones[itemId].selected) {
                    prefs.saveDireccionEntrega(direcciones[itemId].id)
                    checkboxTC.isChecked = false
                    desactivarDirecciones(itemId)
                    prefs.saveTipoEnvio(1)

                } else {
                    prefs.saveDireccionEntrega(-1)
                    prefs.saveTipoPago(-1)
                }
            }
        }, object : Events {
            override fun onClick(itemId: Int) {
                val addFrag = AgregarDireccionFragment.instance() as AgregarDireccionFragment
                addFrag.setData(direcciones[itemId])
                goAdd(addFrag)
            }
        })
        mRecyclerView.adapter = mAdapter
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.setHasFixedSize(true)
    }

    private fun goAdd(fragment: AgregarDireccionFragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    private fun desactivarDirecciones(position: Int) {
        for (i in 0 until direcciones.size) {
            if (i != position) {
                direcciones[i].selected = false
                mAdapter.notifyItemChanged(i)
            }
        }
    }

    override fun dataChanged() {
       mAdapter.notifyDataSetChanged()
    }

    override fun showDireccionesDeUsuario(direcciones: ArrayList<Direccion>) {
        this.direcciones.clear()
        this.direcciones.addAll(direcciones)

        val idDir = prefs.getDireccionEntregId()

        if (idDir == 0) {
            checkboxTC.isChecked = true
            prefs.saveTipoEnvio(2)
        } else if (idDir >= 1) {
            val dir = this.direcciones.find { it.id == idDir }
            dir?.selected = true
            prefs.saveTipoEnvio(1)
        }
        dataChanged()
    }

    override fun showError(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)

            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun setPresenter(presenter: DireccionEntregaContract.Presenter) {
        mPresenter = presenter
    }

    override fun irConfirmarPedido() {
        var direccion: Direccion? = null
        direccion = direcciones.find { it.selected }
        val confirmarPedido = ConfirmarPedidoFragment.instance() as ConfirmarPedidoFragment
        confirmarPedido.setDatos(carrito, tarjeta, direccion)
        (context as MainActivity).mostrarFragment(confirmarPedido)

    }

    override fun showErrorNetwork() {

    }

    override fun showEmpty() {

    }

    fun setCarrito(c : ArrayList<Carrito>?){
        this.carrito = c
    }

    fun setTarjeta(t : TarjetaBancaria?){
        this.tarjeta = t
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DireccionEntregaFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }


}