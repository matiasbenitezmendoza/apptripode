package com.grupomulticolor.tripode.ui.fragments_menu.carrito.formadepago

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.carrito.direcciondeentrega.DireccionEntregaFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar.AgregarTarjetaFragment
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_formas_pago.*

class FormaPagoFragment: FragmentChildNotFab(), FormaPagoContract.View {

    private var mPresenter: FormaPagoContract.Presenter? = null
    private lateinit var mAdapter: FormaPagoAdapter
    private val tarjetas: ArrayList<TarjetaBancaria> = ArrayList()
    private lateinit var vw: View
    private lateinit var prefs: SessionPrefs
    private var carrito: ArrayList<Carrito>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_formas_pago, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FormaPagoPresenter(this, context!!)
        prefs = SessionPrefs(context!!)
        initEvents()
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initEvents() {
        btnAgregarTarjeta.paintFlags = btnAgregarTarjeta.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        btnBack.setOnClickListener { (context as MainActivity).irAtras() }
        btnContinuar.setOnClickListener {
            val idTar = prefs.getTarjetaId()
            val tipoEnvio = prefs.getTipoPago()

            mPresenter?.intentaContinuar(idTar, tipoEnvio)
        }
        btnAgregarTarjeta.setOnClickListener {
            (context as MainActivity).mostrarFragment(AgregarTarjetaFragment.instance())
        }
        checkboxTC.setOnClickListener {
            if (checkboxTC.isChecked) {
                prefs.saveTarjeta(0)
                desactivarTarjetas(-1)
                checkboxTarjeta.isChecked =  true
                checkboxEfectivo.isChecked =  false
                checkboxEfectivo.isEnabled =  true
                checkboxTarjeta.isEnabled =  true
                prefs.saveTipoPago(2)
            } else {
                prefs.saveTipoPago(-1)
                prefs.saveTarjeta(-1)
                desactivarTarjetas(-1)
                checkboxEfectivo.isChecked =  false
                checkboxTarjeta.isChecked =  false
                checkboxEfectivo.isEnabled =  false
                checkboxTarjeta.isEnabled =  false
            }
        }


        checkboxTarjeta.setOnClickListener {
            if(checkboxTarjeta.isChecked ){
                prefs.saveTipoPago(2)
                checkboxEfectivo.isChecked = false
            }else{
                checkboxEfectivo.isChecked = true
                prefs.saveTipoPago(3)
            }
        }


        checkboxEfectivo.setOnClickListener {
            if(checkboxEfectivo.isChecked ){
                prefs.saveTipoPago(3)
                checkboxTarjeta.isChecked = false
            }else{
                checkboxTarjeta.isChecked = true
                prefs.saveTipoPago(2)
            }
        }



    }

    private fun initAdapter() {
        mAdapter = FormaPagoAdapter(tarjetas, object : Events {
            override fun onClick(itemId: Int) {
                if (tarjetas[itemId].selected){
                    prefs.saveTarjeta(tarjetas[itemId].id)
                    prefs.saveTipoPago(1)
                    checkboxTC.isChecked = false
                    checkboxEfectivo.isChecked =  false
                    checkboxTarjeta.isChecked =  false
                    checkboxEfectivo.isEnabled =  false
                    checkboxTarjeta.isEnabled =  false

                    desactivarTarjetas(itemId)
                } else {
                    prefs.saveTipoPago(-1)
                    prefs.saveTarjeta(-1)
                }
            }
        })
        mRecyclerView.adapter = mAdapter
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.setHasFixedSize(true)
    }

    private fun desactivarTarjetas(position: Int) {
        for (i in 0 until tarjetas.size) {
            if (i != position) {
                tarjetas[i].selected = false
                mAdapter.notifyItemChanged(i)
            }
        }
    }

    override fun dataChanged() {
        mAdapter.notifyDataSetChanged()
    }

    override fun showTarjetasDeUsuario(tarjetas: ArrayList<TarjetaBancaria>) {
        this.tarjetas.clear()
        this.tarjetas.addAll(tarjetas)
        val idTar = prefs.getTarjetaId()
        val tipoPago = prefs.getTipoPago()

        if (tipoPago == 1) {
             if (idTar >= 1) {
                val tar = this.tarjetas.find { it.id == idTar }
                tar?.selected = true
            }
        }
        else {
            if (tipoPago == 2) {
                checkboxTC.isChecked = true
                checkboxTarjeta.isChecked =  true
                checkboxEfectivo.isChecked =  false
                checkboxTarjeta.isEnabled =  true
                checkboxEfectivo.isEnabled =  true
            } else if (tipoPago == 3) {
                checkboxTC.isChecked = true
                checkboxTarjeta.isChecked =  false
                checkboxEfectivo.isChecked =  true
                checkboxTarjeta.isEnabled =  true
                checkboxEfectivo.isEnabled =  true
            }
        }
        dataChanged()


    }

    override fun showError(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)

            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun irDirecciones() {
        var tarjeta: TarjetaBancaria? = null
            tarjeta = tarjetas.find { it.selected }
        val fragment = DireccionEntregaFragment.instance() as DireccionEntregaFragment
        fragment.setCarrito(carrito)
        fragment.setTarjeta(tarjeta)
        (context as MainActivity).mostrarFragment(fragment)

    }

    override fun setPresenter(presenter: FormaPagoContract.Presenter) {
        mPresenter = presenter
    }

    override fun showErrorNetwork() {

    }

    override fun showEmpty() {

    }

    fun setCarrito(c : ArrayList<Carrito>){
        this.carrito = c
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = FormaPagoFragment()
            }
            return fragment!!

        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }




}