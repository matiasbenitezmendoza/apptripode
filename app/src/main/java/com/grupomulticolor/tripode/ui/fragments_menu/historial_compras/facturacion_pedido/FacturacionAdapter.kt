package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido

import com.grupomulticolor.tripode.data.facturacion.Facturacion
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.interfaces.Events

class FacturacionAdapter(private val facturacion: ArrayList<Facturacion>,
                              private var onClickCheck: Events?,
                              private var onClickEditar: Events)
    : RecyclerView.Adapter<FacturacionAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vw = inflate.inflate(R.layout.item_facturacion, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return facturacion.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = facturacion[position]
        holder.bind(item)
        holder.cbSeleccionar.setOnClickListener {
            facturacion[position].selected = !facturacion[position].selected
            onClickCheck?.onClick(position)
        }
        holder.btnEditar.setOnClickListener { onClickEditar.onClick(position) }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        private val tvDireccion: TextView = itemView.findViewById(R.id.tvDireccion)
        val btnEditar: ImageButton = itemView.findViewById(R.id.btnEditar)
        val cbSeleccionar: CheckBox = itemView.findViewById(R.id.cbSeleccionar)

        fun bind(item: Facturacion) {
            tvTitle.text = item.nombre_razonSocial
            tvDireccion.text = detalleFacturacion(item)
            cbSeleccionar.isChecked = item.selected

        }

        private fun detalleFacturacion(item: Facturacion) : String {
            return "${item.rfc}\n${item.calle}\n" +
                    "Col ${item.colonia}\n" +
                    "C.P. ${item.codigoPostal}\n" +
                    "${item.municipio}\n" +
                    "Cfdi: ${item.cfdi_name}"
        }

    }
}