package com.grupomulticolor.tripode.ui.fragments_menu.carrito.direcciondeentrega

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.direcciones.Direccion

interface DireccionEntregaContract {

    interface View : BaseView<Presenter> {
        fun dataChanged()
        fun showDireccionesDeUsuario(direcciones: ArrayList<Direccion>)
        fun showError(error: String)
        fun showErrorNetwork()
        fun showEmpty()
        fun irConfirmarPedido()
    }

    interface Presenter : BasePresenter {
        fun requestDireccionesDelUsuario()
        fun intentContinuar(idDir: Int)
    }
}