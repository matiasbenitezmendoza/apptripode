package com.grupomulticolor.tripode.ui.fragments_menu.mensajes

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.chat.MessageChat
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_centrode_mensajes.*

class CentroMensajesFragment : FragmentChildNotFab(), CentroMensajesContract.View {


    private lateinit var vw: View
    private var mPresenter: CentroMensajesContract.Presenter? = null
    private var mensajes = ArrayList<MessageChat>()
    private var mAdapter: MessageAdapter? = null
    private var limit = 20
    private var offset = 0
    private var position = 0
    private var aptoParaCargar = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_centrode_mensajes, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CentroMensajesPresenter(this, context!!)
        initAdapter()

        ViewCompat.setNestedScrollingEnabled(vw,false)
        inputMensaje.requestFocus()
        mRecyclerView.scrollToPosition(0)

        btnSend.setOnClickListener {
            sendData()
        }

        mensajes.clear()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
        offset = 0
    }

    private fun initAdapter() {
        mAdapter = MessageAdapter(mensajes)
        mRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.stackFromEnd = true
        linearLayoutManager.reverseLayout =  true
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                Log.d("TAG", "scroll............")

                super.onScrolled(recyclerView, dx, dy)
                if (dy < 0) {
                    val visibleItemCount = linearLayoutManager.childCount
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition()

                    if (aptoParaCargar) {

                        if (visibleItemCount + pastVisibleItem >= totalItemCount) {
                            aptoParaCargar = false
                            offset += 20
                            position = mensajes.size -1
                            mPresenter?.loadMessages(limit, offset)
                        }
                    }
                }
            }
        })
    }

    private fun sendData() {
        val msg = inputMensaje.text.toString().trim()
        if (!TextUtils.isEmpty(msg)) {
            mPresenter?.sendMessage(msg)
        }
    }

    override fun showMessages(mensajes: ArrayList<MessageChat>) {
        aptoParaCargar = true
        this.mensajes.addAll(mensajes)
        mAdapter?.notifyDataSetChanged()
        position = if (position != -1) position else 0 // si en el chat hay 0 elementos y se realiza la busqueda queda -1
        mRecyclerView.scrollToPosition(position)
    }

    override fun showNetworkError() {
        Snackbar.make(vw, "Sin Conexion", Snackbar.LENGTH_LONG).show()
    }

    override fun showChatSend(messageChat: MessageChat) {
        aptoParaCargar = true
        mensajes.add(0, messageChat)
        mAdapter?.notifyDataSetChanged()
        mRecyclerView.scrollToPosition(0)
        inputMensaje.text.clear()
    }

    override fun showMessageError(err: String?) {
        aptoParaCargar = true

        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Advertencia")
            .setMessage(err ?: context!!.getString(R.string.error_unknown))
            .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar), null)
            .setButtonPositive("Ok", null).build()
        dialog.show()
    }

    override fun setPresenter(presenter: CentroMensajesContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = CentroMensajesFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }

}