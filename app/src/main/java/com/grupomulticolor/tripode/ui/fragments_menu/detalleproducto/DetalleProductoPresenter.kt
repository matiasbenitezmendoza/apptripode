package com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto

import android.content.Context
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.carrito.source.CarritoService
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.producto.ProductoCount
import com.grupomulticolor.tripode.data.producto.source.ProductoService
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener

class DetalleProductoPresenter(val mDetalleProductoView : DetalleContract.View, val producto: Producto, var context: Context)
    : DetalleContract.Presenter {
    private val service: CarritoService = CarritoService.instance(context)
    private val serviceProducto: ProductoService = ProductoService.instance(context)

    init {
        mDetalleProductoView.setPresenter(this)
    }

    override fun start() {
        requestProductosRelacionados()
        mDetalleProductoView.showProducto(producto)
    }

    override fun requestProductosRelacionados() {
        var id_cat = producto.category_Id!!

        serviceProducto.getProductosRelacionados("D", id_cat,"",10,1, object : ResponseListener<ResponseDataTripode<ProductoCount>> {

            override fun onSuccess(response: ResponseDataTripode<ProductoCount>) {
                    val pr = response.data as ProductoCount
                    val productosRelacionados = pr.items as ArrayList<Producto>
                    mDetalleProductoView.showProductosRelacionados(productosRelacionados)
            }

            override fun onError(t: Throwable) {
                   //mDetalleProductoView.showError(t.message!!)
            }

        })

    }

    override fun agregarAlCarrito(id : Int, item: String, cantidad: Int ) {
        service.addCarrito( id, item, cantidad,"" , object : ResponseListener<ResponseDataTripode<Any>> {

            override fun onSuccess(response: ResponseDataTripode<Any>) {
                try {
                    //esponse.data as Int
                    mDetalleProductoView.showAddProducto()
                    //editarCarrito(id, item, cantidad)
                } catch (e: Exception) {
                    e.printStackTrace()
                    mDetalleProductoView.showError(e.message!!)
                }
            }

            override fun onError(t: Throwable) {
                mDetalleProductoView.showError(t.message!!)
            }

        })


    }

    override fun editarCarrito(id : Int, item: String, cantidad: Int ) {
        service.updateCarrito( id, item, cantidad ,"", object : ResponseListener<ResponseDataTripode<Any>> {

            override fun onSuccess(response: ResponseDataTripode<Any>) {
                try {
                    mDetalleProductoView.showAddProducto()
                } catch (e: Exception) {
                    e.printStackTrace()
                    mDetalleProductoView.showError(e.message!!)
                }
            }

            override fun onError(t: Throwable) {
                mDetalleProductoView.showError(t.message!!)
            }


        })



    }

    override fun editarNotificacion(id : Int) {
        service.getCarrito(id,"", object : ResponseListener<ResponseDataTripode<ArrayList<Carrito>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<Carrito>>) {
                try {
                    mDetalleProductoView.showNotificacion(response.data as ArrayList<Carrito>)
                } catch (e: Exception) {
                    e.printStackTrace()
                    mDetalleProductoView.showNotificacionError()
                }
            }

            override fun onError(t: Throwable) {
                mDetalleProductoView.showNotificacionError()
            }


        })

    }

    override fun requestCarrito(id : Int) {
        service.getCarrito(id,"", object : ResponseListener<ResponseDataTripode<ArrayList<Carrito>>> {

            override fun onSuccess(response: ResponseDataTripode<ArrayList<Carrito>>) {
                try {
                    mDetalleProductoView.setCarrito(response.data as ArrayList<Carrito>)
                } catch (e: Exception) {
                    e.printStackTrace()
                    mDetalleProductoView.showCarritoVacio()
                }
            }

            override fun onError(t: Throwable) {
                mDetalleProductoView.showCarritoVacio()
            }


        })

    }


}