package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido.FacturacionPedidoContract
import com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido.FacturacionPedidoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido.FacturacionPedidoPresenter
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import kotlinx.android.synthetic.main.fragment_historial_compras.*
import java.util.*
import kotlin.collections.ArrayList

class HistorialFragment : FragmentChildNotFab(), HistorialContract.View {

    private var mPresenter: HistorialContract.Presenter? = null
    private lateinit var vw: View
    private var ordenes = ArrayList<Ordenes>()
    private var pedidos = ArrayList<Pedido>()

    private var allordenes = ArrayList<Ordenes>()
    private var mAdapter: HistorialAdapter? = null
    private var filtro = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_historial_compras, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        HistorialPresenter(this, context!!)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initAdapter() {
        mAdapter = HistorialAdapter(ordenes, pedidos, object : HistorialAdapter.ListenerView {
            override fun onClick(item: Ordenes, b : Boolean) {
                FacturacionPedidoPresenter(
                    FacturacionPedidoFragment.instance() as FacturacionPedidoContract.View,  context!!, item)
                var f = FacturacionPedidoFragment.instance() as FacturacionPedidoFragment
                f.setBandera(b)
                fragmentTransaction(f)
            }
        })
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context) // add listener
        mRecyclerView.adapter = mAdapter

        val meses = arrayListOf("Mes Actual")
        meses.add("3 meses")
        meses.add("6 meses")
        val adapter : ArrayAdapter<String> = ArrayAdapter(context!!, R.layout.spinner_text_item, meses)
        spinnerMes.adapter = adapter
        spinnerMes.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                         evento()
            }

        }
    }

    override fun showPedidos(ordenes: ArrayList<Ordenes>, pedidos: ArrayList<Pedido>) {
        this.allordenes.clear()
        this.pedidos.clear()
        this.allordenes.addAll(ordenes)
        this.pedidos.addAll(pedidos)
        this.evento()
    }

    override fun showErrorNetwork() {
        Snackbar.make(vw, "Ocurrio un problema al obtener su historial", Snackbar.LENGTH_LONG).show()
    }

    override fun setPresenter(presenter: HistorialContract.Presenter) {
        mPresenter = presenter
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    private fun evento (){
        this.ordenes.clear()
        var p = setFiltro(this.allordenes)
        this.ordenes.addAll(p)
        this.ordenes.reverse()
        mAdapter?.notifyDataSetChanged()
    }

    private fun setFiltro(ordenes: ArrayList<Ordenes>): ArrayList<Ordenes>{

        filtro =  spinnerMes.selectedItemPosition
        var ord: ArrayList<Ordenes> =  ArrayList<Ordenes>()

        //numero de dias
        var tipof = 32

        if(filtro == 1) {
            tipof = 94
        }
        if(filtro == 2) {
            tipof = 187
        }

        for(orden in ordenes){
                val fecha_actual = Calendar.getInstance()
                val fecha_pedido = Calendar.getInstance()

                var anio: Int = ((orden.fecha_pedido!!).substring(0,4)).toInt()
                var mes: Int = ((orden.fecha_pedido!!).substring(5,7)).toInt()
                var dia: Int = ((orden.fecha_pedido!!).substring(8,10)).toInt()

                fecha_pedido.isLenient = false
                fecha_pedido.set(Calendar.YEAR, anio)
                fecha_pedido.set(Calendar.MONTH, mes - 1  ) // [0,...,11]
                fecha_pedido.set(Calendar.DAY_OF_MONTH, dia)

                var m = (fecha_actual.timeInMillis - fecha_pedido.timeInMillis)
                var diferencia = ( ( (m/1000)/60) / 60 ) / 24

               //comparacion numero de dias
               if(diferencia < tipof){
                   ord.add(orden)
               }
        }

        return ord
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = HistorialFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }
}