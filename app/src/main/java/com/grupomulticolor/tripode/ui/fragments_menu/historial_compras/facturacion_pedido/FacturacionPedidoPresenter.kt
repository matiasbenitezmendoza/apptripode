package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido

import android.content.Context
import com.grupomulticolor.tripode.data.detallepedido.DetalleOrden
import com.grupomulticolor.tripode.data.facturacion.Factura
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.facturacion.source.FacturaService
import com.grupomulticolor.tripode.data.facturacion.source.FacturacionService
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.pedido.source.OrdenesService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import java.lang.Exception

class FacturacionPedidoPresenter(var mContractView: FacturacionPedidoContract.View, var context: Context , val orden: Ordenes) : FacturacionPedidoContract.Presenter {

    private val service: OrdenesService = OrdenesService.instance(context)
    private val serviceFacturacion: FacturacionService = FacturacionService.instance(context)
    private val serviceFactura: FacturaService = FacturaService.instance(context)

    init {
        mContractView.setPresenter(this)
    }

    override fun start() {
        mContractView.showPedido(orden)
        requestDetallesProducto()

    }

    override fun requestFacturacionDelUsuario() {
        serviceFacturacion.getAll(object : ResponseListener<ResponseData<ArrayList<Facturacion>>> {
                override fun onSuccess(response: ResponseData<ArrayList<Facturacion>>) {
                    try {
                        mContractView.showFacturacionDeUsuario(response.data as ArrayList<Facturacion>)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        // mView.showMessageError(e.message)
                        mContractView.showEmpty()
                    }
                }

                override fun onError(t: Throwable) {
                    // mView.showMessageError(t.message)
                    mContractView.showEmpty()
                }
            })
    }


    override fun intentaFinalizarCompra(num_transaccion: String, fac : Facturacion) {

        serviceFactura.post("",num_transaccion,fac.rfc,fac.email,fac.cfdi, object : ResponseListener<ResponseDataTripode<Factura>> {
            override fun onSuccess(response: ResponseDataTripode<Factura>) {
                try {
                    mContractView.showFacturacionExitosa(response.data as Factura)
                } catch (e: Exception) {
                    e.printStackTrace()
                    mContractView.showFacturacionError(e.message!!)
                }
            }

            override fun onError(t: Throwable) {
                mContractView.showFacturacionError(t.message!!)
            }
        })
    }

    override fun agregarDireccionFiscal() {
    }

    override fun requestDetallesProducto() {

        service.getDetallePedidos(orden.orden!!,"",object : ResponseListener<ResponseDataTripode<ArrayList<DetalleOrden>>> {
            override fun onSuccess(response: ResponseDataTripode<ArrayList<DetalleOrden>>) {
                try {

                    var ordenes = response.data as ArrayList<DetalleOrden>
                    mContractView.intentarAgregarDetalle(ordenes)

                } catch (e: Exception) {
                    e.printStackTrace()
                    mContractView.showErrorDetallePedido(e.message!!)
                }
            }

            override fun onError(t: Throwable) {
                mContractView.showErrorDetallePedido(t.message!!)
            }
        })
    }
}