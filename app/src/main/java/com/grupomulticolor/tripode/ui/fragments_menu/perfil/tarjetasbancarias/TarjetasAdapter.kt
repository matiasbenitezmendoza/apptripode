package com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancarias

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.interfaces.Events

class TarjetasAdapter(var tarjetasbancarias: ArrayList<TarjetaBancaria>,
                      var onClickEliminar: Events? = null
                      ) : RecyclerView.Adapter<TarjetasAdapter.ViewHolder>(){

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vw = inflate.inflate(R.layout.item_cardview_tarjeta_bancaria, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return tarjetasbancarias.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = tarjetasbancarias[position]
        holder.bind(item)
        holder.btnEliminar.setOnClickListener { onClickEliminar?.onClick(position) }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvDetalle: TextView = itemView.findViewById(R.id.tvDetalle)
        val ivTipoTarjeta: ImageView = itemView.findViewById(R.id.ivTipoTarjeta)
        val btnEliminar: ImageButton = itemView.findViewById(R.id.btnEliminar)

        fun bind(item: TarjetaBancaria) {
            tvTitle.text = "Tarjeta Visa"
            tvDetalle.text = item.detalle()
        }

    }
}