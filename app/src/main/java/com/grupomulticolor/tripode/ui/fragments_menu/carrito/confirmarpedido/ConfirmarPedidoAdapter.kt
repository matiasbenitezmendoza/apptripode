package com.grupomulticolor.tripode.ui.fragments_menu.carrito.confirmarpedido

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.data.tarjeta.TarjetaBancaria
import com.grupomulticolor.tripode.util.Currency

class ConfirmarPedidoAdapter(var carrito: ArrayList<Carrito>, var direccionEntrega: Direccion?, var formaPago: TarjetaBancaria?)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vh: RecyclerView.ViewHolder
        val vw: View

        when(viewType) {
            Tipo.PRODUCT -> {
                vw = inflate.inflate(R.layout.item_confirmar_producto, parent, false)
                vh = ViewHolderItemProducto(vw)
            }
            Tipo.TOTAL -> {
                vw = inflate.inflate(R.layout.item_tv_total, parent, false)
                vh = ViewHolderItemTotal(vw)
            }
            Tipo.DIRECCION -> {
                vw = inflate.inflate(R.layout.item_cardview_direccion, parent, false)
                vh = ViewHolderItemDireccion(vw)
            }
            Tipo.TARJETA -> {
                vw = inflate.inflate(R.layout.item_cardview_tarjeta_bancaria, parent, false)
                vh = ViewHolderItemTarjeta(vw)
            }
            else -> {
                vw = inflate.inflate(R.layout.item_empty, parent, false)
                vh = ViewHolderItemEmpty(vw)
            }
        }
        return vh
    }

    override fun getItemCount(): Int {
        var items = 0
        if (carrito.isNotEmpty()) {
            //  calculo del total + direccion entrega + forma de pago
            items += carrito.size + 1
            if (direccionEntrega != null)
                items += 1
            if (formaPago != null)
                items += 1
        }

        return items
    }

    override fun getItemViewType(position: Int): Int {

        return when {
            carrito.size > position -> Tipo.PRODUCT
            carrito.size == position -> Tipo.TOTAL
            carrito.size + 1 == position && direccionEntrega != null -> Tipo.DIRECCION
            carrito.size + 1 == position && direccionEntrega == null && formaPago != null -> Tipo.TARJETA
            carrito.size + 2 == position && formaPago != null -> Tipo.TARJETA
            else -> -1
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when(holder.itemViewType) {
            Tipo.PRODUCT -> (holder as ViewHolderItemProducto).bind(carrito[position])
            Tipo.TOTAL -> {
                var total = 0.0
                for (pro in carrito) {
                    total += pro.total!!
                }
                (holder as ViewHolderItemTotal).tvTotal.text = Currency.toCurrency(total, "MXN")
            }
            Tipo.TARJETA -> (holder as ViewHolderItemTarjeta).bind(formaPago)
            Tipo.DIRECCION -> (holder as ViewHolderItemDireccion).bind(direccionEntrega)
            else -> (holder as ViewHolderItemEmpty)
        }
    }

    inner class ViewHolderItemProducto(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitleProducto: TextView = itemView.findViewById(R.id.tvTitleProducto)
        val tvCantidad: TextView = itemView.findViewById(R.id.tvCantidad)
        val tvTotal: TextView = itemView.findViewById(R.id.tvTotal)
        val infoProducto: TextView = itemView.findViewById(R.id.infoProducto)

        fun bind(carrito: Carrito?) {
            tvTitleProducto.text = carrito?.nombre
            tvCantidad.text = carrito?.cantidad.toString()
            tvTotal.text =
                Currency.toCurrency((carrito?.total!!), "MXN")
            infoProducto.text = carrito.detalle
        }
    }

    inner class ViewHolderItemTotal(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTotal: TextView = itemView.findViewById(R.id.tvTotal)
    }

    inner class ViewHolderItemDireccion(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvDireccion: TextView = itemView.findViewById(R.id.tvDireccion)
        val btnEliminar: ImageButton = itemView.findViewById(R.id.btnEliminar)
        val btnEditar: ImageButton = itemView.findViewById(R.id.btnEditar)

        fun bind(direccion: Direccion?) {
            tvTitle.text = direccion?.nombreReferencia
            tvDireccion.text = direccion?.detalle()
            btnEliminar.visibility = View.GONE
            btnEditar.visibility = View.GONE
        }
    }

    inner class ViewHolderItemTarjeta(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvDetalle: TextView = itemView.findViewById(R.id.tvDetalle)
        val ivTipoTarjeta: ImageView = itemView.findViewById(R.id.ivTipoTarjeta)
        val btnEliminar: ImageButton = itemView.findViewById(R.id.btnEliminar)

        fun bind(tarjetaBancaria: TarjetaBancaria?) {
            tvTitle.text = tarjetaBancaria?.tipo
            tvDetalle.text = tarjetaBancaria?.detalle()
            btnEliminar.visibility = View.GONE

            if(tarjetaBancaria!!.tipo == "mastercard") {
                Picasso
                    .get()
                    .load(R.drawable.ico_mc)
                    .into(ivTipoTarjeta)
                tvTitle.text = "Tarjeta MasterCard"

            }else{
                Picasso
                    .get()
                    .load(R.drawable.visa)
                    .into(ivTipoTarjeta)
                tvTitle.text = "Tarjeta Visa"

            }
        }
    }

    inner class ViewHolderItemEmpty(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    object Tipo {
        const val PRODUCT = 1
        const val TOTAL = 2
        const val DIRECCION = 3
        const val TARJETA = 4
    }

}