package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add

import android.content.Context
import android.text.TextUtils
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.cfdi.Cfdi
import com.grupomulticolor.tripode.data.cfdi.source.CfdiService
import com.grupomulticolor.tripode.data.direcciones.*
import com.grupomulticolor.tripode.data.direcciones.source.DireccionService
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.facturacion.source.FacturacionService
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.residencial.source.ResidencialService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.Network
import com.grupomulticolor.tripode.util.config.Constants
import java.util.regex.Pattern

class AddFacturacionInteractor(var context: Context) {

    private val service: FacturacionService = FacturacionService.instance(context)
    private val direccionService: DireccionService = DireccionService.instance(context)
    private val cfdiservice: CfdiService = CfdiService.instance(context)
    private val residencialservice: ResidencialService = ResidencialService.instance(context)

    fun addFacturacion(facturacion: Facturacion, callback: Callback) {

        if (!isValidRFC(facturacion.rfc, callback)) return
        if (!isValidRazon(facturacion.nombre_razonSocial, callback)) return
        if (!isValidCalle(facturacion.calle, callback)) return
        if (!isValidNumExt(facturacion.noExt, callback)) return
        if (!isValidCondigoPostal(facturacion.codigoPostal, callback)) return
        if (!isValidEstado(facturacion.estado, callback)) return
        if (!isValidColonia(facturacion.colonia, callback)) return
        if (!isValidCiudad(facturacion.municipio, callback)) return
        if (!isNetworkAvalible(callback)) return

        if (facturacion.id != 0)
            put(facturacion, callback)
        else
            post(facturacion, callback)

    }



    fun existeDireccionEntregaSeleccionada(callback: Callback) {

        // extraer de prefs
        if(false) {

        } else {
            callback.onRequestFailed(context.getString(R.string.no_hay_datos_entrega))
        }

    }




    private fun post(facturacion: Facturacion, callback: Callback) {

        service.post(facturacion, object : ResponseListener<ResponseData<Facturacion>> {
            override fun onSuccess(response: ResponseData<Facturacion>) {
                val msg = context.getString(R.string.mensaje_direccion_agregada)
                callback.onRequestUpdateSucces(msg)
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })

    }

    private fun put(facturacion: Facturacion, callback: Callback) {
        service.put(facturacion, object : ResponseListener<ResponseData<Facturacion>> {
            override fun onSuccess(response: ResponseData<Facturacion>) {
                val msg = context.getString(R.string.mensaje_direccion_actualizada)
                callback.onRequestUpdateSucces(msg)
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getCfdi(callback: Callback) {
        cfdiservice.get(object : ResponseListener<ResponseDataTripode<ArrayList<Cfdi>>> {
            override fun onSuccess(response: ResponseDataTripode<ArrayList<Cfdi>>) {
                try {
                    callback.onRequestCfdi(response.data as ArrayList<Cfdi>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }


    fun getResidencial(callback: Callback) {
        residencialservice.get(object : ResponseListener<ResponseData<ArrayList<Residencial>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Residencial>>) {
                try {
                    callback.onRequestResidencial(response.data as ArrayList<Residencial>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getEstados(callback: Callback) {
        direccionService.getEstados(object : ResponseListener<ResponseData<ArrayList<Estados>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Estados>>) {
                try {
                    callback.onRequestEstados(response.data as ArrayList<Estados>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getMunicipios(mun: String, callback: Callback) {
        direccionService.getMunicipios(mun,object : ResponseListener<ResponseData<ArrayList<Municipios>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Municipios>>) {
                try {
                    callback.onRequestMunicipios(response.data as ArrayList<Municipios>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }

    fun getColonias(est: String, mun: String, callback: Callback) {
        direccionService.getColonias(est,mun,object : ResponseListener<ResponseData<ArrayList<Colonias>>> {
            override fun onSuccess(response: ResponseData<ArrayList<Colonias>>) {
                try {
                    callback.onRequestColonias(response.data as ArrayList<Colonias>)
                } catch (e: Exception) {
                    callback.onRequestFailed(e.message!!)
                }
            }
            override fun onError(t: Throwable) {
                callback.onRequestFailed(t.message!!)
            }
        })
    }


    /* comprobar la conexion de red */
    private fun isNetworkAvalible(callback: Callback) : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            callback.onNetworkConnectionFailed()
        return isAvailable
    }

    /* verifica que el nombre ingresado sea correcto*/
    private fun isValidRFC(nombre: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(nombre)) {
            callback.onRFCError(context.getString(R.string.error_item_empty))
            return false
        }
        return true
    }

    /* verifica que el nombre ingresado sea correcto*/
    private fun isValidRazon(nombre: String?, callback: Callback) : Boolean {

        if (TextUtils.isEmpty(nombre)) {
            callback.onRazonError(context.getString(R.string.error_item_empty))
            return false
        }
        return true
    }


    /* verifica que la calle sea correcta*/
    private fun isValidCalle(calle: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(calle)) {
            callback.onCalleError(context.getString(R.string.error_item_empty))
            return false
        }

        val patron = Pattern.compile(Constants.patternNombre)
        if (!patron.matcher(calle).matches()) {
            callback.onCalleError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    /* verifica que el num ext ingresado sea correcto*/
    private fun isValidNumExt(numExt: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(numExt)) {
            callback.onExtError(context.getString(R.string.error_item_empty))
            return false
        }

        return true
    }

    /* verifica que la residencia seleccionada sea correcta*/
    private fun isValidResidencial(residencial: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(residencial)) {
            callback.onResidencialError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidCondigoPostal(cp: String?, callback: Callback) : Boolean  {
        if (TextUtils.isEmpty(cp)) {
            callback.onCPError(context.getString(R.string.error_item_empty))
            return false
        }
        val patron = Pattern.compile(Constants.patternNumber)
        if (!patron.matcher(cp).matches()) {
            callback.onCPError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidEstado(estado: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(estado)) {
            callback.onEstadoError(context.getString(R.string.error_match))
            return false
        }
        return true
    }

    private fun isValidColonia(colonia: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(colonia)) {
            callback.onColoniaError(context.getString(R.string.error_match))
            return false
        }

        if (context.getString(R.string.spinner_header_colonia) == colonia) {
            callback.onColoniaError(context.getString(R.string.error_match))
            return false
        }

        return true
    }

    private fun isValidCiudad(ciudad: String?, callback: Callback) : Boolean {
        if (TextUtils.isEmpty(ciudad)) {
            callback.onMunDelError(context.getString(R.string.error_match))
            return false
        }
        return true
    }


    interface Callback {
        fun onRFCError(err: String)
        fun onRazonError(err: String)
        fun onCalleError(err: String)
        fun onExtError(err: String)
        fun onResidencialError(err: String)
        fun onCPError(err: String)
        fun onEstadoError(err: String)
        fun onColoniaError(err: String)
        fun onMunDelError(err: String)
        fun onCfdiError(err: String)

        fun onNetworkConnectionFailed()
        fun onRequestFailed(err: String)
        fun onRequestAddSuccess(msg: String)
        fun onRequestUpdateSucces(msg: String)
        fun onDireccionEntrega(direccion: Direccion)


        fun onRequestEstados(estados: ArrayList<Estados>)
        fun onRequestColonias(estados: ArrayList<Colonias>)
        fun onRequestMunicipios(municipios: ArrayList<Municipios>)
        fun onRequestCfdi(Cfdi: ArrayList<Cfdi>)
        fun onRequestResidencial(Cfdi: ArrayList<Residencial>)

    }
}