package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.pedido.Pedido

interface EstatusContract {

    interface View : BaseView<Presenter> {
        fun showPedidos(ordenes: ArrayList<Pedido>)
        fun showErrorNetwork()
    }

    interface Presenter : BasePresenter {
        fun loadOrdenes(limit: Int, offset : Int)
    }
}