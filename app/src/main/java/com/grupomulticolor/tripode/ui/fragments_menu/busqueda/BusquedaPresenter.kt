package com.grupomulticolor.tripode.ui.fragments_menu.busqueda

import android.content.Context
import com.grupomulticolor.tripode.data.producto.ProductoCount
import com.grupomulticolor.tripode.data.producto.source.ProductoService
import com.grupomulticolor.tripode.data.service.model.ResponseDataTripode
import com.grupomulticolor.tripode.data.service.model.ResponseListener


class BusquedaPresenter( val mProductoView : BusquedaContract.View, var context: Context) : BusquedaContract.Presenter{

    private val service: ProductoService = ProductoService.instance(context)

    init {
        mProductoView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
             //getproductos("pastillas", 20,1,0,0)
    }

    override fun getproductos(search: String, limit: Int, offset: Int, oa: Int, op: Int) {

        service.getProductos("", search, limit, offset, oa, op, object :
            ResponseListener<ResponseDataTripode<ProductoCount>> {

            override fun onSuccess(response: ResponseDataTripode<ProductoCount>) {
                var productoCount = response.data as ProductoCount
                mProductoView.agregarProductosFilterList(productoCount.items!!, productoCount.count!!)
            }

            override fun onError(t: Throwable) {
               mProductoView.muestrarErrorAlCargar(t.message!!)
            }

        })
    }




}