package com.grupomulticolor.tripode.ui.fragments_menu.mensajes

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.chat.MessageChat

interface CentroMensajesContract {

    interface View: BaseView<Presenter> {
        fun showMessages(mensajes: ArrayList<MessageChat>)
        fun showChatSend(messageChat: MessageChat)
        fun showMessageError(err: String?)
        fun showNetworkError()
    }

    interface Presenter : BasePresenter {
        fun loadMessages(limit: Int, offset: Int)
        fun sendMessage(messageChat: String)
    }
}