package com.grupomulticolor.tripode.ui.registro

import android.content.Context
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.util.lib.TextDrawable
import kotlinx.android.synthetic.main.fragment_registro.*
import android.graphics.Paint
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ScrollView
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.grupomulticolor.tripode.data.direcciones.Colonias
import com.grupomulticolor.tripode.data.padecimientos.Padecimiento
import com.grupomulticolor.tripode.data.direcciones.DireccionPostal
import com.grupomulticolor.tripode.data.direcciones.Estados
import com.grupomulticolor.tripode.data.direcciones.Municipios
import com.grupomulticolor.tripode.data.residencial.Residencial
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.util.Input
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_registro.inputCP
import kotlinx.android.synthetic.main.fragment_registro.inputCalle
import kotlinx.android.synthetic.main.fragment_registro.inputNumExt
import kotlinx.android.synthetic.main.fragment_registro.inputNumInt
import kotlinx.android.synthetic.main.fragment_registro.spinnerColonia
import kotlinx.android.synthetic.main.fragment_registro.spinnerMunicipio
import kotlinx.android.synthetic.main.fragment_registro.spinnerResidencial

class RegistroFragment(var user: Usuario)  : Fragment(), RegistroContract.View {

    private var mPresenter: RegistroContract.Presenter? = null
    private lateinit var cntx: Context

    private val mPadecimientos: ArrayList<Padecimiento> = ArrayList()

    private var mEstados: ArrayList<Estados> = ArrayList()
    private var mMunicipios: ArrayList<Municipios> = ArrayList()
    private var mColonias: ArrayList<Colonias> = ArrayList()
    private var mResidencial: ArrayList<Residencial> = ArrayList()
  // private var user = Usuario()



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_registro, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cntx = context!!
        setEvents()
        inputNombre.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
        setDatosFacebook()

    }

    /* eventos de botonos e inputs de la interfaz*/
    private fun setEvents() {
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired
        if(isLoggedIn){
            LoginManager.getInstance().logOut()
            SessionPrefs(context!!).logOut()
        }

        agregaTextListenerAInput(inputNombre)
        agregaTextListenerAInput(inputApellidoPaterno)
        agregaTextListenerAInput(inputApellidoMaterno)
        agregaTextListenerAInput(inputFechNac)
        agregaTextListenerAInput(inputCalle)
        agregaTextListenerAInput(inputNumExt)
        agregaTextListenerAInput(inputNumInt)
        agregaTextListenerAInput(inputCP)
        agregaTextListenerAInput(inputCorreo)
        agregaTextListenerAInput(inputConfirmCorreo)
        agregaTextListenerAInput(inputPassword)
        agregaTextListenerAInput(inputConfirmPassword)

        /*
        inputCP.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                mPresenter?.consultarDireccionPostal(inputCP.text.toString())
            }
        }*/

        mPresenter?.consultarEstados()
        mPresenter?.consultarResidenciales()

        spinnerEstado.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                mPresenter?.consultarMunicipios(mEstados[p3.toInt()].Id.toString())
            }

        }

        spinnerMunicipio.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                mPresenter?.consultarColonias(mEstados[spinnerEstado.selectedItemPosition].Id.toString() ,mMunicipios[p3.toInt()].Id.toString())
            }

        }


        inputFechNac.inputType = InputType.TYPE_CLASS_NUMBER
        inputFechNac.keyListener = DigitsKeyListener.getInstance("1234567890+-()/")
        val birthday = MaskedTextChangedListener("[00]/[00]/[0000]", inputFechNac)
        inputFechNac.addTextChangedListener(birthday)
        inputFechNac.onFocusChangeListener = birthday

        btnEnviar.setOnClickListener { registrarUsuario() }
        btnTerminosCondiciones.paintFlags = btnTerminosCondiciones.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        btnTerminosCondiciones.setOnClickListener { muestraTerminosyCondiciones() }


        btnFormulario.paintFlags = btnTerminosCondiciones.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        btnFormulario.setOnClickListener { regresarRegistro() }





    }

    private fun agregaTextListenerAInput(editText: EditText) {
        editText.addTextChangedListener( object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Input.onClear(cntx, editText)
            }
        })
    }

    private fun registrarUsuario() {
        user.nombre = inputNombre.text.toString()
        user.apellidoPaterno = inputApellidoPaterno.text.toString()
        user.apellidoMaterno = inputApellidoMaterno.text.toString()
        user.fechaNac = inputFechNac.text.toString()
        user.calle = inputCalle.text.toString()
        user.numExt = inputNumExt.text.toString()
        user.numInt = inputNumInt.text.toString()
        user.codigoPostal = inputCP.text.toString()

        if (spinnerMunicipio.selectedItem != null)
            user.ciudad = mMunicipios[spinnerMunicipio.selectedItemPosition].Id.toString()
        if (spinnerEstado.selectedItem != null)
            user.estado = mEstados[spinnerEstado.selectedItemPosition].Id.toString()
        if (spinnerColonia.selectedItem != null)
            user.colonia = mColonias[spinnerColonia.selectedItemPosition].Id.toString()
        if (spinnerResidencial.selectedItem != null)
            user.residencialId = mResidencial[spinnerResidencial.selectedItemPosition].nombre


        user.correo = inputCorreo.text.toString()
        user.correoConfirm = inputConfirmCorreo.text.toString()
        user.password = inputPassword.text.toString()
        user.passwordConfirm = inputConfirmPassword.text.toString()
        user.terminosCondiciones = checkboxTC.isChecked
        user.padecimientos = getCadenaPadecimientos()
        mPresenter?.intentaRegistrarUsuario(user)

        //setDatosFacebook()
    }

    private fun showInputError(editText: EditText, error: String) {
        editText.background = ContextCompat.getDrawable(cntx, R.drawable.input_white_error)
        editText.setCompoundDrawables(null, null, TextDrawable(cntx, error), null)
    }

    override fun muestraProgress(show: Boolean) {
    }

    override fun muestraErrorEnNombre(error: String) {
        showInputError(inputNombre, error)
    }

    override fun muestraErrorEnApellidoPaterno(error: String) {
        showInputError(inputApellidoPaterno, error)
    }

    override fun muestraErrorEnApellidoMaterno(error: String) {
        showInputError(inputApellidoMaterno, error)
    }

    override fun muestraErrorEnFechaNac(error: String) {
        showInputError(inputFechNac, error)
    }

    override fun muestraErrorEnCalle(error: String) {
        showInputError(inputCalle, error)
    }

    override fun muestraErrorEnNumExt(error: String) {
        showInputError(inputNumExt, error)
    }

    override fun muestraErrorEnNumInt(error: String) {
        showInputError(inputNumInt, error)
    }

    override fun muestraErrorEnResidencia(error: String) {
        try {
            spinnerResidencial.background = ContextCompat.getDrawable(cntx, R.drawable.spinner_error)
            Snackbar.make(view!!, error, Snackbar.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun muestraErrorEnCodigoPostal(error: String) {
        showInputError(inputCP, error)
    }

    override fun muestraErrorListado(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestraErrorEnCorreo(error: String) {
        showInputError(inputCorreo, error)
    }

    override fun muestraErrorEnConfirmCorreo(error: String) {
        showInputError(inputConfirmCorreo, error)
    }

    override fun muestraErrorEnPasswd(error: String) {
        showInputError(inputPassword, error)
    }

    override fun muestraErrorEnConfirmPasswd(error: String) {
        showInputError(inputConfirmPassword, error)
    }

    override fun muestraErrorEnTerminosyCondiciones(error: String) {
        try {
            Snackbar.make(view!!, error, Snackbar.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun muestrarErrorAlCargar(err: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(err)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestraDatosDeCP(direccionPostal: DireccionPostal) {
        //inputEstado.setText(direccionPostal.estado)
        //inputDelMun.setText(direccionPostal.munucipio)
        //val colonias = arrayListOf(context!!.getString(R.string.spinner_header_colonia))
        //if (direccionPostal.colonias != null) colonias.addAll(direccionPostal.colonias!!)
        //val adapter : ArrayAdapter<String> = ArrayAdapter(context!!, R.layout.spinner_text_item, colonias)
        //spinnerColonia.adapter = adapter
    }

    override fun muestraDatosEstados(estados: ArrayList<Estados>) {
        mEstados.clear()
        mEstados.addAll(estados)
        val adapter : ArrayAdapter<Estados> = ArrayAdapter(context!!, R.layout.spinner_text_item, mEstados)
        spinnerEstado.adapter = adapter

        /*Datos por defaul v1*/
        spinnerEstado.setSelection(14)
        spinnerEstado.isEnabled = false
        spinnerEstado.isClickable = false
        var codigo = "52774"
        inputCP.setText(codigo)
        inputCP.isEnabled = false

    }

    override fun muestraDatosMunicipios(municipios: ArrayList<Municipios>) {
        mMunicipios.clear()
        mMunicipios.addAll(municipios)
        val adapter : ArrayAdapter<Municipios> = ArrayAdapter(context!!, R.layout.spinner_text_item, mMunicipios)
        spinnerMunicipio.adapter = adapter

        /*Datos por defaul v1*/
        spinnerMunicipio.setSelection(36)
        spinnerMunicipio.isEnabled = false
        spinnerMunicipio.isClickable = false
    }

    override fun muestraDatosColonias(colonias: ArrayList<Colonias>) {
        mColonias.clear()
        mColonias.addAll(colonias)
        val adapter : ArrayAdapter<Colonias> = ArrayAdapter(context!!, R.layout.spinner_text_item, mColonias)
        spinnerColonia.adapter = adapter

        /*Datos por defaul v1*/
        spinnerColonia.setSelection(69)
        spinnerColonia.isEnabled = false
        spinnerColonia.isClickable = false

    }

    override fun muestraDatosResidencial(residenciales: ArrayList<Residencial>) {
        mResidencial.clear()
        mResidencial.addAll(residenciales)
        val adapter : ArrayAdapter<Residencial> = ArrayAdapter(context!!, R.layout.spinner_text_item, mResidencial)
        spinnerResidencial.adapter = adapter
    }

    override fun muestraErrorEnConexion() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(R.string.msg_sin_conexion)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun irAIniciarSesion() {
        (activity as RegistroActivity).onBackPressed()
    }

    override fun muestraErrorDePeticion(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun muestraMensajesSuccess(mensaje: String) {


        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage(mensaje)
            .setButtonPositive("Ir a iniciar sesión", object : WADialog.OnClickListener {
                override fun onClick() {
                    irAIniciarSesion()
                }
            }).build()
        dialog.show()
    }

    override fun muestraTerminosyCondiciones() {
        (activity as RegistroActivity).setBack(false)
        formulario.visibility = View.GONE
        terminos.visibility  = View.VISIBLE
        scroll.fullScroll(ScrollView.FOCUS_UP)
    }

     fun regresarRegistro() {
         (activity as RegistroActivity).setBack(true)
         formulario.visibility = View.VISIBLE
         terminos.visibility  = View.GONE
    }


    override fun agregarPadecimientosFilterList(padecimientos: ArrayList<Padecimiento>) {
        mPadecimientos.clear()
        mPadecimientos.addAll(padecimientos)

        for (tema in mPadecimientos) {
            chipsInput.addChip(tema.nombre)
        }

    }

    override fun setPresenter(presenter: RegistroContract.Presenter) {
        mPresenter = presenter
    }

    private fun getCadenaPadecimientos() : String {
        var cadenaPadecimients = ""
        val temasSelect = chipsInput.chipsSelected()
        for (i in 0 until temasSelect.size) {
            cadenaPadecimients += if (i+1  < temasSelect.size) {
                "${temasSelect[i]},"
            } else
                temasSelect[i]
        }
        return cadenaPadecimients
    }


    fun setDatosFacebook(){
        Log.d("tag", "set facebook")
        if(user != null) {
             Log.d("tag", "set facebook true")
             inputNombre.setText(user.nombre)
             inputApellidoPaterno.setText(user.apellidoPaterno)
             inputCorreo.setText(user.correo)
             inputConfirmCorreo.setText(user.correo)
        }
    }



    companion object {
        fun getInstance(user: Usuario) : Fragment {
            return RegistroFragment(user)
        }
    }
}