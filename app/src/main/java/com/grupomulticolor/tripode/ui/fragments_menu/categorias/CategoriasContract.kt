package com.grupomulticolor.tripode.ui.fragments_menu.categorias

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.producto.Producto

interface CategoriasContract {

    interface View : BaseView<Presenter> {
        fun muestraErrorEnConexion()
        fun muestraErrorDePeticion(error: String)
        fun muestrarErrorAlCargar(err: String)
        fun agregarProductosFilterList(productos: ArrayList<Producto>, total: Int)
    }

    interface Presenter : BasePresenter {
        fun getproductos(id: Int, limit: Int, offset : Int)
    }
}