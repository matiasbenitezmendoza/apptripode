package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleEstatusContract
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleEstatusFragment
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus.DetalleEstatusPresenter
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import kotlinx.android.synthetic.main.fragment_mis_pedidos.*

class EstatusFragment : FragmentChildNotFab(), EstatusContract.View {

    private var mPresenter: EstatusContract.Presenter? = null
    private lateinit var vw: View
    private var pedidos = ArrayList<Pedido>()
    private var mAdapter: EstatusPedidoAdapter? = null

    private var limit = 10
    private var offset = 0
    private var aptoParaCargar = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_mis_pedidos, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EstatusPresenter(this, context!!)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        pedidos.clear()
        limit = 10
        offset = 0
        aptoParaCargar = true
        mPresenter?.start()

    }

    private fun initAdapter() {
       /* mAdapter = EstatusPedidoAdapter(pedidos, object : EstatusPedidoAdapter.ListenerView {
            override fun onClick(item: Pedido) {

                DetalleEstatusPresenter(
                    DetalleEstatusFragment.instance() as DetalleEstatusContract.View, item, context!!)
                fragmentTransaction(DetalleEstatusFragment.instance())

            }
        })
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context) // add listener
        mRecyclerView.adapter = mAdapter*/

        mAdapter = EstatusPedidoAdapter(pedidos, object : EstatusPedidoAdapter.ListenerView {
            override fun onClick(item: Pedido) {

                DetalleEstatusPresenter(
                    DetalleEstatusFragment.instance() as DetalleEstatusContract.View, item, context!!)
                fragmentTransaction(DetalleEstatusFragment.instance())

            }
        })

        val linearLayoutManager = LinearLayoutManager(context)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)


        mRecyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1
                    if (aptoParaCargar) {
                        if (  pastVisibleItem >= totalItemCount) {
                            aptoParaCargar = false
                            offset += 10
                            mPresenter?.loadOrdenes(limit, offset)
                        }
                    }
                }

            }


        })


    }



    override fun showPedidos(ordenes: ArrayList<Pedido>) {
        Log.d("TAG", "::::show pedidos")
        aptoParaCargar = true
        this.pedidos.addAll(ordenes)
        mAdapter?.notifyDataSetChanged()
    }

    override fun showErrorNetwork() {
        Snackbar.make(vw, "Sin Conexion", Snackbar.LENGTH_LONG).show()
    }


    override fun setPresenter(presenter: EstatusContract.Presenter) {
        mPresenter = presenter
    }


    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = EstatusFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }
}