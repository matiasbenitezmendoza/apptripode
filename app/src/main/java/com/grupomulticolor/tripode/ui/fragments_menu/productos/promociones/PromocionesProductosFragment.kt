package com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones

import android.view.LayoutInflater
import android.view.View
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.tripode.data.producto.Producto
import com.grupomulticolor.tripode.data.producto.ProductoPromocion
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleContract
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.detalleproducto.DetalleProductoPresenter
import kotlinx.android.synthetic.main.fragment_promociones.*

class PromocionesProductosFragment : FragmentChildNotFab(), PromocionesContract.View {

    private var vw: View? = null
    private lateinit var mAdapter: PromocionesAdapter
    private var mproductos = ArrayList<ProductoPromocion>()
    private var mPresenter: PromocionesContract.Presenter? = null

    private var detalle = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_promociones, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        PromocionesPresenter(this, context!!)
        initAdapter()
    }

    override fun onResume() {
        super.onResume()
        if(!detalle){
            mproductos.clear()
            mPresenter?.start()
        }
    }

    private fun initAdapter() {

        mAdapter = PromocionesAdapter(mproductos!!,  object : PromocionesAdapter.ListenerView {
            override fun onClick(item: ProductoPromocion) {
                goDetalleProducto(item)
            }
        })

        mRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(context)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)

    }

    private fun goDetalleProducto(producto: ProductoPromocion) {
        setDetalle(true)
        var nuevop = Producto(
            producto.nombre,
            producto.detalle,
            producto.img,
            producto.marca,
            producto.itemNum,
            producto.precio,
            producto.descuento,
            producto.cantidad,
            producto.category_Id,
            producto.caregory,
            producto.PromotionName,
            producto.PromotionType,
            producto.HasLoyaltyPlan)
        DetalleProductoPresenter(
            DetalleProductoFragment.instance() as DetalleContract.View, nuevop, context!!)
        fragmentTransaction(DetalleProductoFragment.instance())
    }


    override fun muestraErrorEnConexion() {
       // Snackbar.make(vw, "Sin conexion a internet", Snackbar.LENGTH_LONG).show()
    }

    override fun muestraErrorDePeticion(error: String) {

    }

    override fun muestrarErrorAlCargar(err: String) {

    }

    override fun agregarProductosFilterList(productos: ArrayList<ProductoPromocion>, t: Int) {
        mproductos.addAll(productos)
        mAdapter.notifyDataSetChanged()
    }

    override fun setPresenter(presenter: PromocionesContract.Presenter) {
        mPresenter = presenter
    }

    fun setDetalle(b : Boolean){
        this.detalle = b
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = PromocionesProductosFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }
}