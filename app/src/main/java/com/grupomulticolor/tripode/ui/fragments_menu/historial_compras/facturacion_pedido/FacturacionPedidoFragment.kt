package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido


import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.detallepedido.DetalleOrden
import com.grupomulticolor.tripode.data.facturacion.Factura
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_confirmar_pedido.btnBack
import java.util.*
import com.grupomulticolor.tripode.interfaces.Events
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add.AgregarFacturacionFragment
import kotlinx.android.synthetic.main.fragment_confirmar_facturacion.*
import kotlinx.android.synthetic.main.fragment_confirmar_pedido.mRecyclerView


class FacturacionPedidoFragment: FragmentChildNotFab(), FacturacionPedidoContract.View {

    private var mPresenter: FacturacionPedidoContract.Presenter? = null
    private lateinit var vw: View
    private lateinit var mAdapter: FacturacionPedidoAdapter
    private lateinit var mAdapter2: FacturacionAdapter
    private var pedido: Ordenes? = null
    private val detallepedidos: ArrayList<DetalleOrden> = ArrayList()
    private lateinit var prefs: SessionPrefs

    private val dfacturacion: ArrayList<Facturacion> = ArrayList()
    private  var bandera = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_confirmar_facturacion, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //FacturacionPedidoPresenter(this, context!!)
        prefs = SessionPrefs(context!!)

        initAdapter()
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()

    }

    private fun initAdapter() {
        //var f = Facturacion(0, "", "", "", "", "", "", "", "", "", "", "", "", "",1,1,1,1,false)
        mAdapter = FacturacionPedidoAdapter(detallepedidos)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
    }

    private fun initAdapter2() {

        mAdapter2 = FacturacionAdapter(dfacturacion, object: Events {
            override fun onClick(itemId: Int) { // position
                if (dfacturacion[itemId].selected) {
                    //prefs.saveDireccionEntrega(direcciones[itemId].id)
                    //checkboxTC.isChecked = false
                    desactivarItem(itemId)
                    //prefs.saveTipoEnvio(1)

                } else {
                    // prefs.saveDireccionEntrega(-1)
                    // prefs.saveTipoPago(-1)
                }
            }
        }, object : Events {
            override fun onClick(itemId: Int) {
                val addFrag = AgregarFacturacionFragment.instance() as AgregarFacturacionFragment
                addFrag.setData(dfacturacion[itemId])
                goAdd(addFrag)
            }
        })
        mRecyclerView2.adapter = mAdapter2
        mRecyclerView2.layoutManager = LinearLayoutManager(context)
        mRecyclerView2.setHasFixedSize(true)

    }

    private fun goAdd(fragment: AgregarFacturacionFragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    private fun initEvents() {
        btnBack.setOnClickListener {
            (context as MainActivity).irAtras()
        }

        if(bandera){
                    btnAgregarfacturacion.visibility = View.VISIBLE
                    btnFacturarldcom.visibility = View.VISIBLE
                    btnAgregarfacturacion.paintFlags = btnAgregarfacturacion.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                    btnAgregarfacturacion.setOnClickListener {
                        // agregarDireccion
                        (AgregarFacturacionFragment.instance() as AgregarFacturacionFragment).setData(Facturacion())
                        (context as MainActivity).mostrarFragment(AgregarFacturacionFragment.instance())
                    }
                    initAdapter2()
                    btnFacturarldcom.setOnClickListener {
                        var fac: Facturacion? = dfacturacion.find { it.selected }
                        var num: String? = pedido!!.Transaccion

                        if(fac != null && num != null && num != ""){
                            mPresenter?.intentaFinalizarCompra(num!!, fac!!)
                        }
                        else{
                            val dialog = WADialog.Builder(context!!)
                                .error()
                                .setTitle("Error")
                                .setMessage("Debes seleccionar tus datos de facturacion")
                                .setButtonPositive("Ok", object : WADialog.OnClickListener {
                                    override fun onClick() {
                                    }
                                }).build()
                            dialog.show()
                        }

                    }
                    mPresenter?.requestFacturacionDelUsuario()
        }else{
                    btnAgregarfacturacion.visibility = View.GONE
                    btnFacturarldcom.visibility = View.GONE
        }

    }

    private fun desactivarItem(position: Int) {
        for (i in 0 until dfacturacion.size) {
            if (i != position) {
                dfacturacion[i].selected = false
                mAdapter2.notifyItemChanged(i)
            }
        }
    }

    override fun intentarAgregarDetalle(detallepedido: ArrayList<DetalleOrden>){
        detallepedidos.clear()
        detallepedidos.addAll(detallepedido)
        mAdapter.notifyDataSetChanged()
    }

    override fun showPedido(pedido : Ordenes){
        numPedido.text = "#"+String.format("%08d",Integer.parseInt(pedido.orden))
        this.pedido = pedido
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }

    override fun showErrorDetallePedido(msj : String){


    }

    override fun showErrorPedido(msj : String){

    }

    override fun showFacturacionDeUsuario(facturacion: ArrayList<Facturacion>) {
        dfacturacion.clear()

        if(bandera){
            dfacturacion.addAll(facturacion)
        }

        mAdapter2.notifyDataSetChanged()


    }

    override fun showEmpty() {

    }

    override fun cancelar() {

    }

    override fun showFacturacionExitosa(Factura : Factura) {
        val dialog = WADialog.Builder(context!!)
            .succes()
            .setTitle("¡Completado!")
            .setMessage("Se genero su factura con exito")
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                    (context!! as MainActivity).irAtras()
                }
            }).build()
        dialog.show()

    }

    override fun showFacturacionError(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun showConectionError() {
        Snackbar.make(vw, "ir a confirmar", Snackbar.LENGTH_LONG).show()
    }

    override fun setPresenter(presenter: FacturacionPedidoContract.Presenter) {
        mPresenter = presenter
    }

    fun setBandera(b: Boolean){
        this.bandera = b
    }



    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = FacturacionPedidoFragment()
            }
            return fragment!!
        }
        @Synchronized fun destroy() {
            fragment = null
        }
    }

}