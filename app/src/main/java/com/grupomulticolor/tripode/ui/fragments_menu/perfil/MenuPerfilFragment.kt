package com.grupomulticolor.tripode.ui.fragments_menu.perfil

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.facebook.login.LoginManager
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datos_facturacion_add.AgregarFacturacionFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.datosfacturacion.DatosFacturacionFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direcciones.DireccionesFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.direccionesagregar.AgregarDireccionFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.editinfopersonal.EditInfoFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.infopersonal.InfoPersonalFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.missuscripciones.MisSuscripcionesFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancarias.TarjetasBancariasFragment
import com.grupomulticolor.tripode.ui.fragments_menu.perfil.tarjetasbancariasagregar.AgregarTarjetaFragment
import com.grupomulticolor.tripode.ui.fragments_menu.productos.InicioProductosFragment
import com.grupomulticolor.tripode.ui.fragments_menu.productos.promociones.PromocionesProductosFragment
import com.grupomulticolor.tripode.ui.fragments_menu.sin_conexion.SinConexionFragment
import com.grupomulticolor.tripode.ui.login.LoginActivity
import com.grupomulticolor.tripode.util.lib.FragmentParentNotFab
import kotlinx.android.synthetic.main.fragment_opcion_perfil.*

class MenuPerfilFragment : FragmentParentNotFab(), MenuPerfilContract.View {

    private var mPresenter: MenuPerfilContract.Presenter? = null
    private lateinit var vw: View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val interactor = MenuPerfilInteractor(context!!)
        MenuPerfilPresenter(this, interactor)
        btnCerrarSesion.paintFlags = btnCerrarSesion.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        events()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw =  inflater.inflate(R.layout.fragment_opcion_perfil, container, false)
        return vw
    }

    private fun events() {
        menuItemInformacionPersonal.setOnClickListener {
            irAInformacionPersonal()
        }
        menuItemDirecciones.setOnClickListener {
            irADirecciones()
        }
        menuItemTarjetasBancarias.setOnClickListener {
            irATarjetasBancarias()
        }
        menuItemSuscripciones.setOnClickListener {
            irASuscripciones()
        }
        menuItemFacturacion.setOnClickListener {
            irADatosFacturacion()
        }
        btnCerrarSesion.setOnClickListener {
            irALogin()
        }


    }

    /*implementacion contract.view*/
    @SuppressLint("SetTextI18n")
    override fun muestraInformacionUsuario() {
        val usuario = SessionPrefs(context!!).currentUsu()
        tvNameUser.text = "${usuario.nombre} ${usuario.apellidoPaterno} ${usuario.apellidoMaterno}"
        tvEmailUser.text = "${usuario.correo}"
    }

    override fun irAInformacionPersonal() {
        fragmentTransaction(InfoPersonalFragment.instance())
    }

    override fun irADirecciones() {
        fragmentTransaction(DireccionesFragment.instance())
    }

    override fun irATarjetasBancarias() {
        fragmentTransaction(TarjetasBancariasFragment.instance())
    }

    override fun irASuscripciones() {
        fragmentTransaction(MisSuscripcionesFragment.instance())
    }

    override fun irADatosFacturacion() {
        fragmentTransaction(DatosFacturacionFragment.instance())
    }

    override fun irALogin() {

        // destroy intances
        AgregarFacturacionFragment.destroy()
        DatosFacturacionFragment.destroy()
        DireccionesFragment.destroy()
        AgregarDireccionFragment.destroy()
        EditInfoFragment.destroy()
        InfoPersonalFragment.destroy()
        MisSuscripcionesFragment.destroy()
        TarjetasBancariasFragment.destroy()
        AgregarTarjetaFragment.destroy()
        PromocionesProductosFragment.destroy()
        InicioProductosFragment.destroy()
        SinConexionFragment.destroy()
        MenuPerfilFragment.destroy()


        LoginManager.getInstance().logOut()
        SessionPrefs(context!!).logOut()
        val intent = Intent(context, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP
        (context as MainActivity).startActivity(intent)
        (context as MainActivity).finish()

    }

    override fun setPresenter(presenter: MenuPerfilContract.Presenter) {
        this.mPresenter = presenter
    }

    private fun fragmentTransaction(fragment: Fragment) {
        (context as MainActivity).mostrarFragment(fragment)
    }


    companion object {

        private var fragment : Fragment? = null

        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = MenuPerfilFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }

    }
}