package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.pedido.Ordenes
import com.grupomulticolor.tripode.data.pedido.Pedido
import com.grupomulticolor.tripode.util.Currency
import kotlin.collections.ArrayList

class HistorialAdapter(var ordenes: ArrayList<Ordenes>, var pedidos: ArrayList<Pedido>, var listenerView: ListenerView)
    : RecyclerView.Adapter<HistorialAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val vw = LayoutInflater.from(context).inflate(R.layout.item_cardview_historial_pedido, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return ordenes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemOrden : Ordenes = ordenes[position]
        val itemPedido: Pedido = get_pedido(itemOrden.orden!!)
        holder.bind(itemOrden, itemPedido)
    }

    fun get_pedido(NumOrden: String): Pedido{
        for (l in pedidos) {
            if(l.orden == NumOrden){
                return l
            }
        }
        return Pedido()
    }

    fun get_fecha(dia : String, mes : String, anio : String ): String {
        var m: String = ""
        if(mes == "01") m = "Jan"
        if(mes == "02") m = "Feb"
        if(mes == "03") m = "Mar"
        if(mes == "04") m = "Apr"
        if(mes == "05") m = "May"
        if(mes == "06") m = "Jun"
        if(mes == "07") m = "Jul"
        if(mes == "08") m = "Aug"
        if(mes == "09") m = "Sep"
        if(mes == "10") m = "Oct"
        if(mes == "11") m = "Nov"
        if(mes == "12") m = "Dec"
        return "$m $dia, $anio"
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvStatusPedido: TextView = itemView.findViewById(R.id.tvFechaPedido)
        val tvNumPedido: TextView = itemView.findViewById(R.id.tvNumPedido)
        val tvCantidadProductos: TextView = itemView.findViewById(R.id.tvCantidadProductos)
        val tvInfoProductos: TextView = itemView.findViewById(R.id.tvInfoProductos)
        val tvPrecioProducto: TextView = itemView.findViewById(R.id.tvPrecioProducto)

        val btnFacturar: Button = itemView.findViewById(R.id.btnFacturar)
        val btnFacturado: Button = itemView.findViewById(R.id.btnFacturado)
        val btnDevolucion: Button = itemView.findViewById(R.id.btnDevolucion)



        fun bind(orden: Ordenes, pedido: Pedido) {
            tvNumPedido.text = "#"+String.format("%08d",Integer.parseInt(orden.orden))
            tvCantidadProductos.text = "Productos"
            tvStatusPedido.text = get_fecha(orden.fecha_pedido!!.substring(8, 10), orden.fecha_pedido!!.substring(5, 7), orden.fecha_pedido!!.substring(0, 4))
            tvPrecioProducto.text = Currency.toCurrency(orden.total!!, "MXN")

            if(pedido.orden != null && pedido.orden != ""){

                    //detalle de productos
                    if(pedido.detallePedido != null) {
                            var infoProd = ""
                            for (prod in pedido.detallePedido!!) {
                                infoProd += "${prod.producto},\n"
                            }
                            tvInfoProductos.text = infoProd
                    }


                    //Pedido Entregado (Status == 3) && Orden ldcom (Transaccion != null)
                    if(pedido.status_pedido == 3 && orden.Transaccion != null && orden.Transaccion != ""){

                                //Orden ldcom (NumeroFacturaFiscal != null -> Mostrar btn Facturado)
                                if(orden.NumeroFacturaFiscal != null && orden.NumeroFacturaFiscal != ""){
                                            btnDevolucion.visibility = View.GONE
                                            btnFacturar.visibility = View.GONE
                                            btnFacturado.visibility = View.VISIBLE
                                            btnFacturado.setOnClickListener { listenerView.onClick(orden, false) }
                                }
                                //Orden ldcom (NumeroFacturaFiscal == null -> Mostrar btn para Facturar)
                                else{
                                            btnDevolucion.visibility = View.GONE
                                            btnFacturar.visibility = View.VISIBLE
                                            btnFacturado.visibility = View.GONE
                                            btnFacturar.setOnClickListener { listenerView.onClick(orden, true) }
                                }
                    }
                    //Pedido Cancelado (Status == 4), btn Canc/dev..
                    else if(pedido.status_pedido == 4){
                                btnDevolucion.visibility = View.VISIBLE
                                btnFacturar.visibility = View.GONE
                                btnFacturado.visibility = View.GONE
                    }
                    //Pedido en ruta (Status < 3), no mostrar ningun btn
                    else{
                                btnDevolucion.visibility = View.GONE
                                btnFacturar.visibility = View.GONE
                                btnFacturado.visibility = View.GONE
                    }


            }
        }
    }

    interface ListenerView {
        fun onClick(item: Ordenes, b : Boolean)
    }
}