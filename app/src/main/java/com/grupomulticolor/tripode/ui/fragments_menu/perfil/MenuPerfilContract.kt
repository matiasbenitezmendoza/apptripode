package com.grupomulticolor.tripode.ui.fragments_menu.perfil

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView

interface MenuPerfilContract {

    interface View : BaseView<Presenter> {
        fun muestraInformacionUsuario() // usuario
        fun irAInformacionPersonal()
        fun irADirecciones()
        fun irATarjetasBancarias()
        fun irASuscripciones()
        fun irADatosFacturacion()
        fun irALogin()
    }

    interface Presenter : BasePresenter {
        fun cerrarSesion()
    }
}