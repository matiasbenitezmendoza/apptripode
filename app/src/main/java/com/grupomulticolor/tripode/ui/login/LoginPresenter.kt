package com.grupomulticolor.tripode.ui.login

import androidx.annotation.NonNull
import com.facebook.AccessToken
import com.grupomulticolor.tripode.data.usuario.Usuario

class LoginPresenter(@NonNull val mLoginView : LoginContract.View,
                     @NonNull val mLoginInteractor: LoginInteractor ) : LoginContract.Presenter, LoginInteractor.Callback {

    init {
        mLoginView.setPresenter(this)
    }

    /* TODO implementacion de contract presenter */
    override fun start() {
    }

    override fun intentaLoginPorCorreo(correo: String, passwd: String) {
        mLoginInteractor.loginWithEmail(correo, passwd, this)
    }

    override fun intentaLoginPorFacebook(accessToken: AccessToken) {
        mLoginInteractor.loginWithFacebook(accessToken, this)
    }

    /* TODO implementacion de interactor callback */

    override fun onEmailError(err: String) {
        mLoginView.muestraErrorEnCorreo(err)
    }

    override fun onPasswordError(err: String) {
        mLoginView.muestraErrorEnPasswd(err)
    }

    override fun onNetworkConnectionFailed() {
        mLoginView.muestrarErrorEnNetwork()
    }

    override fun onAuthSuccess() {
        mLoginView.muestraTutorial()
    }

    override fun onAuthError(err: String) {
        mLoginView.muestrarErrorDeLogin(err)
    }


    override fun onAuthSuccessFacebook(usuario: Usuario) {
        mLoginView.muestraRegistrarUsuarioFacebook(usuario)
    }

}