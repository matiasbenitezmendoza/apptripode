package com.grupomulticolor.tripode.ui.tutorial

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView

interface TutorialContract {

    interface View : BaseView<Presenter> {
        fun mostrarSiguientePaso()
        fun mostrarInicio()
    }

    interface Presenter : BasePresenter {

    }
}