package com.grupomulticolor.tripode.ui.login

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.usuario.Usuario
import com.grupomulticolor.tripode.ui.recover.RecoverActivity
import com.grupomulticolor.tripode.ui.registro.RegistroActivity
import com.grupomulticolor.tripode.ui.tutorial.TutorialActivity
import com.grupomulticolor.tripode.util.Keyboard
import com.grupomulticolor.tripode.util.lib.TextDrawable
import com.grupomulticolor.walibray.dialog.WADialog
import kotlinx.android.synthetic.main.fragment_login.*
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.grupomulticolor.tripode.data.session.SessionPrefs


class LoginFragment : Fragment(), LoginContract.View {

    private var mPresenter: LoginContract.Presenter? = null
    private lateinit var cntx: Context
    private var mCallbackFacebook = CallbackManager.Factory.create()
    private val REQUEST_FACEBOOK_CODE = 64206

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cntx = context!!
        setEvents()
        btnRecuperarPass.paintFlags = btnRecuperarPass.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            REQUEST_FACEBOOK_CODE -> mCallbackFacebook.onActivityResult(requestCode, resultCode, data)
        }
    }

    /* eventos de botonos e inputs de la interfaz*/
    private fun setEvents() {
        inputUsuario.addTextChangedListener( object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputUsuario.background = ContextCompat.getDrawable(cntx, R.drawable.input_white)
                inputUsuario.setCompoundDrawables(
                    //ContextCompat.getDrawable(cntx, R.drawable.ico_profile),
                    inputUsuario.compoundDrawables[0],
                    null,
                    null,
                    null)
            }
        })

        inputPassword.addTextChangedListener( object  : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputPassword.background = ContextCompat.getDrawable(cntx, R.drawable.input_white)
                inputPassword.setCompoundDrawables(
                    inputPassword.compoundDrawables[0],
                    null,
                    null,
                    null)

            }

        })

        inputPassword.setOnEditorActionListener { textView, actionId, event ->
            var procesado = false
            if (actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                loginWithEmail()
                // Ocultar teclado virtual
                Keyboard.hide(context!!, textView as TextView)
                procesado = true
            }
            return@setOnEditorActionListener procesado
        }

        btnLogin.setOnClickListener { loginWithEmail() }

        btnRecuperarPass.setOnClickListener {
            muestraRecuperarPass() }

        btnFacebook.setOnClickListener { Log.d( LoginFragment::class.java.name, "login with facebook" ) }

        btnSingup.setOnClickListener {
            muestraRegistrarUsuario()
        }


        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired
        if(isLoggedIn){
            LoginManager.getInstance().logOut()
            SessionPrefs(context!!).logOut()
        }

        Log.d("tag", "result: $isLoggedIn")


        btnFacebook.setReadPermissions(arrayListOf("public_profile", "email"))
        btnFacebook.fragment = this
        btnFacebook.registerCallback(mCallbackFacebook, object : FacebookCallback<LoginResult> {

            override fun onSuccess(result: LoginResult?) {
                if (result != null) {
                    val token = result.accessToken
                    mPresenter?.intentaLoginPorFacebook(token)
                }
            }

            override fun onCancel() {
                Log.d("fb_login_sdk", "callback cancel")
            }

            override fun onError(error: FacebookException?) {
                Log.d("fb_login_sdk", error?.message)
            }

        })
    }

    private fun loginWithEmail() {
        mPresenter?.intentaLoginPorCorreo(
            inputUsuario.text.toString(),
            inputPassword.text.toString())
    }

    /**
     * IMPLEMENTACION DE CONTRACT VIEW
     * */

    override fun muestraErrorEnCorreo(error: String) {

        val txd = TextDrawable(cntx, error)
        inputUsuario.background = ContextCompat.getDrawable(cntx, R.drawable.input_white_error)
        inputUsuario.setCompoundDrawables(
            inputUsuario.compoundDrawables[0], //left
            null,
            txd, // right
            null)
    }

    override fun muestraErrorEnPasswd(error: String) {

        inputPassword.background = ContextCompat.getDrawable(cntx, R.drawable.input_white_error)
        inputPassword.setCompoundDrawables(
            //ContextCompat.getDrawable(cntx, R.drawable.ico_look), // left
            inputPassword.compoundDrawables[0], //left
            null,
            TextDrawable(cntx, error), // right
            null)
    }

    override fun muestrarErrorEnNetwork() {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(R.string.msg_sin_conexion)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    override fun setPresenter(presenter: LoginContract.Presenter) {
        mPresenter = presenter
    }

    override fun muestraTutorial() {
        startActivity(Intent(activity, TutorialActivity::class.java))
        activity!!.finish()
    }

    override fun muestraRecuperarPass() {
        val intent = Intent(activity, RecoverActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity?.startActivity(intent)

    }

    override fun muestraRegistrarUsuario() {
        val intent = Intent(activity, RegistroActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("usuario", "")
        intent.putExtra("apellido", "")
        intent.putExtra("email", "")
        activity?.startActivity(intent)
    }

    override fun muestraRegistrarUsuarioFacebook(user: Usuario) {
        /*val intent = Intent(activity, RegistroActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity?.startActivity(intent)*/

        val intent = Intent(activity, RegistroActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("usuario", user.nombre)
        intent.putExtra("apellido", user.apellidoPaterno)
        intent.putExtra("email", user.correo)
        activity?.startActivity(intent)

    }

    override fun muestrarErrorDeLogin(error: String) {
        val dialog = WADialog.Builder(context!!)
            .error()
            .setTitle("Error")
            .setMessage(error)
            .setButtonPositive("Ok", object : WADialog.OnClickListener {
                override fun onClick() {
                }
            }).build()
        dialog.show()
    }

    companion object {
        fun getInstance() : Fragment {
            return LoginFragment()
        }
    }
}