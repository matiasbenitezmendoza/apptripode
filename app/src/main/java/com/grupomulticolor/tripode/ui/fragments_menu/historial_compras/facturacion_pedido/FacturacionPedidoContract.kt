package com.grupomulticolor.tripode.ui.fragments_menu.historial_compras.facturacion_pedido


import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.detallepedido.DetalleOrden
import com.grupomulticolor.tripode.data.facturacion.Factura
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.pedido.Ordenes

interface FacturacionPedidoContract {

    interface View: BaseView<Presenter> {
        fun cancelar()
        fun showConectionError()
        fun intentarAgregarDetalle(detallepedido: ArrayList<DetalleOrden>)
        fun showErrorPedido(msj: String)
        fun showErrorDetallePedido(msj: String)
        fun showPedido(pedido: Ordenes)
        fun showFacturacionDeUsuario(facturacion: ArrayList<Facturacion>)
        fun showEmpty()
        fun showFacturacionExitosa(Factura: Factura)
        fun showFacturacionError(error: String)


    }

    interface Presenter: BasePresenter {
        fun requestDetallesProducto()
        fun intentaFinalizarCompra(num_transaccion: String, fac : Facturacion)
        fun agregarDireccionFiscal()
        fun requestFacturacionDelUsuario()

    }

}