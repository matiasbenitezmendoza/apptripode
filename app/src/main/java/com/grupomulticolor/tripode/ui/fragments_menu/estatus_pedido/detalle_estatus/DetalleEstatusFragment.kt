package com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.detalle_estatus

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.repartidor.Repartidor
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.EstatusFragment
import com.grupomulticolor.tripode.ui.fragments_menu.estatus_pedido.ubicacion_pedido.UbicacionPedidoFragment
import com.grupomulticolor.tripode.util.ConfigList
import com.grupomulticolor.tripode.util.lib.FragmentChildNotFab
import kotlinx.android.synthetic.main.fragment_status_pedido.*
import kotlinx.android.synthetic.main.item_cardview_repartidor.*
import org.qap.ctimelineview.TimelineRow
import org.qap.ctimelineview.TimelineViewAdapter

class DetalleEstatusFragment : FragmentChildNotFab(), DetalleEstatusContract.View {

    private var mPresenter: DetalleEstatusContract.Presenter? = null
    private lateinit var vw: View
    private var repartidor = Repartidor()
    private var orden = ""



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vw = inflater.inflate(R.layout.fragment_status_pedido, container, false)
        return vw
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEvents()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    private fun initEvents() {
        btnVerMapa.setOnClickListener {
            if(this.repartidor.id != null  &&  this.repartidor.id != 0 && this.repartidor.id != -1 ){
                    val fragment = UbicacionPedidoFragment.instance() as UbicacionPedidoFragment
                    fragment.setIdRepartidor(this.repartidor)
                    fragment.setOrden(this.orden)
                    (context as MainActivity).mostrarFragment(fragment)
            }
        }
        btnActualizar.setOnClickListener {
            mPresenter?.RequestPedido()
        }
    }

    override fun showInfoRepartidor(repartidor: Repartidor) {
        nombreRepartidor.text = repartidor.nombre + " " + repartidor.apellido_paterno +" "+repartidor.apellido_materno
        avatarRepartidor
        if(repartidor.imagen != "") {
            Picasso
                .get()
                .load(repartidor.imagen)
                .into(avatarRepartidor)
        }
        this.repartidor = repartidor
    }

    override fun showInfoSinRepartidor() {
        nombreRepartidor.text = "En espera de repartidor"
        btnVerMapa.visibility = View.GONE
    }

    override fun showErrorNetwork() {

    }

    override fun showDetalleEstatus(status: Int, o: String) {
        this.orden = o
        val timelineRowsList = ArrayList<TimelineRow>()
        for (i in 1..3)
            timelineRowsList.add(makeRow(i, i <= status))

        val mAdapter = TimelineViewAdapter(
            context, 0, timelineRowsList,
            false
        )

        timelineListView.adapter = mAdapter
        timelineListView.isClickable = false
        ConfigList.setListViewHeightBasedOnChildren(timelineListView)
        mAdapter.notifyDataSetChanged()

        numPedido.text = "#"+String.format("%08d",Integer.parseInt(o))

        btnBack.setOnClickListener {
            val fragment = EstatusFragment.instance() as EstatusFragment
            (context as MainActivity).mostrarFragment(fragment)
        }

        if (status != 2){
            btnVerMapa.visibility = View.GONE
        }
        else{
            btnVerMapa.visibility = View.VISIBLE
        }

    }

    override fun setPresenter(presenter: DetalleEstatusContract.Presenter) {
        mPresenter = presenter
    }

    private fun makeRow(status: Int, checked: Boolean): TimelineRow {
        val myRow = TimelineRow(status)
        myRow.title = detailStatus(status)

        if (checked) {
            myRow.image = BitmapFactory.decodeResource(context?.resources, R.drawable.status_pedido_checked)
            myRow.titleColor = ContextCompat.getColor(context!!, R.color.nasty_green)
            myRow.bellowLineColor = ContextCompat.getColor(context!!, R.color.nasty_green)
        } else {
            myRow.image = BitmapFactory.decodeResource(context?.resources, R.drawable.status_pedido_unchecked)
            myRow.titleColor = ContextCompat.getColor(context!!, R.color.greyish_brown)
            myRow.bellowLineColor = ContextCompat.getColor(context!!, R.color.greyish_brown)
        }

        myRow.bellowLineSize = 3
        myRow.imageSize = 24
        return myRow
    }

    private fun detailStatus(status: Int): String {
        return when (status) {
            1 -> context!!.getString(R.string.status_1)
            2 -> context!!.getString(R.string.status_3)
            3 -> context!!.getString(R.string.status_4)
            else -> ""
        }
    }

    companion object {
        private var fragment : Fragment? = null
        @Synchronized fun instance() : Fragment {
            if (fragment == null) {
                fragment = DetalleEstatusFragment()
            }
            return fragment!!
        }

        @Synchronized fun destroy() {
            fragment = null
        }
    }
}