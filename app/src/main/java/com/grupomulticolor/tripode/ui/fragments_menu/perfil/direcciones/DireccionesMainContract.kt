package com.grupomulticolor.tripode.ui.fragments_menu.perfil.direcciones

import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.direcciones.Direccion

interface DireccionesMainContract {

    interface View : BaseView<Presenter> {
        fun showDirecciones(direcciones: ArrayList<Direccion>)
        fun showEmpty()
        fun showMessageError(err: String?)
        fun showErrorNetwork()
    }

    interface Presenter : BasePresenter {
        fun loadDirecciones()
        fun eliminarDireccion(itemId: Int)
    }
}