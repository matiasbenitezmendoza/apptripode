package com.grupomulticolor.tripode.ui.fragments_menu.carrito

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.carrito.Carrito
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.util.Currency
import com.grupomulticolor.walibray.dialog.WADialog

class CarritoAdapter(var carrito: ArrayList<Carrito>, var Presenter: CarritoContract.Presenter, var listenerView: ListenerView) : RecyclerView.Adapter<CarritoAdapter.ViewHolder>() {

    lateinit var context: Context
    var c: Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val vw = inflater.inflate(R.layout.item_producto_en_carrito, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return carrito.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = carrito[position]
        holder.nombreProducto.text = item.nombre
        holder.detalleProducto.text = item.detalle

        var url = R.drawable.img_nodisponible
        if(item.imagen != "" && item.imagen != null && item.imagen != "null") {
            Picasso
                .get()
                .load(item.imagen!!)
                .into(holder.imageProducto)
        }else{
            Picasso
                .get()
                .load(url)
                .into(holder.imageProducto)
        }

        holder.precioProducto.text = Currency.toCurrency(item.precio!!, "MXN")
        holder.precioTotal.text = Currency.toCurrency(item.total!!, "MXN")

        val btneliminar: Button = holder.itemView.findViewById(R.id.btnEliminar)
        val btnmas: Button = holder.itemView.findViewById(R.id.btnsumarcarrito)
        val btnmenos: Button = holder.itemView.findViewById(R.id.btnrestarrcarrito)
        val cantidad: Button = holder.itemView.findViewById(R.id.valorcantidad)

        cantidad.text = item.cantidad.toString()


        btnmas.setOnClickListener {
            if( item.cantidad!! < 99){
                item.cantidad = item.cantidad!! + 1
                item.subTotal = item.cantidad!! * item.precio!!

                holder.precioTotal.text = Currency.toCurrency(item.total!!, "MXN")
                cantidad.text = item.cantidad.toString()
                listenerView.onClick()
            }
        }

        btnmenos.setOnClickListener {
            if( item.cantidad!! > 1){
                item.cantidad = item.cantidad!! - 1
                item.subTotal = item.cantidad!! * item.precio!!

                holder.precioTotal.text = Currency.toCurrency(item.total!!, "MXN")
                cantidad.text = item.cantidad.toString()
                listenerView.onClick()
            }

        }

        btneliminar.setOnClickListener {

            val dialog = WADialog.Builder(context!!)
                .error()
                .setTitle("Eliminar")
                .setMessage("¿Desea eliminar este producto del carrito?")
                .setButtonNegative(context!!.getString(R.string.mensaje_btn_cancelar), object : WADialog.OnClickListener {
                    override fun onClick() {
                    }
                })
                .setButtonPositive(context!!.getString(R.string.mensaje_btn_eliminar_carrito), object : WADialog.OnClickListener {
                    override fun onClick() {
                        val id_carrito = SessionPrefs.getInstance(context!!).getIdSC()
                        Presenter.requestEliminarProducto(id_carrito, item.itemId!!, carrito)

                    }
                }).build()
            dialog.show()

        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nombreProducto : TextView = itemView.findViewById(R.id.nombreProducto)
        val detalleProducto: TextView = itemView.findViewById(R.id.detalleProducto)
        val imageProducto : ImageView = itemView.findViewById(R.id.imgProducto)
        //val cantidadPicker : NumberPicker = itemView.findViewById(R.id.numberPicker)
        val precioProducto : TextView = itemView.findViewById(R.id.precioPorProducto)
        val precioTotal : TextView = itemView.findViewById(R.id.precioTotalProducto)
    }


    interface ListenerView {
        fun onClick()
    }
}