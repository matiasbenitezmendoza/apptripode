package com.grupomulticolor.tripode.ui.login

import com.facebook.AccessToken
import com.grupomulticolor.tripode.BasePresenter
import com.grupomulticolor.tripode.BaseView
import com.grupomulticolor.tripode.data.usuario.Usuario

interface LoginContract {

    interface View : BaseView<Presenter> {
        fun muestraErrorEnCorreo(error: String)
        fun muestraErrorEnPasswd(error: String)
        fun muestrarErrorEnNetwork()
        fun muestrarErrorDeLogin(error: String)
        fun muestraTutorial()
        fun muestraRecuperarPass()
        fun muestraRegistrarUsuario()
        fun muestraRegistrarUsuarioFacebook(usuario: Usuario)

    }

    interface Presenter : BasePresenter {
        fun intentaLoginPorCorreo( correo: String, passwd: String)
        fun intentaLoginPorFacebook(accessToken: AccessToken)
    }
}