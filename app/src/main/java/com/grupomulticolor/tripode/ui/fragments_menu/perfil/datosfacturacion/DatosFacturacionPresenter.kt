package com.grupomulticolor.tripode.ui.fragments_menu.perfil.datosfacturacion

import android.content.Context
import com.grupomulticolor.tripode.data.facturacion.Facturacion
import com.grupomulticolor.tripode.data.facturacion.source.FacturacionService
import com.grupomulticolor.tripode.data.service.model.ResponseData
import com.grupomulticolor.tripode.data.service.model.ResponseListener
import com.grupomulticolor.tripode.util.Network

class DatosFacturacionPresenter(val mView: DatosFacturacionContract.View, var context: Context)
    : DatosFacturacionContract.Presenter{

    private val service = FacturacionService.instance(context)

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        loadFacturacion()
    }

    override fun loadFacturacion() {
        if (isNetworkAvalible()) {
            service.getAll(object : ResponseListener<ResponseData<ArrayList<Facturacion>>> {
                override fun onSuccess(response: ResponseData<ArrayList<Facturacion>>) {
                    try {
                        mView.showFacturacion(response.data as ArrayList<Facturacion>)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mView.showEmpty()
                    }
                }

                override fun onError(t: Throwable) {
                    mView.showEmpty()
                }
            })
        }
    }

    override fun eliminarFacturacion(itemId: Int) {
        if (isNetworkAvalible()) {
            service.delete(itemId, object : ResponseListener<ResponseData<Any>> {

                override fun onSuccess(response: ResponseData<Any>) {
                    loadFacturacion()
                }

                override fun onError(t: Throwable) {
                    mView.showMessageError(t.message)
                }
            })
        }
    }

    private fun isNetworkAvalible() : Boolean {
        val isAvailable = Network.isAvailable(context)
        if (!isAvailable)
            mView.showNetworkError()
        return isAvailable
    }


}