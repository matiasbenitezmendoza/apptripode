package com.grupomulticolor.tripode.ui.fragments_menu.carrito.direcciondeentrega

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.data.direcciones.Direccion
import com.grupomulticolor.tripode.interfaces.Events

class DireccionEntregaAdapter(private val direcciones: ArrayList<Direccion>,
                              private var onClickCheck: Events?,
                              private var onClickEditar: Events)
    : RecyclerView.Adapter<DireccionEntregaAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(context)
        val vw = inflate.inflate(R.layout.item_direccion_envio, parent, false)
        return ViewHolder(vw)
    }

    override fun getItemCount(): Int {
        return direcciones.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = direcciones[position]
        holder.bind(item)
        holder.cbSeleccionar.setOnClickListener {
            direcciones[position].selected = !direcciones[position].selected
            onClickCheck?.onClick(position)
        }
        holder.btnEditar.setOnClickListener { onClickEditar.onClick(position) }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        private val tvDireccion: TextView = itemView.findViewById(R.id.tvDireccion)
        val btnEditar: ImageButton = itemView.findViewById(R.id.btnEditar)
        val cbSeleccionar: CheckBox = itemView.findViewById(R.id.cbSeleccionar)

        fun bind(direccion: Direccion) {
            tvTitle.text = direccion.nombreReferencia
            tvDireccion.text = direccion.detalle()
            cbSeleccionar.isChecked = direccion.selected
        }
    }
}