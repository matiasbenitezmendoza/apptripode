package com.grupomulticolor.tripode

interface BasePresenter {
    fun start()
}