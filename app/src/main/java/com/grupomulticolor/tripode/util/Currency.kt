package com.grupomulticolor.tripode.util

object Currency {

    fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)

    fun toCurrency(value: Double, format: String) : String {

        return when(format.toUpperCase()) {
            "MXN" -> {
                "$${value.format(2)} mxn"
            }
            "USD" -> {
                // convertir a dolares
                "$${value.format(2)} usd"
            }
            else -> ""
        }
    }

    fun applyPercent(value: Double, porcent: Double) : Double {
        return value - (value * (porcent/100))
    }
}