package com.grupomulticolor.tripode.util

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.core.content.ContextCompat
import com.grupomulticolor.tripode.R
import com.grupomulticolor.tripode.util.lib.TextDrawable

object Input {

    fun onError(context: Context, editText: EditText, error: String) {
        editText.background = ContextCompat.getDrawable(context, R.drawable.input_white_error)
        editText.setCompoundDrawables(null, null, TextDrawable(context, error), null)
    }

    fun onClear(context: Context, editText: EditText) {
        editText.background = ContextCompat.getDrawable(context, R.drawable.input_white)
        editText.setCompoundDrawables(null, null, null, null)
    }

    fun addTextListener(context: Context, editText: EditText) {
        editText.addTextChangedListener( object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Input.onClear(context, editText)
            }
        })
    }
}