package com.grupomulticolor.tripode.util.lib

interface IMainView {
    interface View {
        fun setCallback(callback: Callback?)
        fun showFab(show: Boolean)
        fun showSearch(show: Boolean)
    }

    interface Callback {
        fun actionFab()
    }
}