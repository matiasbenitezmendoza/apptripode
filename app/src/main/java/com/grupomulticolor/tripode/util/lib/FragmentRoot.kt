package com.grupomulticolor.tripode.util.lib

import androidx.fragment.app.Fragment

open class FragmentRoot : Fragment() {
    var isRoot: Boolean = false
}