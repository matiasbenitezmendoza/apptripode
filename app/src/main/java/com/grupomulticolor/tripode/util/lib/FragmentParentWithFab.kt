package com.grupomulticolor.tripode.util.lib

import android.os.Bundle
import android.view.View

abstract class FragmentParentWithFab : FragmentRoot(), IMainView.Callback {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (context as IMainView.View).setCallback(this)
        (context as IMainView.View).showFab(true)
        (context as IMainView.View).showSearch(false)
        isRoot = true
    }

}