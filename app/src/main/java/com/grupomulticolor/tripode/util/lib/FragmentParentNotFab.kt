package com.grupomulticolor.tripode.util.lib

import android.os.Bundle
import android.view.View

abstract class FragmentParentNotFab : FragmentRoot(), IMainView.Callback {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (context as IMainView.View).setCallback(null)
        (context as IMainView.View).showFab(false)
        (context as IMainView.View).showSearch(false)
        isRoot = true
    }

    override fun actionFab() { }
}