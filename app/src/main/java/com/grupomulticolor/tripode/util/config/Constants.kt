package com.grupomulticolor.tripode.util.config

object Constants {

    const val PRODUCTION = true
    const val patternNombre = "^[a-zA-Z\u00f1\u00d1\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da ]+$"
    const val patternPasswd = "^[a-zA-Z0-9_.,*?!]+$"
    const val patternResidencial = "^[0-9a-zA-Z.,]+$"
    const val patternNumber = "^[0-9]+$"
    const val patternNumberCard = "^d{16}$"
    const val TAG_FRAGMENT = "TAG_FRAGMENT"


    const val OPEN_PAY_ID = "mfv3k7ynfmotpjhdkc0h"
    const val OPEN_PAY_KEY_PRIVADA = "sk_9faca3c79b8e4905924ebe88849de233"
    const val OPEN_PAY_kEY_PUBLICA = "pk_450543d90bd942f48a6ce75d03c56f10"
    const val FORMAT_DATE = "yyyy-MM-dd HH:mm"
    val URL_HOST_TRIPODE =
        if (PRODUCTION)
            "https://new.aphcenter.com/FRAGARE/WebAPI_eCommerce/api/v2/ecommerce/"
        else "https://new.aphcenter.com/FRAGARE/WebAPI_eCommerce/api/v2/ecommerce/"


    val URL_HOST_WA =
        if (PRODUCTION)
            "https://www.farmaciastripode.com/admin/index.php/api/"
        else
            "https://www.farmaciastripode.com/admin/index.php/api/"

}