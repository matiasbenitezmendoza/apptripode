package com.grupomulticolor.tripode

interface BaseView<T> {
    fun setPresenter(presenter: T)
}