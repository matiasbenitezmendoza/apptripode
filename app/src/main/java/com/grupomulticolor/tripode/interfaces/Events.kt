package com.grupomulticolor.tripode.interfaces

interface Events {

    fun onClick(itemId: Int)
}