package com.grupomulticolor.tripode.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.grupomulticolor.tripode.MainActivity
import com.grupomulticolor.tripode.R as RT
import com.grupomulticolor.tripode.data.session.SessionPrefs
import com.grupomulticolor.tripode.ui.splash.SplashActivity


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private lateinit var prefs: SessionPrefs

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // Check if message contains a data payload.
        remoteMessage?.data?.isNotEmpty()?.let {
            if (remoteMessage.data != null){
                var titulo = remoteMessage.data!!["title"]
                var mensaje = remoteMessage.data!!["mensaje"]
                var orden = remoteMessage.data!!["id"]
                sendNotification(mensaje!!, titulo!!, orden!!)
            }
        }


    }

    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: $token")
        sendRegistrationToServer(token)
    }


    private fun scheduleJob() {
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
    }

    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
    }

    private fun sendNotification(messageBody: String, titulo: String, id: String) {
        prefs = SessionPrefs(applicationContext)
        prefs.saveCalificacion(id)



        val channelId = "Tripode"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        var notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            val notificationChannel = NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT)
            notificationChannel.description = channelId
            notificationChannel.enableLights(true)
            notificationChannel.enableVibration(true)
            notificationChannel.setSound(defaultSoundUri, audioAttributes)
            notificationChannel.vibrationPattern = longArrayOf(1000,1000,1000)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        val mBuilder = NotificationCompat.Builder(applicationContext, channelId)
            .setSmallIcon(RT.mipmap.ic_launcher_foreground) // notification icon
            .setContentTitle(titulo) // title for notification
            .setContentText(messageBody)
            .setDefaults(Notification.DEFAULT_LIGHTS )
            .setAutoCancel(true)
            .setWhen(System.currentTimeMillis())
            .setPriority(NotificationCompat.PRIORITY_HIGH)// message for notification


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            mBuilder.setSound(defaultSoundUri)
            mBuilder.setVibrate(longArrayOf(1000,1000,1000))
        }


        val intent = Intent(this,  MainActivity::class.java)
        //intent.putExtra("id_pedido", id)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        mBuilder.setContentIntent(pi)
        notificationManager.notify(0 /* ID of notification */, mBuilder.build())


    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}